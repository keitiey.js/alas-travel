import React from 'react';
import Link from 'react-router-dom/Link';

import { numberToMoney } from '../../../global';

class FormTour extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            giaTour: ''
        });
    }
    componentDidMount() {
        const { GiaTour } = this.props;
        this.setState({
            giaTour: numberToMoney(GiaTour)
        });
    }
    render() {
        const { AnhTour, MaTour, TenTour, ThoiGian } = this.props;
        const { giaTour } = this.state;
        const img = `/upload/tours/${AnhTour}`;
        return (
            <div className="wrap-tour-right-content-xtt clear-fix">
                <div className="left-tour-right-content-xtt clear-fix">
                    <img src={img} alt="" />
                </div>
                <div className="right-tour-right-content-xtt clear-fix">
                    <div className="left-right-tour-right-content-xtt clear-fix">
                        <div className="name-tour-xtt clear-fix">
                            <Link 
                                onClick={() => { window.location.reload(); }}
                                to={`/chi-tiet-tour?MaTour=${MaTour}`}
                            >{TenTour}</Link>
                        </div>
                        <div className="chi-tiet-tour-xtt clear-fix">
                            <div className="ma-tour-xtt">
                                <span>Mã: {MaTour}</span>
                            </div>
                            <div className="thoigian-tour-xtt">
                                <span>
                                    <i className="fa fa-clock-o" aria-hidden="true" />
                                <span> {ThoiGian}</span>
                            </span>
                            </div>
                            <div className="phuongtien-tour-xtt" />
                        </div>
                    </div>
                    <div className="right-right-tour-right-content-xtt clear-fix">
                        <div className="wrap-gia-tour-xtt">
                            <span>{giaTour} VND</span>
                        </div>
                        <div className="wrap-button">
                            <button>
                                <Link 
                                    onClick={() => { window.location.reload(); }}
                                    to={`/chi-tiet-tour?MaTour=${MaTour}`}
                                > CHI TIẾT <i className="fa fa-chevron-right" aria-hidden="true" />
                                </Link>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = FormTour;
