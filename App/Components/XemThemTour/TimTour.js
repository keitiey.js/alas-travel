import React from 'react';
import ReactDOM from 'react-dom';

import { localhost } from '../../../configure';

class TimTour extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            showDiv: false
        });
        this.showGoiY = this.showGoiY.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.handleClickSearch = this.handleClickSearch.bind(this);
    }
    componentDidMount() {
        const { findDOMNode } = ReactDOM;
        const { showDiv } = this.state;
        const { ddGoiY } = this.refs;
        if (showDiv === false) {
            findDOMNode(ddGoiY).style.display = 'none';
        } else {
            findDOMNode(ddGoiY).style.display = 'block';
        }
    }
    componentWillUpdate(nextProps, nextState) {
        const { findDOMNode } = ReactDOM;
        const { ddGoiY } = this.refs;
        if (nextState.showDiv === false) {
            findDOMNode(ddGoiY).style.display = 'none';
        } else {
            findDOMNode(ddGoiY).style.display = 'block';
        }
    }
    handleClickSearch() {
        const inputSearch = document.getElementById('form-tim-dia-diem').value;
        window.location.href = `${localhost}/xem-them?Go=${inputSearch}`;   
        window.location.reload();     
    }
    handleSearch() {
        const { aSocket } = this.props;
        const value = document.getElementById('form-tim-dia-diem').value;
        aSocket.emit('Client-send-data', `${value}`);
        aSocket.on('Server-send-data', (data) => {
            document.getElementById('content-suggestTour').style.display = 'block';
            document.getElementById('diadiemSearch').innerHTML = '';
            for (let i = 0; i < data.DiaDiem.length; i++) {
                document.getElementById('diadiemSearch').innerHTML += `
                    <a class="goDiaDiem" href="/xem-them/where?Go=${data.DiaDiem[i].MaDD}">
                        <div class="DiaDiem-search">
                        <span style="font-size: 14px;">${data.DiaDiem[i].TenDD}</span>
                        </div>
                    </a>                
                `;
            }
            for (let i = 0; i < document.getElementsByClassName('goDiaDiem').length; i++) {
                document.getElementsByClassName('goDiaDiem')[i].addEventListener('click', () => {
                    window.location.reload();
                });
            }
            document.getElementById('tourSearch').innerHTML = '';
            for (let i = 0; i < data.Tours.length; i++) {
                document.getElementById('tourSearch').innerHTML += `
                    <a class="goTour" href="/chi-tiet-tour?MaTour=${data.Tours[i].MaTour}">
                        <div class="Tour-search">
                            <span style="font-size: 14px;">
                                ${data.Tours[i].MaTour} - ${data.Tours[i].TenTour}
                            </span>
                        </div>
                    </a>                
                `;
            }
            for (let i = 0; i < document.getElementsByClassName('goTour').length; i++) {
                document.getElementsByClassName('goTour')[i].addEventListener('click', () => {
                    window.location.reload();
                });
            }
        });
    }
    showGoiY() {
        if (this.state.showDiv === true) {
            this.setState({ showDiv: false });
        } else {
            this.setState({ showDiv: true });
        }
    }
    render() {
        return (
            <div className="wrap-tim-tour">
                <div className="heading-tim-tour">
                    <h3>Tìm Tours</h3>
                </div>
                <div className="content-tim-tour">
                    <label htmlFor="form-tim-dia-diem"><h4>Bạn muốn đi du lịch đến: </h4></label>
                    <input 
                        onChange={this.handleSearch} onClick={this.showGoiY} 
                        type="text" id="form-tim-dia-diem" placeholder="Địa điểm" 
                    />
                    <button onClick={this.handleClickSearch}>
                        <i style={{ color: 'white' }} className="fa fa-search" aria-hidden="true" />
                    </button>
                    <div id="goi-y-diadiem" ref="ddGoiY">
                        <div id="content-suggestTour" className="clear-fix">
                            <div>
                                <span style={{ marginBottom: '10px', fontSize: '14px' }}>
                                    <b>
                                        <i 
                                            className="fa fa-map-marker" 
                                            aria-hidden="true" 
                                        />ĐỊA ĐIỂM
                                    </b>
                                </span>
                                <div id="diadiemSearch" />
                            </div>
                            <div>
                                <span style={{ marginBottom: '10px', fontSize: '14px' }}>
                                    <b><i className="fa fa-map-marker" aria-hidden="true" />
                                        TOURS
                                    </b>
                                </span>
                                <div id="tourSearch" />
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = TimTour;
