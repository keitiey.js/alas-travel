import React from 'react';
import ReactDOM from 'react-dom';
import Link from 'react-router-dom/Link';

class DropDownTours extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            isToggle: false,
            hideDiv: true
        });
        this.handleToggle = this.handleToggle.bind(this);
    }
    handleToggle() {
        const { findDOMNode } = ReactDOM;
        const { hideDiv } = this.state;

        this.setState({ isToggle: !this.state.isToggle });
        if (hideDiv === true) {
            findDOMNode(this.refs.contentGioiThieuDiaDiem).style.display = 'none';
            this.setState({ hideDiv: !this.state.hideDiv });
        } else {
            findDOMNode(this.refs.contentGioiThieuDiaDiem).style.display = 'block';
            this.setState({ hideDiv: !this.state.hideDiv });
        }
    }
    render() {
        const { TrongNuocNgoaiNuoc, KhuVuc } = this.props;

        if (TrongNuocNgoaiNuoc == null) {
            return <div />;
        }
        return (
            <div className="wrap-gioithieu-diadiem">
                <div onClick={this.handleToggle} className="heading-gioithieu-diadiem">
                    <h3><i className="fa fa-chevron-down" aria-hidden="true" />{KhuVuc}</h3>
                </div>
                <div 
                    className="content-gioithieu-diadiem" ref="contentGioiThieuDiaDiem" 
                    id="contentGioiThieuDiaDiem"
                >
                    {TrongNuocNgoaiNuoc.map((e, i) => (
                        <Link
                            onClick={() => { window.location.reload(); }}
                            key={i}
                            to={`/xem-them/where?Go=${e.MaDD}`}
                            style={{ textDecoration: 'none' }}
                        >
                            <div className="child-content-gioithieu-diadiem">{e.TenDD}</div>
                        </Link>
                    ))}
                </div>
            </div>
        );
    }
}

module.exports = DropDownTours;
