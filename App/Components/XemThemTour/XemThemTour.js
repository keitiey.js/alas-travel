import React from 'react';
import Link from 'react-router-dom/Link';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import queryString from 'query-string';
import { fetchDataXemThemTour } from '../../Redux/action.js';

import TimTour from './TimTour';
import DropDownTours from './DropDownTours';
import MenuXTT from './MenuXTT';
import FormTour from './FormTour';

const Loading = require('react-loading-animation');

class XemThemTour extends React.Component {
    componentDidMount() {
        const { location } = this.props;
        const { Go, DeXuat, Gia, ThoiLuong } = queryString.parse(location.search);
        this.props.fetchDataXemThemTour(`${Go}`, `${DeXuat}`, `${Gia}`, `${ThoiLuong}`);
    }
    render() {
        const { dataFetched, data } = this.props.dataXemThemTour;
        const { socket, location } = this.props;
        const { Go } = queryString.parse(location.search);
        if (dataFetched === false) {
            return (
                <div style={{ marginTop: '20%' }}>
                    <Loading />
                </div>
            );
        }
        return (
            <div className="wrap-xemthemtour">
                <div className="wrap-content-xtt clear-fix">
                    <div className="wrap-left-content-xtt-pc clear-fix">
                        <TimTour aSocket={socket} />
                        <div className="wrap-gioithieu-diadiem">
                            <div className="heading-gioithieu-diadiem">
                                <h3>Địa điểm HOT trong nước</h3>
                            </div>
                            <div className="content-gioithieu-diadiem">
                                {data.HotTrongNuoc.map((e, i) => (
                                    <Link
                                        onClick={() => { window.location.reload(); }}
                                        key={i}
                                        to={`/xem-them/where?Go=${e.MaDD}`}
                                        style={{ textDecoration: 'none' }}
                                    >
                                        <div className="child-content-gioithieu-diadiem">
                                            {e.TenDD}
                                        </div>
                                    </Link>
                                ))}
                            </div>
                        </div>

                        <div className="wrap-gioithieu-diadiem">
                            <div className="heading-gioithieu-diadiem">
                                <h3>Địa điểm HOT nước ngoài</h3>
                            </div>
                            <div className="content-gioithieu-diadiem">
                                {data.HotNgoaiNuoc.map((e, i) => (
                                    <Link
                                        onClick={() => { window.location.reload(); }}
                                        key={i}
                                        to={`/xem-them/where?Go=${e.MaDD}`}
                                        style={{ textDecoration: 'none' }}
                                    >
                                        <div className="child-content-gioithieu-diadiem">
                                            {e.TenDD}
                                        </div>
                                    </Link>
                                ))}
                            </div>
                        </div>

                        {/* Co slider */}
                        <DropDownTours
                            TrongNuocNgoaiNuoc={data.TrongNuoc}
                            KhuVuc="Tour trong nước"
                        />

                        <DropDownTours
                            TrongNuocNgoaiNuoc={data.NgoaiNuoc}
                            KhuVuc="Tour ngoài nước"
                        />

                    </div>
                    <div className="wrap-right-content-xtt clear-fix">
                        <div className="wrap-top-right-content-xtt">
                            <h1>{data.Go}</h1>
                            < MenuXTT
                                isGo={Go}
                            />
                        </div>
                        {data.XemThem.map((e, i) =>
                            <FormTour
                                key={i}
                                MaTour={e.MaTour}
                                TenTour={e.TenTour}
                                AnhTour={e.AnhTour}
                                GiaTour={e.GiaTour}
                                ThoiGian={e.ThoiGian}
                            />
                        )}
                    </div>
                    <div className="wrap-left-content-xtt-mb">
                        <TimTour aSocket={socket} />

                        <div className="wrap-gioithieu-diadiem">
                            <div className="heading-gioithieu-diadiem">
                                <h3>Địa điểm HOT trong nước</h3>
                            </div>
                            <div className="content-gioithieu-diadiem">
                                {data.HotTrongNuoc.map((e, i) => (
                                    <Link
                                        onClick={() => { window.location.reload(); }}
                                        key={i}
                                        to={`/xem-them/where?Go=${e.MaDD}`}
                                        style={{ textDecoration: 'none' }}
                                    >
                                        <div className="child-content-gioithieu-diadiem">
                                            {e.TenDD}
                                        </div>
                                    </Link>
                                ))}
                            </div>
                        </div>
                        <div className="wrap-gioithieu-diadiem">
                            <div className="heading-gioithieu-diadiem">
                                <h3>Địa điểm HOT nước ngoài</h3>
                            </div>
                            <div className="content-gioithieu-diadiem">
                                {data.HotNgoaiNuoc.map((e, i) => (
                                    <Link
                                        onClick={() => { window.location.reload(); }}
                                        key={i}
                                        to={`/xem-them/where?Go=${e.MaDD}`}
                                        style={{ textDecoration: 'none' }}
                                    >
                                        <div className="child-content-gioithieu-diadiem">
                                            {e.TenDD}
                                        </div>
                                    </Link>
                                ))}
                            </div>
                        </div>
                        {/* Co slider */}
                        <DropDownTours
                            TrongNuocNgoaiNuoc={data.TrongNuoc}
                            KhuVuc="Tour trong nước"
                        />

                        <DropDownTours
                            TrongNuocNgoaiNuoc={data.NgoaiNuoc}
                            KhuVuc="Tour ngoài nước"
                        />

                    </div>
                </div>
                <div className="wrap-footer-xtt" />
            </div>
        );
    }
}
const mapStateToProps = (state) => ({ dataXemThemTour: state.dataXemThemTour });
const mapDispatchToProps = (dispatch) => bindActionCreators({
    fetchDataXemThemTour
}, dispatch);

module.exports = connect(mapStateToProps, mapDispatchToProps)(XemThemTour);
