import React from 'react';
import Link from 'react-router-dom/Link';
import { localhost } from '../../../configure';

class MenuXTT extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            isGo: this.props.isGo
        });
    }
    componentDidMount() {
        const { isGo } = this.state;
        const currentUrl = window.location.href;
        if (currentUrl === `${localhost}/xem-them?Go=${isGo}&DeXuat=true`) {
            document.getElementById('divDeXuat').classList.add('activeMenu');
        }
        if (currentUrl === `${localhost}/xem-them?Go=${isGo}&Gia=Cao`) {
            document.getElementById('divGiaThap').classList.add('activeMenu');
            document.getElementById('giaCao').style.display = 'none';
            document.getElementById('giaThap').style.display = 'inline';
        }
        if (currentUrl === `${localhost}/xem-them?Go=${isGo}&Gia=Thap`) {
            document.getElementById('divGiaCao').classList.add('activeMenu');
            document.getElementById('giaThap').style.display = 'none';
            document.getElementById('giaCao').style.display = 'inline';
        }
        if (currentUrl === `${localhost}/xem-them?Go=${isGo}&ThoiLuong=Cao`) {
            document.getElementById('divThoiLuongThap').classList.add('activeMenu');
            document.getElementById('thoiLuongCao').style.display = 'none';
            document.getElementById('thoiLuongThap').style.display = 'inline';
        }
        if (currentUrl === `${localhost}/xem-them?Go=${isGo}&ThoiLuong=Thap`) {
            document.getElementById('divThoiLuongCao').classList.add('activeMenu');
            document.getElementById('thoiLuongThap').style.display = 'none';
            document.getElementById('thoiLuongCao').style.display = 'inline';
        }
    }
    render() {
        const { isGo } = this.state;
        return (
            <div className="wrap-menu-xtt-mb clear-fix">
                <div className="title-menu-xtt-mb clear-fix">
                    <p>Sắp xếp theo: </p>
                </div>
                <Link
                    onClick={() => { window.location.reload(); }}
                    id="deXuat" to={`/xem-them?Go=${isGo}&DeXuat=true`}
                >
                    <div id="divDeXuat" className="child-menu-xtt-mb clear-fix">
                        <p>Đề xuất</p>
                    </div>
                </Link>
                <Link
                    onClick={() => { window.location.reload(); }}
                    id="giaCao" to={`/xem-them?Go=${isGo}&Gia=Cao`}
                >
                    <div id="divGiaCao" className="child-menu-xtt-mb clear-fix">
                        <p>Giá</p>
                    </div>
                </Link>
                <Link
                    onClick={() => { window.location.reload(); }}
                    id="giaThap" to={`/xem-them?Go=${isGo}&Gia=Thap`}
                >
                    <div id="divGiaThap" className="child-menu-xtt-mb clear-fix">
                        <p>Giá</p>
                    </div>
                </Link>
                <Link
                    onClick={() => { window.location.reload(); }}
                    id="thoiLuongCao" to={`/xem-them?Go=${isGo}&ThoiLuong=Cao`}
                >
                    <div id="divThoiLuongCao" className="child-menu-xtt-mb clear-fix">
                        <p>Thời lượng</p>
                    </div>
                </Link>
                <Link
                    onClick={() => { window.location.reload(); }}
                    id="thoiLuongThap" to={`/xem-them?Go=${isGo}&ThoiLuong=Thap`}
                >
                    <div id="divThoiLuongThap" className="child-menu-xtt-mb clear-fix">
                        <p>Thời lượng</p>
                    </div>
                </Link>
            </div>
        );
    }
}
module.exports = MenuXTT;
