import React from 'react';

class Tabs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: this.props.selected
        };
    }
    handleClick(index, event) {
        event.preventDefault();
        this.setState({
            selected: index
        });
    }
    renderTitles() {
        function labels(child, index) {
            const activeClass = (this.state.selected === index ? 'active' : '');
            return (
                <li key={index}>
                    <a
                        href="" className={activeClass}
                        onClick={this.handleClick.bind(this, index)}
                    >
                        {child.props.label}
                    </a>
                </li>
            );
        }
        return (
            <ul className="tabs__labels">
                {this.props.children.map(labels.bind(this))}
            </ul>
        );
    }
    renderContent() {
        return (
            <div className="tabs__content">
                {this.props.children[this.state.selected]}
            </div>
        );
    }
    render() {
        return (
            <div className="tabs">
                {this.renderTitles()}
                {this.renderContent()}
            </div>
        );
    }
}
Tabs.defaultProps = {
    selected: 0
};
Tabs.propTypes = {
    selected: React.PropTypes.number,
    children: React.PropTypes.oneOfType([
        React.PropTypes.array,
        React.PropTypes.element
    ]).isRequired
};

module.exports = Tabs;
