import Tabs from './Tabs';
import Pane from './Pane';

module.exports = {
    Tabs, Pane
};

