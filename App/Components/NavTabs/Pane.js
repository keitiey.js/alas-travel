import React from 'react';

class Pane extends React.Component {
    render() {
        return (
        <div>
            {this.props.children}
        </div>
        );
    }
}
Pane.propTypes = {
    label: React.PropTypes.string.isRequired,
    children: React.PropTypes.element.isRequired
};

module.exports = Pane;
