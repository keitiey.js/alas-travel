import React from 'react';
import axios from 'axios';
import Link from 'react-router-dom/Link';
import queryString from 'query-string';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchData } from '../../Redux/action.js';
import { Tabs, Pane } from '../NavTabs/NavTabs.js';
import { DetailTourChild } from '../Tours/DetailTourChild.js';
import { localhost, urlTourDaXem } from '../../../configure.js';
import { moneyToNumber, numberToMoney } from '../../../global';

const Loading = require('react-loading-animation');

class ChiTietTour extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            giaTour: Number(),
            isLoading: true,
            currentTime: '2000-01-01',
            SLK: 1,
            ngayKH: null,
        };
        this.makeActive = this.makeActive.bind(this);
        this.changeSoKhach = this.changeSoKhach.bind(this);
        this.changeNgayKH = this.changeNgayKH.bind(this);
    }
    componentDidMount() {
        const { location } = this.props;
        const parsed = queryString.parse(location.search);
        // dispatch(fetchData(parsed.MaTour));
        this.props.fetchData(parsed.MaTour);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.dataCTTour.dataFetched === true) {
            const today = new Date();
            let dd = today.getDate();
            let mm = today.getMonth() + 1;
            const yyyy = today.getFullYear();
            if (dd < 10) {
                dd = `0${dd}`;
            }
            if (mm < 10) {
                mm = `0${mm}`;
            }
            const currentTimeTemp = `${yyyy}-${mm}-${dd}`;
            const { GiaTour } = nextProps.dataCTTour.data.ChiTietTour[0];
            this.setState({
                giaTour: `${numberToMoney(GiaTour)} VND`,
                currentTime: currentTimeTemp,
                ngayKH: currentTimeTemp
            });
        }
    }
    componentDidUpdate() {
        if (this.props.dataCTTour.dataFetched === true) {
            // Handle scroll
            this.handleScrollWindow('form-dat-tour', 'right-content-ctt');

            axios.post(`${urlTourDaXem.insert}`, {
                MaTour: this.props.dataCTTour.data.ChiTietTour[0].MaTour
            })
                .then((response) => console.log(response))
                .catch((error) => console.log(error));
        }
    }

    handleScrollWindow(divFormTemp) {
        const divForm = document.getElementById(divFormTemp);
        const distanceDivFromTop = divForm.offsetTop;
        window.onscroll = () => {
            if (document.body.scrollTop > distanceDivFromTop
                || document.documentElement.scrollTop > distanceDivFromTop) {
                divForm.classList.add('scrollDiv');
            } else {
                divForm.classList.remove('scrollDiv');
            }
            this.scrollDiv();
        };
    }
    makeActive(e, objDiv, objDivClick) {
        const aDiv = document.getElementById(objDiv);
        const aDiv1 = document.getElementById(objDivClick);
        const selector = '.wrap-nav-tour-pc .nav-tour-pc';
        const elems = document.querySelectorAll(selector);
        for (let i = 0; i < elems.length; i++) elems[i].classList.remove('active');
        aDiv1.classList.add('active');
        aDiv.scrollIntoView();
    }
    scrollDiv() {
        const aLichTrinhTour = document.getElementById('lichTrinhTour');
        const navLichTrinhTour = document.getElementById('navLichTrinhTour');

        const aDichVuDiKem = document.getElementById('dichVuDiKem');
        const navDichVuDiKem = document.getElementById('navDichVuDiKem');

        const aDieuKhoan = document.getElementById('dieuKhoan');
        const navDieuKhoan = document.getElementById('navDieuKhoan');

        const aQuyDinh = document.getElementById('quyDinh');
        const navQuyDinh = document.getElementById('navQuyDinh');

        const selector = '.wrap-nav-tour-pc .nav-tour-pc';
        const elems = document.querySelectorAll(selector);
        const distanceLichTrinhTour = aLichTrinhTour.getBoundingClientRect().top;
        const distanceDichVuDiKem = aDichVuDiKem.getBoundingClientRect().top;
        const distanceDieuKhoan = aDieuKhoan.getBoundingClientRect().top;
        const distanceQuyDinh = aQuyDinh.getBoundingClientRect().top;

        if (distanceLichTrinhTour <= 5 && distanceLichTrinhTour >= -5) {
            for (let i = 0; i < elems.length; i++) { elems[i].classList.remove('active'); }
            navLichTrinhTour.classList.add('active');
        }
        if (distanceDichVuDiKem <= 5 && distanceDichVuDiKem >= -5) {
            for (let i = 0; i < elems.length; i++) { elems[i].classList.remove('active'); }
            navDichVuDiKem.classList.add('active');
        }
        if (distanceDieuKhoan <= 5 && distanceDieuKhoan >= -5) {
            for (let i = 0; i < elems.length; i++) { elems[i].classList.remove('active'); }
            navDieuKhoan.classList.add('active');
        }
        if (distanceQuyDinh <= 5 && distanceQuyDinh >= -5) {
            for (let i = 0; i < elems.length; i++) { elems[i].classList.remove('active'); }
            navQuyDinh.classList.add('active');
        }
    }
    changeSoKhach(e) {
        const value = parseInt(e.target.value, 10);
        const giaTourBanDau = moneyToNumber(this.props.dataCTTour.data.ChiTietTour[0].GiaTour);
        const currentMoney = giaTourBanDau * value;
        this.setState({
            giaTour: `${numberToMoney(currentMoney)} VND`,
            SLK: value
        }, function () {
            console.log(this.state.SLK);
        });
    }
    changeNgayKH(e) {
        const value = e.target.value;
        this.setState({
            ngayKH: value
        }, () => {

        });
    }
    render() {
        const { dataFetched, data } = this.props.dataCTTour;
        const { giaTour, currentTime, ngayKH, SLK } = this.state;
        if (dataFetched === false) {
            return (
                <div style={{ marginTop: '20%' }}>
                    <Loading />
                </div>
            );
        }
        const AnhTour = `${localhost}/upload/tours/${data.ChiTietTour[0].AnhTour}`;
        const PhuongTien = [];
        data.PhuongTien.forEach((pt) => {
            PhuongTien.push(pt.MaPT);
        });
        const arrPhuongTien = [];
        for (let i = 0; i < PhuongTien.length; i++) {
            if (PhuongTien[i] === 'BUS') {
                arrPhuongTien.push('fa fa-bus');
            } else {
                arrPhuongTien.push('');
            }
            if (PhuongTien[i] === 'PLANE') {
                arrPhuongTien.push('fa fa-plane');
            } else {
                arrPhuongTien.push('');
            }
            if (PhuongTien[i] === 'SHIP') {
                arrPhuongTien.push('fa fa-ship');
            }
        }
        return (
            <div style={{ height: '100%' }}>
                <div className="wrap-content-ctt">
                    <h3 id="wrap-content-ctt-h3">{data.ChiTietTour[0].TenTour}</h3>
                    <div className="left-content-ctt">
                        <div className="wrap-top-left-content-ctt">
                            <div className="wrap-img-top-left-content-cntt">
                                <img src={AnhTour} alt="" />
                            </div>
                            <div className="wrap-info-top-left-content-cntt">
                                <div className="wrap-info-1-top-left-content-cntt">
                                    <div className="left-info-1-top-left-content-cntt">
                                        <i
                                            style={{ color: '#50535D' }}
                                            className="fa fa-map-marker fa-1x" aria-hidden="true"
                                        />
                                        <span>{data.DiaDiem[0].TenDD}</span>
                                    </div>
                                    <div className="right-info-1-top-left-content-cntt">
                                        <span>
                                            Mã tour: <span>{data.ChiTietTour[0].MaTour}</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="wrap-info-2-top-left-content-cntt clear-fix">
                                    <i
                                        style={{ color: '#50535D' }}
                                        className="fa fa-clock-o" aria-hidden="true"
                                    />
                                    <span>{data.ChiTietTour[0].ThoiGian}</span>
                                </div>
                                <div className="wrap-info-3-top-left-content-cntt">
                                    <i
                                        style={{ color: '#50535D' }}
                                        className="fa fa-paper-plane" aria-hidden="true"
                                    />
                                    <span>Phương tiện:</span>
                                    {
                                        arrPhuongTien.map((pt, i) =>
                                            <i
                                                key={i}
                                                style={{ color: '#50535D', marginLeft: '10px' }}
                                                className={pt} aria-hidden="true"
                                            />
                                        )
                                    }
                                </div>
                                <div className="wrap-info-4-top-left-content-cntt">
                                    <i
                                        style={{ color: '#50535D' }} className="fa fa-file"
                                        aria-hidden="true"
                                    />
                                    <span>KH: {data.ChiTietTour[0].GioiThieu}</span>
                                </div>
                            </div>
                            <div className="form-dat-tour-mobile">
                                <div className="top-form-dat-tour">
                                    {/* <span>11.990.000 VND</span> */}
                                    <span>{giaTour}</span>
                                </div>
                                <div className="mid-form-dat-tour">
                                    <div className="left-mid-form-dat-tour clear-fix">
                                        <label htmlFor="khoi-hanh">KHỞI HÀNH</label><br />
                                        <input type="date" defaultValue={currentTime} />
                                    </div>
                                    <div className="right-mid-form-dat-tour clear-fix">
                                        <label htmlFor="so-khach">SỐ KHÁCH</label><br />
                                        <input
                                            type="number" defaultValue="1" min="1" max="100"
                                            id="SLK" onChange={this.changeSoKhach}
                                        />
                                    </div>
                                </div>
                                <div className="bot-form-dat-tour">
                                    <Link
                                        onClick={() => { window.location.reload(); }}
                                        to={`/thanh-toan?MaTour=${data.ChiTietTour[0].MaTour}
                                            &SLK=${SLK}
                                            &GiaTour=${giaTour}
                                            &NgayKH=${ngayKH}`}
                                    >
                                        <button>ĐẶT TOUR</button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div className="wrap-mid-left-content-ctt">
                            <div className="wrap-content-tour-1" id="lichTrinhTour">
                                <h3>LỊCH TRÌNH TOUR</h3>
                                <div className="content-tour-1">
                                    {data.LichTrinhTour.map((e, i) =>
                                        <div key={i} className="lich-trinh-tour">
                                            <p>{e.NoiDung1}</p>
                                            <img src={`upload/tours/${e.Anh}`} alt="" />
                                            <p>{e.NoiDung2}</p>
                                        </div>)}
                                </div>
                            </div>
                            <div className="wrap-content-tour-2" id="dichVuDiKem">
                                <h3>DỊCH VỤ ĐI KÈM</h3>
                                <div className="content-tour-2">
                                    <div className="row-content-tour-2 clear-fix">
                                        <div className="left-row-content-tour-2">
                                            <i 
                                                className="fa fa-check" aria-hidden="true" 
                                            /><span>Bảo hiểm</span>
                                        </div>
                                        <div className="right-row-content-tour-2">
                                            <i 
                                                className="fa fa-cutlery" aria-hidden="true" 
                                            /><span>Bữa ăn</span>
                                        </div>
                                    </div>
                                    <div className="row-content-tour-2 clear-fix">
                                        <div className="left-row-content-tour-2">
                                            <i 
                                                className="fa fa-flag" aria-hidden="true" 
                                            /><span>Hướng dẫn</span>
                                        </div>
                                        <div className="right-row-content-tour-2">
                                            <i 
                                                className="fa fa-ticket" aria-hidden="true" 
                                            /><span>Vé tham quan</span>
                                        </div>
                                    </div>
                                    <div className="row-content-tour-2 clear-fix">
                                        <div className="left-row-content-tour-2" />
                                        <div className="right-row-content-tour-2">
                                            <i 
                                                className="fa fa-bus" aria-hidden="true" 
                                            /> <span>Xe đưa đón</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="wrap-content-tour-3" id="dieuKhoan">
                                <h3>ĐIỀU KHOẢN</h3>
                                <div className="content-tour-3">
                                    <Tabs selected={0}>
                                        <Pane label="Giá Tour Bao Gồm">
                                            <div>
                                                <div style={{ marginBottom: '20px' }}>
                                                    <p>{
                                                        data.DieuKhoan[0].VanChuyen !== ''
                                                            ? data.DieuKhoan[0].VanChuyen
                                                            : ''
                                                    }</p>
                                                </div>
                                                <div style={{ marginBottom: '20px' }}>
                                                    <p>{
                                                        data.DieuKhoan[0].LuuTru !== ''
                                                            ? data.DieuKhoan[0].LuuTru
                                                            : ''
                                                    }</p>
                                                </div>
                                                <div style={{ marginBottom: '20px' }}>
                                                    <p>{
                                                        data.DieuKhoan[0].Khac !== ''
                                                            ? data.DieuKhoan[0].Khac
                                                            : ''
                                                    }</p>
                                                </div>
                                            </div>
                                        </Pane>
                                        <Pane label="Phụ Thu">
                                            <div>
                                                <p>- Từ 6 tuổi đến dưới 12 tuổi: 75% giá vé người lớn (có ghế ngồi, ngủ chung giường với cha mẹ)</p>
                                                <br />
                                                <p>- Từ 2 tuổi đến dưới 6 tuổi: 50% giá vé người lớn (có ghế ngồi, ngủ chung giường với cha mẹ)</p>
                                                <br />
                                                <p>- Dưới 2 tuổi: Không tính vé, thu phí bảo hiểm và cửa khẩu 300.000đ, gia đình tự lo.</p>
                                                <br />
                                                <p>Hai người lớn chỉ được kèm 1 trẻ em, nếu trẻ em đi kèm nhiều hơn thì từ trẻ em thứ 2 trở lên phải mua ½ vé. Từ trên 12 tuổi: 100% giá tour.</p>
                                            </div>
                                        </Pane>
                                        <Pane label="Đối Tác">
                                            <div>
                                                <p>
                                                    <span
                                                        style={{
                                                            textDecoration: 'underline',
                                                            fontWeight: 'bold'
                                                        }}
                                                    >
                                                        Đơn vị tổ chức:
                                                    </span>
                                                    <span>{data.DoiTac[0].TenDT}</span>
                                                </p>
                                                <br />
                                                <p>
                                                    <span
                                                        style={{
                                                            textDecoration: 'underline',
                                                            fontWeight: 'bold'
                                                        }}
                                                    >
                                                        Địa chỉ:
                                                    </span>
                                                    <span>{data.DoiTac[0].DiaChi}</span></p>
                                            </div>
                                        </Pane>
                                    </Tabs>
                                </div>
                            </div>
                            <div className="wrap-content-tour-4" id="quyDinh">
                                <h3>QUY ĐỊNH</h3>
                                <div className="content-tour-4">
                                    <Tabs selected={0}>
                                        <Pane label="Chú Ý">
                                            <div>
                                                <p>- Trường hợp Quý khách không được xuất cảnh và nhập cảnh vì lý do cá nhân, Công Ty sẽ không chịu trách niệm và sẽ không hoàn trả tiền tour.</p>
                                                <br />
                                                <p>- Du khách có mặt tại điểm đón trước 15 phút. Du khách đến trễ khi xe đã khởi hành hoặc hủy tour không báo trước vui lòng chịu phí như hủy vé ngay ngày khởi hành.</p>
                                                <br />
                                                <p>- Du khách mang theo giấy CMND hoặc Hộ chiếu (Bản chính), nên mang theo hành lý gọn nhẹ, không mang valy lớn.</p>
                                                <br />
                                                <p>- Trẻ em phải đi cùng cha mẹ hoặc có giấy cam kết đi tour cùng người thân, khi đi phải mang theo giấy khai sinh hoặc hộ chiếu.</p>
                                                <br />
                                                <p>- Du khách tự quản lý tiền bạc tư trang trong qua trình tham quan du lịch.</p>
                                                <br />
                                                <p>- Khi đăng ký, quý khách vui lòng cung cấp Hộ Chiếu bản chính (còn giá trị ít nhất 6 tháng tính đến ngày về)</p>
                                                <br />
                                                <p>- Đối với du khách là Việt kiều, Quốc tế nhập cảnh Việt Nam bằng visa rời, vui lòng mang theo tờ khai hải quan và visa khi đi tour.</p>
                                                <br />
                                                <p>- Miễn Visa Campuchia với người mang quốc tịch: Malaysia, Lào, Phillippin, Singapore, Việt Nam…</p>
                                                <br />
                                                <p>- Quý khách mang 02 Quốc tịch hoặc Travel document (chưa nhập Quốc tịch) vui lòng thông báo với nhân viên bán tour và nộp bản gốc các giấy tờ có liên quan khi tham gia tour.</p>
                                                <br />
                                                <p>- Quý khách mang thẻ xanh và không còn hộ chiếu Việt Nam còn hiệu lực thì không đăng ký tour được.</p>
                                                <br />
                                                <p>- Đối với khách hàng hơn 70 tuổi, gia đình và Quý khách phải cam kết đảm bảo tình trạng sức khoẻ với công ty chúng tôi trước khi tham gia Tour. Nếu có bất cứ xự cố nào xảy ra trên tour, Công Ty Du Lịch Nam Á Châu sẽ không chịu trách nhiệm.</p>
                                                <br />
                                                <p>- Tùy theo điều kiện thực tế mà chương trình tham quan, khách sạn, có thể thay đổi cho phù hợp, tuy nhiên tổng số điểm tham quan vẫn đảm bảo đầy đủ.</p>
                                            </div>
                                        </Pane>
                                        <Pane label="Hủy Tour">
                                            <div>
                                                <p>- Sau khi đăng ký, nếu huỷ tour khách phải trả 50% tour.</p>
                                                <br />
                                                <p>- Truớc 03 – 05 ngày xuất phát: khách phải trả 80% tiền tour trọn gói.</p>
                                                <br />
                                                <p>- Trong vòng 24 giờ xuất phát: khách phải trả 100% tiền tour trọn gói.</p>
                                            </div>
                                        </Pane>
                                    </Tabs>
                                </div>
                            </div>
                        </div>
                        <div className="wrap-bot-left-content-ctt clear-fix">
                            <div className="top-bot-left-content-ctt clear-fix">
                                <h3>{data.ToursLienQuan[0].TenDD}</h3>
                            </div>
                            <div className="bot-bot-left-content-ctt clear-fix">
                                {data.ToursLienQuan.map((e, i) => (
                                    <Link
                                        key={i}
                                        onClick={() => { window.location.reload(); }}
                                        to={`/chi-tiet-tour?MaTour=${e.MaTour}`}
                                    >
                                        <DetailTourChild
                                            imageTour={e.AnhTour}
                                            nameTour={e.TenTour}
                                            timeTour={e.ThoiGian}
                                            keHoachTour={e.GioiThieu}
                                            giaTour={e.GiaTour}
                                            menhGiaTien="VND"
                                            namePlace1={e.TenDD}
                                            showRibbonAndPlace="no"

                                        />
                                    </Link>
                                ))}
                            </div>
                        </div>
                        <div className="wrap-bot-left-content-ctt clear-fix">
                            <div className="top-bot-left-content-ctt clear-fix">
                                <h3>Tours du lịch bạn đã xem gần đây</h3>
                            </div>
                            <div className="bot-bot-left-content-ctt clear-fix">
                                {data.ToursDaXem.map((e, i) => (
                                    <Link
                                        key={i}
                                        onClick={() => { window.location.reload(); }}
                                        to={`/chi-tiet-tour?MaTour=${e.MaTour}`}
                                    >
                                        <DetailTourChild
                                            imageTour={e.AnhTour}
                                            nameTour={e.TenTour}
                                            timeTour={e.ThoiGian}
                                            keHoachTour={e.GioiThieu}
                                            giaTour={e.GiaTour}
                                            menhGiaTien="VND"
                                            namePlace1={e.TenDD}
                                            showRibbonAndPlace="no"

                                        />
                                    </Link>
                                ))}
                            </div>
                        </div>
                    </div>
                    <div
                        className="right-content-ctt clear-fix"
                        id="right-content-ctt" ref="rightContentCTT"
                    >
                        <div
                            className="wrap-form-dat-tour-pc clear-fix"
                            id="form-dat-tour" ref="formDatTour"
                        >
                            <div className="form-dat-tour-pc clear-fix">
                                <div className="top-form-dat-tour clear-fix">
                                    <span>{giaTour}</span>
                                </div>
                                <div className="mid-form-dat-tour clear-fix">
                                    <div className="left-mid-form-dat-tour clear-fix">
                                        <label htmlFor="khoi-hanh">KHỞI HÀNH</label><br />
                                        <input
                                            type="date" onChange={this.changeNgayKH}
                                            defaultValue={currentTime}
                                        />
                                    </div>
                                    <div className="right-mid-form-dat-tour clear-fix">
                                        <label htmlFor="so-khach">SỐ KHÁCH</label><br />
                                        <input
                                            type="number" defaultValue="1" min="1"
                                            max="100" id="SLK"
                                            onChange={this.changeSoKhach}
                                        />
                                    </div>
                                </div>
                                <div className="bot-form-dat-tour clear-fix">
                                    <Link
                                        onClick={() => { window.location.reload(); }}
                                        to={`/thanh-toan?MaTour=${data.ChiTietTour[0].MaTour}&SLK=${SLK}&GiaTour=${giaTour}&NgayKH=${ngayKH}`}
                                    >
                                        <button>ĐẶT TOUR</button>
                                    </Link>
                                </div>
                            </div>
                            <div className="wrap-nav-tour-pc clear-fix">
                                <div 
                                    id="navLichTrinhTour" 
                                    onClick={(e) => { 
                                        this.makeActive(e, 'lichTrinhTour', 'navLichTrinhTour'); 
                                    }} 
                                    className="nav-tour-pc"
                                >
                                    <span>LỊCH TRÌNH TOUR</span>
                                </div>
                                <div 
                                    id="navDichVuDiKem" 
                                    onClick={(e) => { 
                                        this.makeActive(e, 'dichVuDiKem', 'navDichVuDiKem'); 
                                    }} 
                                    className="nav-tour-pc"
                                >
                                    <span>DỊCH VỤ ĐI KÈM</span>
                                </div>
                                <div 
                                    id="navDieuKhoan" 
                                    onClick={(e) => { 
                                        this.makeActive(e, 'dieuKhoan', 'navDieuKhoan'); 
                                    }} 
                                    className="nav-tour-pc"
                                >
                                    <span>ĐIỀU KHOẢN</span>
                                </div>
                                <div 
                                    id="navQuyDinh" 
                                    onClick={(e) => { 
                                        this.makeActive(e, 'quyDinh', 'navQuyDinh'); 
                                    }} 
                                    className="nav-tour-pc"
                                >
                                    <span>QUY ĐỊNH</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({ dataCTTour: state.dataCTTour });
const mapDispatchToProps = (dispatch) => bindActionCreators({
    fetchData
}, dispatch);

module.exports = connect(mapStateToProps, mapDispatchToProps)(ChiTietTour);
