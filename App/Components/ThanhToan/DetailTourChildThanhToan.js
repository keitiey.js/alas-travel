import React from 'react';

class DetailTourChildThanhToan extends React.Component {
    render() {
        const { 
            imageTour, arrPhuongTien, showRibbonAndPlace, namePlace1,
            maTour, nameTour, timeTour, keHoachTour, slKhach, giaTour
        } = this.props;
        const img = `upload/tours/${imageTour}`;
        const tempArrPhuongTien = [];
        if (typeof (arrPhuongTien) !== 'string') {
            for (let i = 0; i < this.props.arrPhuongTien.length; i++) {
                if (arrPhuongTien[i].MaPT === 'BUS') tempArrPhuongTien.push('fa fa-bus');
                if (arrPhuongTien[i].MaPT === 'PLANE') tempArrPhuongTien.push('fa fa-plane');
                if (arrPhuongTien[i].MaPT === 'SHIP') tempArrPhuongTien.push('fa fa-ship');
            }         
        } else {
            if (arrPhuongTien === 'BUS') tempArrPhuongTien.push('fa fa-bus');
            else tempArrPhuongTien.push('');
            
            if (arrPhuongTien === 'PLANE') tempArrPhuongTien.push('fa fa-plane');
            else tempArrPhuongTien.push('');

            if (arrPhuongTien === 'SHIP') tempArrPhuongTien.push('fa fa-ship');
            else tempArrPhuongTien.push('');
        }
        let aRibbon = '';
        let aPlace = '';
        if (showRibbonAndPlace === 'yes') {
            aRibbon = (
            <div className="ribbon-tour">
                <h2>Deal giá tốt và shock</h2>
            </div>
            );
            aPlace = (
                <div className="clear-fix wrap-diadiem-tour-1">
                    <div className="clear-fix ten-diadiem-tour-1">
                        <i className="fa fa-map-marker fa-1x" aria-hidden="true" /> {namePlace1}
                    </div>
                </div>
            );
        } else {
            aRibbon = null;
            aPlace = null;
        }
        return (
            <div className="tour-thanh-toan clear-fix">
                <div className="wrap-images">
                    {aRibbon}
                    <img src={img} alt="" />
                    {aPlace}
                </div>
                <div className="tour-detail">
                    <div className="name-tour">
                        <span style={{ color: '#00C1DE' }}>
                            {maTour}</span><span> | {nameTour}
                        </span>
                    </div>
                    <div className="thoigian-phuongtien-tour clear-fix">
                        <div className="thoigian-tour">
                            <i className="fa fa-clock-o" aria-hidden="true" /> {timeTour}
                        </div>
                        <div className="phuongtien-tour">
                            {tempArrPhuongTien.map((pt, i) => 
                                <i 
                                    key={i} 
                                    style={{ color: '#50535D', marginLeft: '10px' }} 
                                    className={pt} aria-hidden="true" 
                                /> 
                            )}
                        </div>
                    </div>
                    <div className="khoihanh-tour">
                        <i className="fa fa-building" aria-hidden="true" />
                        KH: {keHoachTour} - <b>{slKhach}</b> Khách
                    </div>
                    <div className="gia-tour">
                        <span 
                            style={{ color: '#4E4E4E', fontSize: '17px' }}
                        >Tổng: </span><span>{giaTour}</span>
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = { DetailTourChildThanhToan };
