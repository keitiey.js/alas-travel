import React from 'react';
import axios from 'axios';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import queryString from 'query-string';

import { localhost } from '../../../configure.js';
import { DetailTourChildThanhToan } from './DetailTourChildThanhToan.js';
import { fetchData } from '../../Redux/action.js';

const Loading = require('react-loading-animation');

class ThanhToan extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            MaTour: '',
            TenTour: '',
            NgayKH: '',
            SLKhach: '',
            TongTien: '',
            TenKH: '',
            EmailKH: '',
            SDTKH: '',
            Khac: '',
            HinhThucThanhToan: ''
        };
        this.handleTenKhachHang = this.handleTenKhachHang.bind(this);
        this.handleDienThoai = this.handleDienThoai.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
        this.handleKhac = this.handleKhac.bind(this);
        this.handleDatTour = this.handleDatTour.bind(this);
        this.handleHinhThucThanhToan = this.handleHinhThucThanhToan.bind(this);
    }
    componentDidMount() {
        const { location } = this.props;
        const { MaTour } = queryString.parse(location.search);
        this.props.fetchData(MaTour);
    }
    componentWillReceiveProps(nextProps) {
        const { NgayKH, SLK, GiaTour } = queryString.parse(nextProps.location.search);
        if (nextProps.dataCTTour.isFetching === false) {
            this.setState({
                MaTour: nextProps.dataCTTour.data.ChiTietTour[0].MaTour,
                TenTour: nextProps.dataCTTour.data.ChiTietTour[0].TenTour,
                NgayKH,
                SLKhach: SLK,
                TongTien: GiaTour
            }, () => { });
        }
    }
    handleDatTour() {
        const tenKH = document.getElementById('tenKH');
        const sdtKH = document.getElementById('sdtKH');
        const emailKH = document.getElementById('emailKH');
        if (tenKH.value === '') {
            alert('Tên khách hàng không được bỏ trống!');
        } else if (sdtKH.value === '') {
            alert('SDT không được bỏ trống!');
        } else if (emailKH.value === '') {
            alert('Email không được bỏ trống!');
        } else {
            const ChiTietHoaDon = {};
            ChiTietHoaDon.MaTour = this.state.MaTour;
            ChiTietHoaDon.TenTour = this.state.TenTour;
            ChiTietHoaDon.NgayKH = this.state.NgayKH;
            ChiTietHoaDon.SLKhach = this.state.SLKhach;
            ChiTietHoaDon.TongTien = this.state.TongTien;
            ChiTietHoaDon.TenKH = this.state.TenKH;
            ChiTietHoaDon.EmailKH = this.state.EmailKH;
            ChiTietHoaDon.SDTKH = this.state.SDTKH;
            ChiTietHoaDon.Khac = this.state.Khac;
            ChiTietHoaDon.HinhThucThanhToan = this.state.HinhThucThanhToan;

            document.getElementById('wrapTopLeftMidThanhToan').innerHTML = '';
            document.getElementById('wrapTopLeftMidThanhToan').innerHTML = `
                <h1>Đơn đặt tour đang được xử lý ... </h1>
            `;
            setTimeout(() => {
                axios.post(`${localhost}/api/chi-tiet-dat-tour`, ChiTietHoaDon)
                    .then((response) => {
                        if (response.statusText === 'OK') {
                            window.location.href = `${localhost}/dat-tour-thanh-cong`;
                        }
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            }, 4000);
        }
    }
    handleHinhThucThanhToan() {
        const HTTT = document.getElementsByClassName('hinh-thuc-thanh-toan');
        for (let i = 0; i < HTTT.length; i++) {
            if (HTTT[i].checked) {
                this.setState({
                    HinhThucThanhToan: HTTT[i].value
                }, () => { });
            }
        }
    }
    handleTenKhachHang(e) {
        const value = e.target.value;
        this.setState({
            TenKH: value
        }, () => { });
    }
    handleDienThoai(e) {
        const value = e.target.value;
        this.setState({
            SDTKH: value
        }, () => { });
    }
    handleEmail(e) {
        const value = e.target.value;
        this.setState({
            EmailKH: value
        }, () => { });
    }
    handleKhac(e) {
        const value = e.target.value;
        this.setState({
            Khac: value
        }, () => { });
    }
    render() {
        const { dataFetched, data } = this.props.dataCTTour;
        const { SLK, GiaTour } = queryString.parse(this.props.location.search);
        if (dataFetched === false) {
            return (
                <div style={{ marginTop: '20%' }}>
                    <Loading />
                </div>
            );
        }
        return (
            <div className="wrap-thanh-toan">
                <div className="top-thanh-toan">
                    <h1>Thanh toán</h1>
                </div>
                <div className="mid-thanh-toan">
                    <div className="wrap-right-mid-thanh-toan clear-fix">
                        {/* wrap-right-mid-thanh-toan */}
                        <div className="wrap-detail-child clear-fix">
                            <DetailTourChildThanhToan
                                maTour={data.ChiTietTour[0].MaTour}
                                imageTour={data.ChiTietTour[0].AnhTour}
                                nameTour={data.ChiTietTour[0].TenTour}
                                timeTour={data.ChiTietTour[0].ThoiGian}
                                arrPhuongTien={data.PhuongTien}
                                keHoachTour={data.ChiTietTour[0].GioiThieu}
                                slKhach={SLK}
                                giaTour={GiaTour}
                                menhGiaTien="VND"
                                namePlace1={data.DiaDiem[0].TenDD}
                                showRibbonAndPlace="yes"
                            />
                        </div>
                        <div className="wrap-bot-left-mid-thanh-toan-tb-pc clear-fix">
                            <p>Sau khi hoàn tất đơn hàng, nhân viên của AlAs.com sẽ liên hệ với quý khách để xác nhận tình trạng tour.</p><br />
                            <p>Mọi thắc mắc, xin Quý khách vui lòng liên hệ tổng đài <span style={{ color: '#003C71', fontWeight: 'bold' }}>(0350) 3 123 456</span></p>
                        </div>
                    </div>
                    <div className="wrap-left-mid-thanh-toan clear-fix">
                        <div id="wrapTopLeftMidThanhToan" className="wrap-top-left-mid-thanh-toan">
                            <div className="title-top-left-mid-thanh-toan">
                                <h3>PHƯƠNG THỨC THANH TOÁN</h3>
                            </div>
                            <div className="form1-top-left-mid-thanh-toan">
                                <input
                                    type="radio"
                                    onClick={this.handleHinhThucThanhToan} id="van-phong"
                                    className="hinh-thuc-thanh-toan" name="kieuThanhToan"
                                    defaultValue="Thanh toán tại văn phòng ALAs"
                                />
                                <label htmlFor="van-phong">
                                    Thanh toán tại văn phòng ALA's
                                </label><br /><br />

                                <input
                                    type="radio" id="tan-noi"
                                    onClick={this.handleHinhThucThanhToan}
                                    className="hinh-thuc-thanh-toan" name="kieuThanhToan"
                                    defaultValue="Thu tiền tận nơi"
                                />
                                <label htmlFor="tan-noi">Thu tiền tận nơi</label><br /><br />

                                <input
                                    type="radio" id="chuyen-khoan"
                                    onClick={this.handleHinhThucThanhToan}
                                    className="hinh-thuc-thanh-toan" name="kieuThanhToan"
                                    defaultValue="Chuyển khoản ngân hàng"
                                />
                                <label htmlFor="chuyen-khoan">Chuyển khoản ngân hàng</label>
                            </div>
                            <div className="form2-top-left-mid-thanh-toan">
                                <h3>THÔNG TIN LIÊN HỆ</h3>
                                <form>
                                    <p>Họ & Tên <span>*</span></p>
                                    <input
                                        onChange={this.handleTenKhachHang} type="text"
                                        id="tenKH" ref="firstname"
                                    /><br />
                                    <p>Điện thoại <span>*</span></p>
                                    <input
                                        onChange={this.handleDienThoai} type="number"
                                        id="sdtKH" ref="lastname"
                                    /> <br />
                                    <p>Email <span>*</span></p>
                                    <input
                                        onChange={this.handleEmail} type="text"
                                        id="emailKH" ref="lastname"
                                    /> <br />
                                    <textarea
                                        onChange={this.handleKhac} rows="4" cols="50"
                                        placeholder="Yêu cầu khác ..."
                                    />
                                    <i className="fa fa-lock" aria-hidden="true" />
                                    <span id="promiseKH">
                                        ALA's.com cam kết bảo mật thông tin của quý khách
                                    </span>
                                    <button onClick={this.handleDatTour} id="btn-hoan-tat-dat-tour">
                                        HOÀN TẤT ĐẶT TOUR 
                                        <i className="fa fa-check" aria-hidden="true" /> 
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div className="wrap-bot-left-mid-thanh-toan-mb">
                            <p>Sau khi hoàn tất đơn hàng, nhân viên của AlAs.com sẽ liên hệ với quý khách để xác nhận tình trạng tour.</p><br />
                            <p>Mọi thắc mắc, xin Quý khách vui lòng liên hệ tổng đài <span style={{ color: '#003C71', fontWeight: 'bold' }}>(0350) 3 123 456</span></p>
                        </div>
                    </div>
                </div>
                <div className="bot-thanh-toan">
                    Bot Thanh Toan
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state) => ({ dataCTTour: state.dataCTTour });
const mapDispatchToProps = (dispatch) => bindActionCreators({
    fetchData
}, dispatch);

module.exports = connect(mapStateToProps, mapDispatchToProps)(ThanhToan);
