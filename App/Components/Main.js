import React from 'react';
import io from 'socket.io-client';
import { renderRoutes } from 'react-router-config';

import Header from './Header/Header';
import { localhost } from '../../configure';

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            socket: null
        };
    }
    componentDidMount() {
        const socket = io(`${localhost}`);
        this.setState({
            socket
        });
    }
    render() {
        const { socket } = this.state;
        return (
            <div className="wrapper">
                <Header />
                <main>
                    {renderRoutes(this.props.route.routes, { socket })}
                </main>
            </div>
        );
    }
}
module.exports = Main;
