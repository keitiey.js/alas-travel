import React from 'react';

class GioiThieu extends React.Component {
    render() {
        return (
            <div className="gioi-thieu">
                <div className="gioithieu-detail clear-fix">
                    <i className="fa fa-check-circle fa-2x" aria-hidden="true" />
                    <p> Tour chọn lọc <strong>chất lượng nhất</strong></p>
                </div>
                <div className="gioithieu-detail">
                    <i className="fa fa-check-circle fa-2x" aria-hidden="true" />
                    <p> Bảo đảm <strong>giá tốt nhất</strong></p>
                </div>
                <div className="gioithieu-detail">
                    <i className="fa fa-check-circle fa-2x" aria-hidden="true" />
                    <p> Đội ngũ tư vấn <strong>chi tiết và tận tình</strong></p>
                </div>
            </div>
        );
    }
}

module.exports = GioiThieu;
