import React from 'react';
import Slider from 'react-slick';

class SliderImage extends React.Component {
    render() {
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            lazyLoad: true,
            arrows: false
        };
        const { image1, image2, image3 } = this.props;
        const img1 = `images/${image1}`;
        const img2 = `images/${image2}`;
        const img3 = `images/${image3}`;
        return (
            <div className="wrap-slide-ks clear-fix">
                <Slider {...settings}>
                    <div className="wrap-slide-img">
                        <h3>3N2Đ + Vé máy bay khứ hồi</h3>
                        <img src={img1} alt="" />
                    </div>
                    <div className="wrap-slide-img">
                        <h3>3N2Đ + Vé máy bay khứ hồi</h3>
                        <img src={img2} alt="" />
                    </div>
                    <div className="wrap-slide-img">
                        <h3>3N2Đ + Vé máy bay khứ hồi</h3>
                        <img src={img3} alt="" />
                    </div>
                </Slider>
            </div>
        );
    }
}
module.exports = SliderImage;
