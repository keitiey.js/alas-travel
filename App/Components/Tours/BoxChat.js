import React from 'react';

class BoxChat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isToggleControlChat: false,
            isToggleBoxChat: false
        };
        this.handleToggleControlChat = this.handleToggleControlChat.bind(this);
        this.handleOpenBoxChat = this.handleOpenBoxChat.bind(this);
        this.handleExitBoxChat = this.handleExitBoxChat.bind(this);
        this.checkInputValue = this.checkInputValue.bind(this);
    }
    componentDidMount() {
        this.props.socket.on('Admin_chat', (data) => {
            const contentBoxChat = document.getElementById('contentBoxChat');
            const pTagCSKH = document.createElement('p');
            pTagCSKH.style.paddingBottom = '1%';
            pTagCSKH.innerHTML = `<span style="color: blue">CSKH:</span> ${data}`;
            contentBoxChat.appendChild(pTagCSKH);
        });
        document.getElementById('btnSendMess').addEventListener('click', () => {
            const contentMessUser = document.getElementById('inputMess');
            this.props.socket.emit('User_send_message', contentMessUser.value);

            const contentBoxChat = document.getElementById('contentBoxChat');
            const pTagUser = document.createElement('p');
            pTagUser.style.paddingBottom = '1%';
            pTagUser.innerHTML = `
                <span style="color: red">Customer:</span> ${contentMessUser.value}
            `;
            contentBoxChat.appendChild(pTagUser);
            contentMessUser.value = '';
        });
    }
    componentWillUpdate(nextProps, nextState) {
        const wrapPushInfo = document.getElementById('wrapPushInfo');
        const wrapBoxChat = document.getElementById('wrapBoxChat');
        if (nextState.isToggleControlChat === true) {
            wrapPushInfo.style.display = 'inline';
        } else {
            wrapPushInfo.style.display = 'none';
        }
        if (nextState.isToggleBoxChat === true) {
            wrapBoxChat.style.display = 'inline';
        } else {
            wrapBoxChat.style.display = 'none';
        }
    }
    handleToggleControlChat(e) {
        e.preventDefault();
        this.setState({
            isToggleControlChat: !this.state.isToggleControlChat
        });
    }
    handleOpenBoxChat(e) {
        e.preventDefault();
        const nameUser = document.getElementById('nameUser');
        const sdtUser = document.getElementById('sdtUser');
        if (nameUser.value !== '' && sdtUser.value !== '') {
            this.setState({
                isToggleBoxChat: !this.state.isToggleBoxChat
            });
            this.props.socket.emit('User_join_room', { 
                nameRoom: `${nameUser.value}_${sdtUser.value}`, 
                nameUser: nameUser.value, 
                sdtUser: sdtUser.value 
            });
        } else {
            if (nameUser.value === '') {
                nameUser.style.border = '1px solid red';
            }
            if (sdtUser.value === '') {
                sdtUser.style.border = '1px solid red';
            }
        }
    }
    handleExitBoxChat(e) {
        e.preventDefault();
        this.setState({
            isToggleControlChat: !this.state.isToggleControlChat,
            isToggleBoxChat: !this.state.isToggleBoxChat
        });
        this.props.socket.emit('User_out_room');
    }
    checkInputValue(idInput) {
        const input = document.getElementById(idInput);
        if (idInput.value !== '') {
            input.style.border = '1px solid #CCCEDB';
        }        
    }
    render() {
        return (
            <div>
                <div className="wrap-control-box-chat" onClick={this.handleToggleControlChat}>
                    <div className="wrap-left-control-box-chat clear-fix">
                        <span>CHAT với ALA's</span>
                    </div>
                    <div className="wrap-right-control-box-chat clear-fix">
                        <i className="fa fa-chevron-up fa-1x" aria-hidden="true" />
                    </div>
                </div>  
                <div style={{ display: 'none' }} id="wrapPushInfo" className="wrap-push-info">
                    <div className="head-push-info">
                        <div 
                            onClick={this.handleToggleControlChat} 
                            id="exitBoxChat" 
                            className="exit-box-chat clear-fix"
                        >
                            <i 
                                className="fa fa-chevron-down" style={{ marginRight: '10px' }} 
                                aria-hidden="true" 
                            />
                        </div>
                    </div>
                    <div className="content-box-push-info">
                        <input 
                            type="text" onChange={() => this.checkInputValue('nameUser')} 
                            id="nameUser" placeholder="Vui lòng nhập tên " 
                        />
                        <input 
                            type="number" 
                            onChange={() => this.checkInputValue('sdtUser')} 
                            id="sdtUser" placeholder="Vui lòng nhập SDT " 
                        />
                    </div>
                    <div className="bottom-push-info">
                        <button onClick={this.handleOpenBoxChat} id="btnJoinChat">Bắt đầu</button>
                    </div>
                </div>
                 <div style={{ display: 'none' }} id="wrapBoxChat" className="wrap-box-chat">
                    <div className="head-box-chat">
                        <div onClick={this.handleExitBoxChat} className="exit-box-chat clear-fix">
                            <i className="fa fa-times" aria-hidden="true" />
                        </div>
                    </div>
                    <div id="contentBoxChat" className="content-box-chat" />
                    <div className="bottom-box-chat">
                        <input type="text" placeholder="Nhập tin nhắn của bạn" id="inputMess" />
                        <button id="btnSendMess">Gửi</button>
                    </div>
                </div> 
            </div>
        );
    }
}
module.exports = BoxChat;
