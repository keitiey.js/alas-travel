import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Link from 'react-router-dom/Link';
// import queryString from 'query-string';

// Components
import Banner from './Banner/Banner';
import GioiThieu from './GioiThieu';
import ToursDaXem from './ToursDaXem';
import DiemDuLichTour from './DiemDuLichTour';
import { DetailTourChild } from './DetailTourChild.js';
import BoxChat from './BoxChat.js';

import { fetchDataTrangChu, fetchData } from '../../Redux/action';

const Loading = require('react-loading-animation');

class Tours extends React.Component {
    static fetchDataTrangChu(store) {
        return store.dispatch(fetchDataTrangChu());
    }
    constructor(props) {
        super(props);
        this.state = {
            countDiv: null
        };
        this.handleClick = this.handleClick.bind(this);
        this.handleState = this.handleState.bind(this);
    }
    componentDidMount() {
        this.props.fetchDataTrangChu();
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.dataTrangChu.data != null) {
            this.setState({ countDiv: nextProps.dataTrangChu.data.ToursDaXem.length });
        }
    }
    componentDidUpdate() {
        const { countDiv } = this.state;
        if (countDiv === 0) {
            setTimeout(() => {
                document.getElementById('wrapWatched').classList.add('hidden');
            }, 500);
        }
    }
    handleState() {
        this.setState({ countDiv: this.state.countDiv - 1 });
    }

    handleClick(e, url) {
        e.preventDefault();
        this.context.router.transitionTo(url);
    }
    render() {
        const { dataFetched, data } = this.props.dataTrangChu;
        if (dataFetched === false) {
            return (
                <div style={{ marginTop: '20%' }}>
                    <Loading />
                </div>
            );
        }
        return (
            <div>
                <Banner
                    socket={this.props.socket} kieuBanner="tours"
                    pathBanner="banner-khachsan.jpg"
                />
                {/* Content */}
                <div id="content-tour" className="clear-fix">
                    <div id="content-tour-top" className="clear-fix">
                        <GioiThieu />
                    </div>
                    <div id="wrapWatched" className="wrap-watched clear-fix">
                        <div className="wrap-content-watched clear-fix">
                            <h3>Tours du lịch bạn đã xem gần đây</h3>
                            {data.ToursDaXem.map((e, i) => (
                                <ToursDaXem
                                    key={i}
                                    AnhTour={e.AnhTour}
                                    TenTour={e.TenTour}
                                    GiaTour={e.GiaTour}
                                    MaTour={e.MaTour}
                                    childHandleState={this.handleState}
                                />
                            ))}
                        </div>
                    </div>
                    <div className="content-tour-bottom clear-fix">
                        <div>
                            <h3 className="tieude">
                                <Link 
                                    onClick={() => { window.location.reload(); }}
                                    to="/xem-them/where?Go=Deal" 
                                >
                                    <span>ALA's DEALS</span>
                                </Link>
                            </h3>
                            {data.Deal.map((e, i) =>
                                <Link
                                    onClick={() => { window.location.reload(); }}
                                    key={i}
                                    to={`/chi-tiet-tour?MaTour=${e.MaTour}`}
                                >
                                    <DetailTourChild
                                        imageTour={e.AnhTour}
                                        nameTour={e.TenTour}
                                        timeTour={e.ThoiGian}
                                        keHoachTour={e.GioiThieu}
                                        giaTour={e.GiaTour}
                                        menhGiaTien="VND"
                                        namePlace1={e.TenDD}
                                        ribbon="Deal giá tốt và shock"
                                        showRibbonAndPlace="yes"
                                    />
                                </Link>
                            )}
                        </div>
                    </div>
                    <div className="content-tour-bottom clear-fix">
                        <div>
                            <h3 className="tieude">
                                <Link 
                                    onClick={() => { window.location.reload(); }}
                                    to="/"
                                >
                                    <span>Khuyến mãi ngày Hè</span>
                                </Link>
                            </h3>
                            {data.KhuyenMai.map((e, i) =>
                                <Link
                                    onClick={() => { window.location.reload(); }}
                                    key={i}
                                    to={`/chi-tiet-tour?MaTour=${e.MaTour}`}
                                >
                                    <DetailTourChild
                                        imageTour={e.AnhTour}
                                        nameTour={e.TenTour}
                                        timeTour={e.ThoiGian}
                                        keHoachTour={e.GioiThieu}
                                        giaTour={e.GiaTour}
                                        menhGiaTien="VND"
                                        namePlace1={e.TenDD}
                                        ribbon={e.TTKM}
                                        showRibbonAndPlace="yes"
                                    />
                                </Link>
                            )}
                        </div>
                    </div>

                    {/* TOUR NƯỚC NGOÀI */}
                    <div className="content-tour-bottom clear-fix">
                        <div>
                            <h3 className="tieude">
                                <Link 
                                    onClick={() => { window.location.reload(); }}
                                    to="/xem-them/where?Go=NgoaiNuoc"
                                >
                                    <span>Bạn thích tour nước ngoài?</span>
                                </Link>
                            </h3>
                            {data.NgoaiNuoc.map((e, i) =>
                                <Link 
                                    onClick={() => { window.location.reload(); }}
                                    key={i}
                                    to={`/chi-tiet-tour?MaTour=${e.MaTour}`}
                                >
                                    <DetailTourChild
                                        imageTour={e.AnhTour}
                                        nameTour={e.TenTour}
                                        timeTour={e.ThoiGian}
                                        keHoachTour={e.GioiThieu}
                                        giaTour={e.GiaTour}
                                        menhGiaTien="VND"
                                        namePlace1={e.TenDD}
                                        ribbon="Deal giá tốt và shock"
                                        showRibbonAndPlace="no"
                                    />
                                </Link>
                            )}
                        </div>
                    </div>

                    {/* TOUR TRONG NƯỚC */}
                    <div className="content-tour-bottom clear-fix">
                        <div>
                            <h3 className="tieude">
                                <Link 
                                    onClick={() => { window.location.reload(); }}
                                    to="/xem-them/where?Go=TrongNuoc"
                                >
                                    <span>Bạn thích tour trong nước?</span>
                                </Link>
                            </h3>
                            {data.TrongNuoc.map((e, i) =>
                                <Link 
                                    onClick={() => { window.location.reload(); }}
                                    key={i}
                                    to={`/chi-tiet-tour?MaTour=${e.MaTour}`}
                                >
                                    <DetailTourChild
                                        imageTour={e.AnhTour}
                                        nameTour={e.TenTour}
                                        timeTour={e.ThoiGian}
                                        keHoachTour={e.GioiThieu}
                                        giaTour={e.GiaTour}
                                        menhGiaTien="VND"
                                        namePlace1={e.TenDD}
                                        ribbon="Deal giá tốt và shock"
                                        showRibbonAndPlace="no"
                                    />
                                </Link>
                            )}
                        </div>
                    </div>

                    {/* TOUR MỚI */}
                    <div className="content-tour-bottom clear-fix">
                        <div>
                            <h3 className="tieude">
                                <Link 
                                    onClick={() => { window.location.reload(); }}
                                    to="/xem-them/where?Go=TourMoi" 
                                >
                                    <span>Tour mới tại ALA's</span>
                                </Link>
                            </h3>
                            {data.TourMoi.map((e, i) =>
                                <Link 
                                    onClick={() => { window.location.reload(); }}
                                    key={i} 
                                    to={`/chi-tiet-tour?MaTour=${e.MaTour}`}
                                >
                                    <DetailTourChild
                                        imageTour={e.AnhTour}
                                        nameTour={e.TenTour}
                                        timeTour={e.ThoiGian}
                                        keHoachTour={e.GioiThieu}
                                        giaTour={e.GiaTour}
                                        menhGiaTien="VND"
                                        namePlace1={e.TenDD}
                                        ribbon="Deal giá tốt và shock"
                                        showRibbonAndPlace="no"
                                    />
                                </Link>
                            )}
                        </div>
                    </div>

                    <div className="content-tour-bottom clear-fix">
                        <h3 className="tieude">
                            <a href="">
                                <span>Các điểm du lịch phổ biến</span>
                            </a>
                        </h3>
                        {data.DiemPhoBien.map((e, i) => (
                                <Link 
                                    onClick={() => { window.location.reload(); }}
                                    key={i}
                                    to={`/xem-them/where?Go=${e.MaDD}`}
                                >
                                    <DiemDuLichTour
                                        imagePlace={e.AnhDD}
                                        namePlace={e.TenDD}
                                    />
                                </Link>
                            ))}
                    </div>
                </div>
                {/* End Content */}
                {/* Footer */}
                <div id="footer" className="clear-fix">
                    <BoxChat socket={this.props.socket} />
                    <div className="content-ks-bottom clear-fix">
                        <div className="content-ks-bottom-left">
                            <div className="wrap-quangcao clear-fix">
                                <h3>5 Lí do chọn ALA's</h3>
                                <div className="wrap-lido clear-fix">
                                    <img src="images/money.svg" alt="" />
                                    <span>Đảm bảo giá tốt nhất</span>
                                    <p>ALAs.com luôn có giá trị tốt nhất thị trường và cam kết <strong>hoàn tiền chênh lệch</strong> nếu bạn có giá thấp hơn</p>
                                </div>
                                <div className="wrap-lido clear-fix">
                                    <img src="images/money.svg" alt="" />
                                    <span>Đảm bảo giá tốt nhất</span>
                                    <p>ALAs.com luôn có giá trị tốt nhất thị trường và cam kết <strong>hoàn tiền chênh lệch</strong> nếu bạn có giá thấp hơn</p>
                                </div>
                                <div className="wrap-lido clear-fix">
                                    <img src="images/money.svg" alt="" />
                                    <span>Đảm bảo giá tốt nhất</span>
                                    <p>ALAs.com luôn có giá trị tốt nhất thị trường và cam kết <strong>hoàn tiền chênh lệch</strong> nếu bạn có giá thấp hơn</p>
                                </div>
                            </div>
                        </div>
                        <div className="content-ks-bottom-right">
                            <div className="wrap-gioithieu-web">
                                <h3>ALA's - Thổ địa du lịch</h3>
                                <div className="content-gioithieu-web">
                                    <p>Cùng ALAs.com, bạn không còn bỏ lỡ bất cứ điều tuyệt vời nào trong chuyến du lịch của mình. Chúng tôi lựa chọn cho bạn khách sạn phù hợp, tour đặc sắc, thông tin du lịch chi tiết kèm mức giá hấp dẫn.
                                        <br />
                                        Dù bất cứ nơi đâu, hãy để ALAs.com giúp chuyến đi của bạn trở nên hoàn hảo nhất.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Footer */}
            </div>
        );
    }
}

const mapStateToProps = (state) => ({ dataTrangChu: state.dataTrangChu });
const mapDispatchToProps = (dispatch) => bindActionCreators({ 
    fetchDataTrangChu, fetchData 
}, dispatch);

module.exports = connect(mapStateToProps, mapDispatchToProps)(Tours);
