import React from 'react';

class DiemDuLichTour extends React.Component {
    render() {
        const { imagePlace, namePlace } = this.props;

        const img = `upload/tours/${imagePlace}`;
        return (
            <div className="diadiem-tour">
                <img src={img} alt="" />
                <div className="wrap-diadiem-tour">
                    <div className="clear-fix ten-diadiem-tour">
                        <i className="fa fa-map-marker fa-1x" aria-hidden="true" /> 
                        {namePlace}
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = DiemDuLichTour;
