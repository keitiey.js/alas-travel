import React from 'react';
import { numberToMoney } from '../../../global';
import { localhost } from '../../../configure';

class DetailTourChild extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            GiaTour: ''
        });
    }
    componentDidMount() {
        const { giaTour } = this.props;
        this.setState({ GiaTour: numberToMoney(giaTour) });
    }
    render() {
        const { imageTour, showRibbonAndPlace, slKhach, 
            keHoachTour, nameTour, timeTour, menhGiaTien,
            namePlace1  
        } = this.props;
        const { GiaTour } = this.state;
        const img = `${localhost}/upload/tours/${imageTour}`;
        let aRibbon = '';
        let aPlace = '';
        if (showRibbonAndPlace === 'yes') {
            aRibbon = (
            <div className="ribbon-tour">
                <h2>{this.props.ribbon}</h2>
            </div>
            );
            aPlace = (
                <div className="clear-fix wrap-diadiem-tour-1">
                    <div className="clear-fix ten-diadiem-tour-1">
                        <i 
                            className="fa fa-map-marker fa-1x" 
                            aria-hidden="true"
                        /> {namePlace1}
                    </div>
                </div>
            );
        } else {
            aRibbon = null;
            aPlace = (
                <div className="clear-fix wrap-diadiem-tour-1">
                    <div className="clear-fix ten-diadiem-tour-1">
                        <i
                            className="fa fa-map-marker fa-1x" 
                            aria-hidden="true"
                        /> {namePlace1}
                    </div>
                </div>
            );
        }
        return (
            <div className="tours clear-fix">
                <div className="wrap-images">
                    {aRibbon}
                    <img src={img} alt="" />
                    {aPlace}
                </div>
                <div className="tour-detail">
                    <div className="name-tour">
                        <span>{nameTour}</span>
                    </div>
                    <div className="thoigian-phuongtien-tour clear-fix">
                        <div className="thoigian-tour">
                            <i 
                                className="fa fa-clock-o" aria-hidden="true"
                            /> {timeTour}
                        </div>
                        <div className="phuongtien-tour" />
                    </div>
                    <div className="khoihanh-tour" style={{ overflow: 'hidden' }}>
                        <i
                            className="fa fa-building" aria-hidden="true"
                        />
                        KH: {keHoachTour} | {slKhach != null ? slKhach : '1'} Khách
                    </div>
                    <div className="gia-tour">
                        <span>{GiaTour} {menhGiaTien}</span>
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = { DetailTourChild };
