import React from 'react';
import axios from 'axios';
import Link from 'react-router-dom/Link';

import { localhost } from '../../../configure';
import { numberToMoney } from '../../../global';

class ToursDaXem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            giaTour: numberToMoney(props.GiaTour),
            anhTour: `/upload/tours/${props.AnhTour}`,
            tenTour: props.TenTour,
            menhGia: 'VND'
        };
        this.handleDelWatched = this.handleDelWatched.bind(this);
    }
    componentDidUpdate() {
        const { anhTour, tenTour, giaTour } = this.state;
        if (anhTour === '' && tenTour === '' && giaTour === '') {
            setTimeout(() => {
                document.getElementById(`${this.props.MaTour}`).classList.add('hidden');
            }, 500);
        }
    }
    handleDelWatched(MaTour) {
        this.props.childHandleState();
        this.setState({
            giaTour: '',
            anhTour: '',
            tenTour: '',
            menhGia: ''
        });
        axios.post(`${localhost}/api/tours-da-xem/delete`, { MaTour })
        .then((response) => console.log(response))
        .catch((error) => console.log(error));    
    } 
    render() {
        const { MaTour } = this.props;
        return (
            <div className="wrap-detail-watched clear-fix" id={`${MaTour}`}>
                <div className="left-detail-watched clear-fix">
                    <img src={this.state.anhTour} alt="" />
                </div>
                <div className="mid-detail-watched clear-fix">
                    <div className="top-mid-detail-watched clear-fix">
                        <Link 
                            onClick={() => { window.location.reload(); }}
                            to={`/chi-tiet-tour?MaTour=${MaTour}`}
                        ><h3>{this.state.tenTour}</h3></Link>
                    </div>
                    <div className="bot-mid-detail-watched clear-fix">
                        <span>{this.state.giaTour} {this.state.menhGia}</span>
                    </div>
                </div>
                <div className="right-detail-watched clear-fix">
                    <i 
                        onClick={() => this.handleDelWatched(MaTour)} 
                        className="fa fa-times" aria-hidden="true" 
                    />
                </div>
            </div>
        );
    }
}
module.exports = ToursDaXem;
