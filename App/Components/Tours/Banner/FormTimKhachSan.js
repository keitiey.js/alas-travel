import React from 'react';

class FormTimKhachSan extends React.Component {
    render() {
        return (
            <div id="form-tim-khachsan">
                <img src="images/plane.svg" alt="" />
                <div id="wrap-form">
                    <div className="clear-fix form-detail">
                        <p>Tìm nhanh khách sạn</p>
                        <input type="text" placeholder="Nhập tên thành phố, khu vực,..." />
                    </div>
                    <div className="clear-fix form-detail form-detail-pc">
                        <p>Ngày nhận phòng</p>
                        <input type="date" />
                    </div>
                    <div className="clear-fix form-detail form-detail-pc">
                        <p>Ngày trả phòng</p>
                        <input type="date" />
                    </div>
                    <div className="clear-fix form-button form-detail-pc">
                        <button>TÌM KIẾM</button>
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = FormTimKhachSan;
