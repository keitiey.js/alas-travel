import React from 'react';

import FormTimTour from './FormTimTour';
import FormTimKhachSan from './FormTimKhachSan';

class Banner extends React.Component {
    componentDidMount() {
        const cNodes =  document.getElementById('banner').childNodes; //eslint-disable-line
        if (this.props.kieuBanner === 'khachsan') {
            cNodes[0].style.minHeight = '380px';
        }
    }
    render() {
        const { socket, pathBanner, kieuBanner } = this.props;
        let aForm = null;
        if (kieuBanner === 'tours') {
            aForm = <FormTimTour socket={socket} />;
        }
        if (kieuBanner === 'khachsan') {
            aForm = <FormTimKhachSan />;
        }
        const banner = `images/${pathBanner}`;
        return (
            <div id="banner" className="clear-fix">
                <img src={banner} alt="" />
                {aForm}
            </div>
        );
    }
}

module.exports = Banner;
