import React from 'react';

import { localhost } from '../../../../configure';

class FormTimTour extends React.Component {
    constructor(props) {
        super(props);
        this.handleSearch = this.handleSearch.bind(this);
        this.handleClickSearch = this.handleClickSearch.bind(this);
        this.handleClickInput = this.handleClickInput.bind(this);
    }
    componentDidMount() {
        this.props.socket.emit('Client-send-data-banner', 'Luot truy cap moi');
    }
    handleClickSearch() {
        const inputSearch = document.getElementById('input-timkiem').value;
        window.location.href = `${localhost}/xem-them?Go=${inputSearch}`;   
        window.location.reload();     
    }
    handleClickInput() {
        if (document.getElementById('suggestTour').style.display === 'none') {
            document.getElementById('suggestTour').style.display = 'block';
        } else {
            document.getElementById('suggestTour').style.display = 'none';
        }
    }
    handleSearch() {
        const value = document.getElementById('input-timkiem').value;
        this.props.socket.emit('Client-send-data-banner', `${value}`);
        this.props.socket.on('Server-send-data-banner', (data) => {
            document.getElementById('suggestTour').style.display = 'block';
            document.getElementById('diadiemSearch').innerHTML = '';
            for (let i = 0; i < data.DiaDiem.length; i++) {
                document.getElementById('diadiemSearch').innerHTML += `
                    <a 
                        class="goDiaDiem" 
                        href="${localhost}/xem-them/where?Go=${data.DiaDiem[i].MaDD}"
                    >
                        <div class="DiaDiem-search">
                        <span style="font-size: 14px;">${data.DiaDiem[i].TenDD}</span>
                    </div></a>                
                `;
            }
            for (let i = 0; i < document.getElementsByClassName('goDiaDiem').length; i++) {
                document.getElementsByClassName('goDiaDiem')[i].addEventListener('click', () => {
                    window.location.reload();
                });
            }
            document.getElementById('tourSearch').innerHTML = '';
            for (let i = 0; i < data.Tours.length; i++) {
                document.getElementById('tourSearch').innerHTML += `
                    <a 
                        class="goTour" 
                        href="${localhost}/chi-tiet-tour?MaTour=${data.Tours[i].MaTour}"
                    >
                        <div class="Tour-search">
                        <span style="font-size: 14px;">
                            ${data.Tours[i].MaTour} - ${data.Tours[i].TenTour}
                        </span>
                    </div></a>                
                `;
            }
            for (let i = 0; i < document.getElementsByClassName('goTour').length; i++) {
                document.getElementsByClassName('goTour')[i].addEventListener('click', () => {
                    window.location.reload();
                });
            }
        });
    }
    render() {
        return (
            <div id="form-tim-tour">
                <img src="images/plane.svg" alt="" /> 
                <h3>Đặt tours du lịch!</h3>
                <div className="row timkiem">
                    <input 
                        onClick={this.handleClickInput} 
                        type="text" onChange={this.handleSearch} 
                        id="input-timkiem" 
                        placeholder="Tên thành phố, khu vực,..." 
                    />
                    <button id="btn-timkiem" onClick={this.handleClickSearch}>
                        <i className="fa fa-search fa-2x" aria-hidden="true" />
                    </button>
                    <div style={{ display: 'none' }} id="suggestTour">
                        <div id="content-suggestTour" className="clear-fix">
                            <div>
                                <span style={{ marginBottom: '10px' }}>
                                    <b><i className="fa fa-map-marker" aria-hidden="true" /> 
                                        ĐỊA ĐIỂM
                                    </b>
                                </span>
                                <div id="diadiemSearch" />
                            </div>
                            <div>
                                <span style={{ marginBottom: '10px' }}>
                                    <b><i className="fa fa-map-marker" aria-hidden="true" /> 
                                        TOURS
                                    </b>
                                </span>
                                <div id="tourSearch" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = FormTimTour;
