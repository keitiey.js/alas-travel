import React from 'react';
import Link from 'react-router-dom/Link';

class DatTourThanhCong extends React.Component {
    render() {
        return (
            <div className="wrap-dat-tour-thanh-cong">
                <div className="content-dat-tour-thanh-cong">
                    <div className="top-content-dat-tour-thanh-cong">
                        <img src="/images/dat-tour-thanh-cong.jpg" alt="" />
                    </div>
                    <div className="bot-content-dat-tour-thanh-cong">
                        <div className="content-bot-content">
                            <h1>Đặt tour thành công!</h1>
                            <p>
                                Chúc mừng quý khách đã đặt Tour thành công. Email xác nhận đã được gửi đến quý khách.<br />
                                Nhân viên Chăm Sóc Khách Hàng của <span><b>ALA's</b></span> sẽ liên hệ với quý khách để xác nhận tình trạng Tour.<br /> 
                                <span><b>ALA's</b></span> xin chân thành cảm ơn quý khách đã chọn ALA's Tours.
                            </p>
                            <Link 
                                onClick={() => { window.location.reload(); }}
                                to="/"
                            ><button>TRỞ VỀ TRANG CHỦ</button></Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = DatTourThanhCong;
