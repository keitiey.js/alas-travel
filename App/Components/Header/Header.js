import React from 'react';
import { connect } from 'react-redux';

import MenuMobile from './MenuMobile';
import MenuPC from './MenuPC';
import Logo from './Logo';

import { localhost } from '../../../configure';

class Header extends React.Component {
    toggleMenu() {
        const { dispatch } = this.props;
        dispatch({
            type: 'TOGGLE_MENU_BAR'
        });
    }
    render() {
        const { isToggleMenuBar } = this.props;
        let toggleMenuMoblie = null;
        if (isToggleMenuBar === true) {
            toggleMenuMoblie = <MenuMobile />;
        } else {
            toggleMenuMoblie = null;
        }
        return (
            <div>
                <div id="header" className="clear-fix">
                    <Logo pathLogo={`${localhost}/images/logo.svg`} />
                    <div className="lienhe-menubar clear-fix">
                        <MenuPC soDienThoai="(0350) 3 123 456" />
                        <div id="menu-bar">
                            <button onClick={this.toggleMenu.bind(this)}>
                                <i className="fa fa-bars" aria-hidden="true" />
                            </button>
                        </div>
                    </div>
                </div> 
                {toggleMenuMoblie}
            </div>
        );
    }
}
const mapStateToProps = (state) => ({
        isToggleMenuBar: state.isToggleMenuBar
});

module.exports = connect(mapStateToProps)(Header);
