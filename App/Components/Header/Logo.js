import React from 'react';
import Link from 'react-router-dom/Link';

class Logo extends React.Component {
    render() {
        const logo = this.props.pathLogo;
        return (
            <div id="logo">
                <Link onClick={() => { window.location.reload(); }} to="/" >
                    <img src={logo} alt="" />
                </Link>
            </div>
        );
    }
}

module.exports = Logo;
