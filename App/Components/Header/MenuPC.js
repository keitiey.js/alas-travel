import React from 'react';

class MenuPC extends React.Component {
    render() {
        const { soDienThoai } = this.props;
        return (
            <div>
                <div id="menu-pc-left" className="clear-fix">
                    <ul>
                        <span className="slogan-pc">
                            ALA's - A destination for the new millennium
                        </span>
                    </ul>
                </div>
                <div id="menu-pc-right" className="clear-fix">
                    <ul />
                </div>
                <div id="lien-he">
                    <i className="fa fa-phone fa-2x" aria-hidden="true" />
                    <a href="">{soDienThoai} </a>
                    <p> Từ <strong>8h30 - 18h</strong> hàng ngày </p>
                </div>                
            </div>            
        );
    }
}

module.exports = MenuPC;
