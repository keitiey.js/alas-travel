import React from 'react';
import Link from 'react-router-dom/Link';

class MenuMobile extends React.Component {
    render() {
        return (
            <div id="menu-mobile" className="clear-fix">
                <ul>
                    <li>
                        <Link onClick={() => { window.location.reload(); }} to="/" >KHÁCH SẠN</Link>
                    </li>
                    <li>
                        <Link onClick={() => { window.location.reload(); }} to="/" >TOURS</Link>
                    </li>
                    <li>
                        <Link onClick={() => { window.location.reload(); }} to="#" >CẨM NANG DU LỊCH</Link></li>
                    <li>
                        <Link onClick={() => { window.location.reload(); }} to="#" >REVIEWS</Link></li> 
                </ul>
            </div>             
        );
    }
}

module.exports = MenuMobile;
