import Main from './Components/Main';
import Tours from './Components/Tours/Tours';
import ChiTietTour from './Components/ChiTietTour/ChiTietTour';
import ThanhToan from './Components/ThanhToan/ThanhToan';
import XemThemTour from './Components/XemThemTour/XemThemTour';
import DatTourThanhCong from './Components/DatTourThanhCong/DatTourThanhCong';

const routes = [
    {
        component: Main,
        routes: [
            {
                path: '/',
                exact: true,
                component: Tours
            },
            {
                path: '/chi-tiet-tour',
                component: ChiTietTour
            },
            {
                path: '/thanh-toan',
                component: ThanhToan
            },
            {
                path: '/xem-them',
                component: XemThemTour
            },
            {
                path: '/dat-tour-thanh-cong',
                component: DatTourThanhCong
            }
        ]
    }
];

export default routes;
