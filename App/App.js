import React from 'react';
import ReactDOM from 'react-dom';
//  React-Router
import BrowserRouter from 'react-router-dom/BrowserRouter';
import { renderRoutes } from 'react-router-config';
import { Provider } from 'react-redux';

import configureStore from './Redux/configureStore.js';
import routes from './routes';

const store = configureStore();

// require('../public/css/styles.css');
// require('../public/css/table.css');
// require('../public/css/pc.css');
// require('../public/icons/css/font-awesome.min.css');
require('slick-carousel/slick/slick.css');
require('slick-carousel/slick/slick-theme.css');

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            { renderRoutes(routes) }
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
