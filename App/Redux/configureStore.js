const { createStore, applyMiddleware } = require('redux');
const thunk = require('redux-thunk').default;
const reducer = require('./Reducer/reducer');

export default function configStore() {
    const store = createStore(reducer, applyMiddleware(thunk));
    return store;
}
