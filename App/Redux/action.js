import {
    FETCHING_DATA,
    FETCHING_DATA_SUCCESS,
    FETCHING_DATA_FAIL
} from './constants.js';

import getDataTour from './FetchData/getDataTour.js';
import getDataTrangChu from './FetchData/getDataTrangChu.js';
import getDataXemThemTour from './FetchData/getDataXemThemTour.js';

function getData() {
    return {
        type: FETCHING_DATA
    };
}

function getDataSuccess(data) {
    return {
        type: FETCHING_DATA_SUCCESS,
        data
    };
}

function getDataFail() {
    return {
        type: FETCHING_DATA_FAIL
    };
}

const fetchData = (MaTour) => (dispatch) => {
    dispatch(getData());
    getDataTour(MaTour)
        .then(data => dispatch(getDataSuccess(data)))
        .catch((err) => console.log('err: ', err));
};

const fetchDataTrangChu = () => (dispatch) => {
    dispatch(getData());
    getDataTrangChu()
        .then(data => dispatch(getDataSuccess(data)))
        .catch(() => dispatch(getDataFail()));
};

const fetchDataXemThemTour = (xemThemParam, deXuat, gia, thoiLuongTour) => (dispatch) => {
    dispatch(getData());
    getDataXemThemTour(xemThemParam, deXuat, gia, thoiLuongTour)
        .then(data => dispatch(getDataSuccess(data)))
        .catch((err) => console.log('err: ', err));
};

module.exports = {
    getData,
    getDataSuccess,
    getDataFail,
    fetchData,
    fetchDataTrangChu,
    fetchDataXemThemTour
};
