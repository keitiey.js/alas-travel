const axios = require('axios');
const { urlChiTietTour } = require('../../../configure');

function getDataTour(MaTour) {
    return axios.get(urlChiTietTour + MaTour)
        .then((response) => response.data)
        .catch((error) => error);
}
export default getDataTour;
