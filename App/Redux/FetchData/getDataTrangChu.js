const axios = require('axios');
const { urlTrangChu } = require('../../../configure');

function getDataTrangChu() {
    return axios.get(urlTrangChu)
        .then((response) => response.data)
        .catch((error) => error);
}
export default getDataTrangChu;
