const axios = require('axios');
const { urlXemThemTour } = require('../../../configure');

function getDataXemThemTour(xemThemParam, deXuat, gia, thoiLuongTour) {
    if (deXuat === 'undefined' && gia === 'undefined' && thoiLuongTour === 'undefined') {
        const url1 = `${urlXemThemTour}${xemThemParam}`;
        return axios.get(url1)
            .then(response => response.data)
            .catch(error => error);
    }
    if (deXuat !== 'undefined' && gia === 'undefined' && thoiLuongTour === 'undefined') {
        const url2 = `${urlXemThemTour}${xemThemParam}&DeXuat=${deXuat}`;
        return axios.get(url2)
            .then(response => response.data)
            .catch(error => error);
    }
    if (deXuat === 'undefined' && gia !== 'undefined' && thoiLuongTour === 'undefined') {
        const url3 = `${urlXemThemTour}${xemThemParam}&Gia=Cao`;
        const url4 = `${urlXemThemTour}${xemThemParam}&Gia=Thap`;
        switch (gia) {
            case 'Cao':
                return axios.get(url3)
                    .then(response => response.data)
                    .catch(error => error);
            default:
                return axios.get(url4)
                    .then(response => response.data)
                    .catch(error => error);
        }
    }
    if (deXuat === 'undefined' && gia === 'undefined' && thoiLuongTour !== 'undefined') {
        const url5 = `${urlXemThemTour}${xemThemParam}&ThoiLuong=Cao`;
        const url6 = `${urlXemThemTour}${xemThemParam}&ThoiLuong=Thap`;
        switch (thoiLuongTour) {
            case 'Cao':
                return axios.get(url5)
                    .then(response => response.data)
                    .catch(error => error);
            default:
                return axios.get(url6)
                    .then(response => response.data)
                    .catch(error => error);
        }
    }
}

export default getDataXemThemTour;
