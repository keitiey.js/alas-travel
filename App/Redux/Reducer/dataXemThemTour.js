import {
    FETCHING_DATA,
    FETCHING_DATA_SUCCESS,
    FETCHING_DATA_FAIL
} from '../constants.js';

const initialState = {
    dataFetched: false,
    isFetching: false,
    error: false,
};

function dataXemThemTour(state = initialState, action) {
    switch (action.type) {
        case FETCHING_DATA:
            return {
                ...state,
                isFetching: true
            };
        case FETCHING_DATA_SUCCESS:
            return {
                ...state,
                isFetching: false,
                data: action.data,
                dataFetched: true
            };
        case FETCHING_DATA_FAIL:
            return {
                ...state,
                isFetching: false,
                error: true
            };
        default:
            return state;
    }
}
module.exports = dataXemThemTour;
