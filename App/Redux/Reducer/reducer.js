import { combineReducers } from 'redux';
import isToggleMenuBar from './isToggleMenuBar';
import dataCTTour from './dataCTTour';
import dataTrangChu from './dataTrangChu';
import dataXemThemTour from './dataXemThemTour';

const reducer = combineReducers({
    isToggleMenuBar,
    dataCTTour,
    dataTrangChu,
    dataXemThemTour
});
module.exports = reducer;
