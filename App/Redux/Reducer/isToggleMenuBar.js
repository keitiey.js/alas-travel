const isToggleMenuBar = (state = false, action) => {
    switch (action.type) {
        case 'TOGGLE_MENU_BAR':
            return !state;
        default:
            return state;
    }
};
module.exports = isToggleMenuBar;
