var nam = document.getElementById('nam');
window.onload = () => {
    axios.get(`${ localhost }/api/bao-cao/where?Nam=${nam.value}`)
    .then((response) => {

        var aReport = response.data;
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: `Thống kê website ALA's.COM`
            },
            subtitle: {
                text: 'Thống kê lượng khách truy cập, khách đặt tour và hủy tour'
            },
            xAxis: {
                categories: [
                    'Tháng 1',
                    'Tháng 2',
                    'Tháng 3',
                    'Tháng 4',
                    'Tháng 5',
                    'Tháng 6',
                    'Tháng 7',
                    'Tháng 8',
                    'Tháng 9',
                    'Tháng 10',
                    'Tháng 11',
                    'Tháng 12'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Số lượng khách (người)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} người</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Lượng khách truy cập',
                data: [parseInt(aReport.Thang1[0].slktc), parseInt(aReport.Thang2[0].slktc), parseInt(aReport.Thang3[0].slktc), parseInt(aReport.Thang4[0].slktc), parseInt(aReport.Thang5[0].slktc), parseInt(aReport.Thang6[0].slktc), parseInt(aReport.Thang7[0].slktc), parseInt(aReport.Thang8[0].slktc), parseInt(aReport.Thang9[0].slktc), parseInt(aReport.Thang10[0].slktc), parseInt(aReport.Thang11[0].slktc), parseInt(aReport.Thang12[0].slktc)]

            }, {
                name: 'Lượng khách đặt tour',
                data: [parseInt(aReport.Thang1[0].sldattour), parseInt(aReport.Thang2[0].sldattour), parseInt(aReport.Thang3[0].sldattour), parseInt(aReport.Thang4[0].sldattour), parseInt(aReport.Thang5[0].sldattour), parseInt(aReport.Thang6[0].sldattour), parseInt(aReport.Thang7[0].sldattour), parseInt(aReport.Thang8[0].sldattour), parseInt(aReport.Thang9[0].sldattour), parseInt(aReport.Thang10[0].sldattour), parseInt(aReport.Thang11[0].sldattour), parseInt(aReport.Thang12[0].sldattour)]

            }, {
                name: 'Lượng khách hủy tour',
                data: [parseInt(aReport.Thang1[0].slhuytour), parseInt(aReport.Thang2[0].slhuytour), parseInt(aReport.Thang3[0].slhuytour), parseInt(aReport.Thang4[0].slhuytour), parseInt(aReport.Thang5[0].slhuytour), parseInt(aReport.Thang6[0].slhuytour), parseInt(aReport.Thang7[0].slhuytour), parseInt(aReport.Thang8[0].slhuytour), parseInt(aReport.Thang9[0].slhuytour), parseInt(aReport.Thang10[0].slhuytour), parseInt(aReport.Thang11[0].slhuytour), parseInt(aReport.Thang12[0].slhuytour)]

            }
            // , {
            //     name: 'Berlin',
            //     data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

            // }
            ]
        });
    })
    .catch((error) => {
        console.log(error);
    })
}
var handleShowBaoCao = () => {
    console.log(nam.value);
    axios.get(`${ localhost }/api/bao-cao/where?Nam=${nam.value}`)
    .then((response) => {
        var aReport = response.data;
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: `Thống kê website ALA's.COM`
            },
            subtitle: {
                text: 'Thống kê lượng khách truy cập, khách đặt tour và hủy tour'
            },
            xAxis: {
                categories: [
                    'Tháng 1',
                    'Tháng 2',
                    'Tháng 3',
                    'Tháng 4',
                    'Tháng 5',
                    'Tháng 6',
                    'Tháng 7',
                    'Tháng 8',
                    'Tháng 9',
                    'Tháng 10',
                    'Tháng 11',
                    'Tháng 12'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Số lượng khách (người)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} người</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Lượng khách truy cập',
                data: [parseInt(aReport.Thang1[0].slktc), parseInt(aReport.Thang2[0].slktc), parseInt(aReport.Thang3[0].slktc), parseInt(aReport.Thang4[0].slktc), parseInt(aReport.Thang5[0].slktc), parseInt(aReport.Thang6[0].slktc), parseInt(aReport.Thang7[0].slktc), parseInt(aReport.Thang8[0].slktc), parseInt(aReport.Thang9[0].slktc), parseInt(aReport.Thang10[0].slktc), parseInt(aReport.Thang11[0].slktc), parseInt(aReport.Thang12[0].slktc)]

            }, {
                name: 'Lượng khách đặt tour',
                data: [parseInt(aReport.Thang1[0].sldattour), parseInt(aReport.Thang2[0].sldattour), parseInt(aReport.Thang3[0].sldattour), parseInt(aReport.Thang4[0].sldattour), parseInt(aReport.Thang5[0].sldattour), parseInt(aReport.Thang6[0].sldattour), parseInt(aReport.Thang7[0].sldattour), parseInt(aReport.Thang8[0].sldattour), parseInt(aReport.Thang9[0].sldattour), parseInt(aReport.Thang10[0].sldattour), parseInt(aReport.Thang11[0].sldattour), parseInt(aReport.Thang12[0].sldattour)]

            }, {
                name: 'Lượng khách hủy tour',
                data: [parseInt(aReport.Thang1[0].slhuytour), parseInt(aReport.Thang2[0].slhuytour), parseInt(aReport.Thang3[0].slhuytour), parseInt(aReport.Thang4[0].slhuytour), parseInt(aReport.Thang5[0].slhuytour), parseInt(aReport.Thang6[0].slhuytour), parseInt(aReport.Thang7[0].slhuytour), parseInt(aReport.Thang8[0].slhuytour), parseInt(aReport.Thang9[0].slhuytour), parseInt(aReport.Thang10[0].slhuytour), parseInt(aReport.Thang11[0].slhuytour), parseInt(aReport.Thang12[0].slhuytour)]

            }
            // , {
            //     name: 'Berlin',
            //     data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

            // }
            ]
        });
    })
    .catch((error) => {
        console.log(error);
    })
}