const MaTour = getQueryStringValue('MaTour');
const urlSelectTour = `${ localhost }/api/tours/select?MaTour=`;
const urlMaTour = getQueryStringValue('MaTour');

const iMaTour    = document.getElementById('maTour');
const iMaTourHide1 = document.getElementById('maTourHide1');
const iMaTourHide2 = document.getElementById('maTourHide2');
const iMaTourHide3 = document.getElementById('maTourHide3');
const iMaTourHide4 = document.getElementById('maTourHide4');
const iMaTourHide5 = document.getElementById('maTourHide5');
const iMaTourHide6 = document.getElementById('maTourHide6');
const iGiaTour   = document.getElementById('giaTour');
const iTenTour   = document.getElementById('tenTour');
const iThoiGian  = document.getElementById('thoiGian');
const iGioiThieu = document.getElementById('gioiThieu');
const cPhuongTien = document.getElementsByClassName('phuongTien');
const nDiaDiem = document.getElementsByName('diaDiem');
const iAnhTour = document.getElementById('anhTour');
const iAnhTourInput = document.getElementById('anhTourInput');
const iSoNgay = document.getElementById('soNgay');
const cDichVu = document.getElementsByClassName('dichVu');
const iMaDoiTac = document.getElementById('maDoiTac');
const iTenDoiTac = document.getElementById('tenDoiTac');
const iDiaChi = document.getElementById('diachiDoiTac');
const iMaDK = document.getElementById('maDK');
const iVanChuyenDK = document.getElementById('vanChuyenDK');
const iLuuTruDK = document.getElementById('luuTruDK');
const iKhacDK = document.getElementById('khacDK');


const DataCTT = {
    id : {
        MaTour      : (MaTour, aData) => { setID(MaTour, aData, null, null, null, null) },
        MaTourHide1  : (MaTourHide1, aData) => { setID(MaTourHide1, aData, null, null, null, null) },
        MaTourHide2  : (MaTourHide2, aData) => { setID(MaTourHide2, aData, null, null, null, null) },
        MaTourHide3  : (MaTourHide3, aData) => { setID(MaTourHide3, aData, null, null, null, null) },
        MaTourHide4  : (MaTourHide4, aData) => { setID(MaTourHide4, aData, null, null, null, null) },
        MaTourHide5  : (MaTourHide5, aData) => { setID(MaTourHide5, aData, null, null, null, null) },
        MaTourHide6  : (MaTourHide6, aData) => { setID(MaTourHide6, aData, null, null, null, null) },
        GiaTour     : (GiaTour, aData) => { setID(GiaTour, aData, null, null, null, null) },
        TenTour     : (TenTour, aData) => { setID(TenTour, aData, null, null, null, null) },
        ThoiGian    : (ThoiGian, aData) => { setID(ThoiGian, aData, null, null, null, null) },
        GioiThieu   : (GioiThieu, aData) => { setID(GioiThieu, aData, null, null, null, null) },
        DiaDiem     : (DiaDiem, aNameData, aMaData, arrNameData, arrMaData) => { setID(DiaDiem, null, aNameData, aMaData, arrNameData, arrMaData) },
        AnhTour     : (AnhTour, aData) => { setID(AnhTour, aData, null, null, null, null) },
        DoiTac : {
            MaDT : (MaDT, aData) => { setID(MaDT, aData, null, null, null, null) },
            Ten : (Ten, aData) => { setID(Ten, aData, null, null, null, null) },
            DiaChi : (DiaChi, aData) => { setID(DiaChi, aData, null, null, null, null) },
        },
        DieuKhoan : {
            MaDK : (MaDK, aData) => { setID(MaDK, aData, null, null, null, null) },
            VanChuyen : (VanChuyen, aData) => { setID(VanChuyen, aData, null, null, null, null) },
            LuuTru : (LuuTru, aData) => { setID(LuuTru, aData, null, null, null, null) },
            Khac : (Khac, aData) => { setID(Khac, aData, null, null, null, null) },
        }
    },
    class : {
        PhuongTien : (PhuongTien, arrData) => { setClass(PhuongTien, arrData) },
        LichTrinh  : {
            SoNgay : (SoNgay, aData) => { setID(SoNgay, aData, null, null, null, null) },
            Ngay : (Ngay, arrData) => { setClass(Ngay, arrData) },
            Anh : (Anh, arrData) => { setClass(Anh, arrData) },
            NoiDung1 : (NoiDung1, arrData) => { setClass(NoiDung1, arrData) },
            NoiDung2 : (NoiDung2, arrData) => { setClass(NoiDung2, arrData) },
        },
        DichVu : (DichVu, arrData) => { setClass(DichVu, arrData) },
        KhuyenMai : {
            MaKM : (MaKM, aData) => { setID(MaKM, aData, null, null, null, null)},
            TTKM : (TTKM, aData) => { setID(TTKM, aData, null, null, null, null)},
            Deal : (Deal, aData) => { setID(Deal, aData, null, null, null, null)},
            KM   : (KM, aData) => { setID(KM, aData, null, null, null, null)}
        }
    },
}

var handleShowLichTrinh = (idSoNgay, idPlaceOfDisplay) => {
    let soNgay, placeOfDisplay;
    soNgay = document.getElementById(idSoNgay);
    placeOfDisplay = document.getElementById(idPlaceOfDisplay);
    let html = `
        <div class="row lich-trinh-ngay">
            <div class="col-md-8">
                <div class="form-inline" style="margin-bottom: 10px">
                    <label for="ngayLichTrinh">Ngày: </label>
                    <input type="number" min="1" class="form-control ngayLichTrinh" name="ngayLichTrinh" id="ngayLichTrinh">
                </div>
                <div class="form-group">
                    <label for="noiDung1">Nội dung 1: </label>
                    <textarea type="text" onchange="replaceSingleQuote(this)" class="form-control noiDung1" name="noiDung1"></textarea>
                </div>
                <div class="form-group">
                    <label for="noiDung2">Nội dung 2: </label>
                    <textarea type="text" onchange="replaceSingleQuote(this)" class="form-control noiDung2" name="noiDung2"></textarea>
                </div>                                
            </div> 
            <div class="col-md-4">     
                <div class="anh-lich-trinh-tour">
                    <img id="" class="anhLichTrinh" src="${ localhost }/images/default.png" />     
                </div>     
                <label for="" class="custom-file-upload anhLichTrinhLabel">
                    <i class="fa fa-upload" aria-hidden="true"></i> Chọn ảnh
                </label>
                <input id="" class="anhLichTrinhInput" name="anhLichTrinhInput" type="file"/><span id="" class="anhLichTrinhSpan"></span>                      
                <span style="display: block; visibility:hidden" class="errMessage errAnhLichTrinh">Ảnh không được bỏ trống</span>
            </div>
        </div>      
    `;
    placeOfDisplay.innerHTML = '';
    for(let i=0; i<soNgay.value; i++) {
        placeOfDisplay.innerHTML += html;
    }
    if(placeOfDisplay.innerHTML != null) {
        let anhLichTrinh, anhLichTrinhInput, anhLichTrinhSpan, anhLichTrinhLabel, ngayLichTrinh, noiDung1, noiDung2;
        ngayLichTrinh = document.querySelectorAll('.ngayLichTrinh');
        noiDung1 = document.querySelectorAll('.noiDung1');
        noiDung2 = document.querySelectorAll('.noiDung2');
        errAnhLichTrinh = document.querySelectorAll('.errAnhLichTrinh');
        anhLichTrinh = document.querySelectorAll('.anhLichTrinh');
        anhLichTrinhInput = document.querySelectorAll('.anhLichTrinhInput');
        anhLichTrinhSpan = document.querySelectorAll('.anhLichTrinhSpan');
        anhLichTrinhLabel = document.querySelectorAll('.anhLichTrinhLabel');
        // Set ID
        for(let i=0; i<soNgay.value; i++) {
            ngayLichTrinh[i].setAttribute('id', 'ngayLichTrinh' + i);
            noiDung1[i].setAttribute('id', 'noiDung1' + i);
            noiDung2[i].setAttribute('id', 'noiDung2' + i);
            errAnhLichTrinh[i].setAttribute('id', 'errAnhLichTrinh' + i);
            anhLichTrinh[i].setAttribute('id', 'anhLichTrinh' + i);
            anhLichTrinhInput[i].setAttribute('id', 'anhLichTrinhInput' + i);
            anhLichTrinhSpan[i].setAttribute('id', 'anhLichTrinhSpan' + i);
            anhLichTrinhLabel[i].setAttribute('for', 'anhLichTrinhInput' + i);
            let idInput = 'anhLichTrinhInput' + i;
            let idSpan = 'anhLichTrinhSpan' + i;
            let idImage = 'anhLichTrinh' + i;
            document.getElementById(idInput).addEventListener('change', () => {
                handleUploadAnh(idInput, idSpan, idImage)
            });
        }      
    }
}

document.getElementById('Deal').addEventListener('change', () => {
    let idDeal = document.getElementById('Deal');
    let idDealHide = document.getElementById('DealHide');
    if(idDeal.checked == true) {
        idDealHide.value = 'true';
    } else {
        idDealHide.value = 'false';
    }
})
document.getElementById('KM').addEventListener('change', () => {
    let idKM = document.getElementById('KM');
    let idKMHide = document.getElementById('KMHide');
    if(idKM.checked == true) {
        idKMHide.value = 'true';
    } else {
        idKMHide.value = 'false';
    }
})

API.GET.dataChiTietTour(urlSelectTour, urlMaTour)
    .then((data) => {
        DataCTT.id.MaTour('maTour', data.ChiTietTour[0].MaTour);
        DataCTT.id.MaTourHide1('maTourHide1', data.ChiTietTour[0].MaTour);
        DataCTT.id.MaTourHide2('maTourHide2', data.ChiTietTour[0].MaTour);
        DataCTT.id.MaTourHide3('maTourHide3', data.ChiTietTour[0].MaTour);
        DataCTT.id.MaTourHide4('maTourHide4', data.ChiTietTour[0].MaTour);
        DataCTT.id.MaTourHide5('maTourHide5', data.ChiTietTour[0].MaTour);
        DataCTT.id.MaTourHide5('maTourHide6', data.ChiTietTour[0].MaTour);
        DataCTT.id.GiaTour('giaTour', data.ChiTietTour[0].GiaTour);
        DataCTT.id.TenTour('tenTour', data.ChiTietTour[0].TenTour);
        DataCTT.id.ThoiGian('thoiGian', data.ChiTietTour[0].ThoiGian);
        DataCTT.id.GioiThieu('gioiThieu', data.ChiTietTour[0].GioiThieu);
        DataCTT.class.KhuyenMai.MaKM('maKM', data.KhuyenMai[0].MaKM);
        DataCTT.class.KhuyenMai.MaKM('maKMHide', data.KhuyenMai[0].MaKM);
        DataCTT.class.KhuyenMai.TTKM('thongtinKM', data.KhuyenMai[0].TTKM);
        DataCTT.class.KhuyenMai.Deal('Deal', data.KhuyenMai[0].Deal);
        DataCTT.class.KhuyenMai.Deal('DealHide', data.KhuyenMai[0].Deal);
        DataCTT.class.KhuyenMai.KM('KM', data.KhuyenMai[0].KM);
        DataCTT.class.KhuyenMai.KM('KMHide', data.KhuyenMai[0].KM);
        // AAA1
        DataCTT.class.LichTrinh.SoNgay('soNgay', data.LichTrinhTour.length);
        let arrPhuongTien = new Array();
        data.PhuongTien.forEach((item) => {
            arrPhuongTien.push(item.MaPT);
        })
        DataCTT.class.PhuongTien('phuongTien', arrPhuongTien);
        DataCTT.id.AnhTour('anhTour', data.ChiTietTour[0].AnhTour);
        let html = `
            <div class="row lich-trinh-ngay">
                <div class="col-md-8">
                    <div class="form-inline" style="margin-bottom: 10px">
                        <label for="ngayLichTrinh">Ngày: </label>
                        <input type="number" min="1" class="form-control ngayLichTrinh" name="ngayLichTrinh" id="ngayLichTrinh">
                    </div>
                    <div class="form-group">
                        <label for="noiDung1">Nội dung 1: </label>
                        <textarea type="text" onchange="replaceSingleQuote(this)" class="form-control noiDung1" name="noiDung1"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="noiDung2">Nội dung 2: </label>
                        <textarea type="text" onchange="replaceSingleQuote(this)" class="form-control noiDung2" name="noiDung2"></textarea>
                    </div>                                
                </div> 
                <div class="col-md-4">     
                    <div class="anh-lich-trinh-tour">
                        <img id="" class="anhLichTrinh" src="${ localhost }/images/default.png" />     
                    </div>     
                    <label for="" class="custom-file-upload anhLichTrinhLabel">
                        <i class="fa fa-upload" aria-hidden="true"></i> Chọn ảnh
                    </label>
                    <input id="" style="display: none" class="anhLichTrinhInputHide" name="anhLichTrinhInputHide"/>
                    <input id="" class="anhLichTrinhInput" name="anhLichTrinhInput" type="file"/><span id="" class="anhLichTrinhSpan"></span>                      
                    <span style="display: block; visibility:hidden" class="errMessage errAnhLichTrinh">Ảnh không được bỏ trống</span>
                </div>
            </div>      
        `;
        loopDiv('placeOfDisplay', null, html, data.LichTrinhTour.length);
        if(document.getElementById('placeOfDisplay').innerHTML != null) {
            let arrNgay, arrAnh, arrNoiDung1, arrNoiDung2;
            arrNgay = new Array();
            arrAnh = new Array();
            arrNoiDung1 = new Array();
            arrNoiDung2 = new Array();
            data.LichTrinhTour.forEach((item) => {
                arrNgay.push(item.Ngay);
                arrAnh.push(item.Anh);
                arrNoiDung1.push(item.NoiDung1);
                arrNoiDung2.push(item.NoiDung2);
            })      
            DataCTT.class.LichTrinh.Ngay('ngayLichTrinh', arrNgay);
            DataCTT.class.LichTrinh.Anh('anhLichTrinh', arrAnh);
            DataCTT.class.LichTrinh.Anh('anhLichTrinhInputHide', arrAnh);
            DataCTT.class.LichTrinh.NoiDung1('noiDung1', arrNoiDung1);
            DataCTT.class.LichTrinh.NoiDung2('noiDung2', arrNoiDung2);

            let anhLichTrinh, anhLichTrinhInput, anhLichTrinhInputHide, anhLichTrinhSpan, anhLichTrinhLabel, ngayLichTrinh, noiDung1, noiDung2;
            ngayLichTrinh = document.querySelectorAll('.ngayLichTrinh');
            noiDung1 = document.querySelectorAll('.noiDung1');
            noiDung2 = document.querySelectorAll('.noiDung2');
            errAnhLichTrinh = document.querySelectorAll('.errAnhLichTrinh');
            anhLichTrinh = document.querySelectorAll('.anhLichTrinh');
            anhLichTrinhInputHide = document.querySelectorAll('.anhLichTrinhInputHide');
            anhLichTrinhInput = document.querySelectorAll('.anhLichTrinhInput');
            anhLichTrinhSpan = document.querySelectorAll('.anhLichTrinhSpan');
            anhLichTrinhLabel = document.querySelectorAll('.anhLichTrinhLabel');
            // Set ID
            for(let i=0; i<data.LichTrinhTour.length; i++) {
                ngayLichTrinh[i].setAttribute('id', 'ngayLichTrinh' + i);
                noiDung1[i].setAttribute('id', 'noiDung1' + i);
                noiDung2[i].setAttribute('id', 'noiDung2' + i);
                errAnhLichTrinh[i].setAttribute('id', 'errAnhLichTrinh' + i);
                anhLichTrinh[i].setAttribute('id', 'anhLichTrinh' + i);
                anhLichTrinhInputHide[i].setAttribute('id', 'anhLichTrinhInputHide' + i);
                anhLichTrinhInput[i].setAttribute('id', 'anhLichTrinhInput' + i);
                anhLichTrinhSpan[i].setAttribute('id', 'anhLichTrinhSpan' + i);
                anhLichTrinhLabel[i].setAttribute('for', 'anhLichTrinhInput' + i);
                let idInput = 'anhLichTrinhInput' + i;
                let idSpan = 'anhLichTrinhSpan' + i;
                let idImage = 'anhLichTrinh' + i;
                document.getElementById(idInput).addEventListener('change', () => {
                    handleUploadAnh(idInput, idSpan, idImage)
                });
                let idNgay = 'ngayLichTrinh' + i;
                let idNoiDung1 = 'noiDung1' + i;
                let idNoiDung2 = 'noiDung2' + i;
                let idAnhLichTrinh = 'anhLichTrinh' + i;
                let idAnhLichTrinhInput = 'anhLichTrinhInput' + i;
                let idAnhLichTrinhInputHide = 'anhLichTrinhInputHide' + i;
                let idErrAnhLichTrinh = 'errAnhLichTrinh' + i;
                let elementNgay = document.getElementById(idNgay);
                let elementNoiDung1 = document.getElementById(idNoiDung1);
                let elementNoiDung2 = document.getElementById(idNoiDung2);
                let elementAnhLichTrinh = document.getElementById(idAnhLichTrinh)
                let elementAnhLichTrinhInput = document.getElementById(idAnhLichTrinhInput);
                let elementAnhLichTrinhInputHide = document.getElementById(idAnhLichTrinhInputHide);
                if (typeof(elementNgay) != 'undefined' && elementNgay != null) {
                    elementNgay.addEventListener('change', () => {
                        if(elementNgay.value != '' && elementNgay.classList.contains('errBox') || elementNgay.classList.contains('empty')) {
                            elementNgay.classList.remove('errBox'); 
                            elementNgay.classList.remove('empty');                        
                        }
                    })
                }
                if (typeof(elementNoiDung1) != 'undefined' && elementNoiDung1 != null) {
                    elementNoiDung1.addEventListener('change', () => {
                        if(elementNoiDung1.value != '' && elementNoiDung1.classList.contains('errBox') || elementNoiDung1.classList.contains('empty')) {
                            elementNoiDung1.classList.remove('errBox'); 
                            elementNoiDung1.classList.remove('empty');                         
                        }
                    })
                }
                if (typeof(elementNoiDung2) != 'undefined' && elementNoiDung2 != null) {
                    elementNoiDung2.addEventListener('change', () => {
                        if(elementNoiDung2.value != '' && elementNoiDung2.classList.contains('errBox') || elementNoiDung2.classList.contains('empty')) {
                            elementNoiDung2.classList.remove('errBox'); 
                            elementNoiDung2.classList.remove('empty')                        
                        }

                    })
                }
                if (typeof(elementAnhLichTrinhInput) != 'undefined' && elementAnhLichTrinhInput != null) {
                    elementAnhLichTrinhInput.addEventListener('change', () => {
                        // HERE
                        elementAnhLichTrinhInputHide.value = elementAnhLichTrinhInput.files[0].name;   

                        if(elementAnhLichTrinhInput.value != '' && document.getElementById(idErrAnhLichTrinh).style.visibility != 'hidden') {
                                                        
                            document.getElementById(idErrAnhLichTrinh).style.visibility = 'hidden'; 
                            document.getElementById(idErrAnhLichTrinh).classList.remove('empty');
                            
                        }
                    })
                }   
            }            
        }
        

        let arrDichVu = new Array();
        data.DichVu.forEach((item) => {
            arrDichVu.push(item.MaDV);
        })
        DataCTT.class.DichVu('dichVu', arrDichVu);

        DataCTT.id.DoiTac.MaDT('maDoiTac', data.DoiTac[0].MaDT);
        DataCTT.id.DoiTac.MaDT('maDoiTacHide', data.DoiTac[0].MaDT);
        DataCTT.id.DoiTac.Ten('tenDoiTac', data.DoiTac[0].TenDT);
        DataCTT.id.DoiTac.DiaChi('diachiDoiTac', data.DoiTac[0].DiaChi);

        DataCTT.id.DieuKhoan.MaDK('maDK', data.DieuKhoan[0].MaDK);
        DataCTT.id.DieuKhoan.MaDK('maDKHide', data.DieuKhoan[0].MaDK);
        DataCTT.id.DieuKhoan.VanChuyen('vanChuyenDK', data.DieuKhoan[0].VanChuyen);
        DataCTT.id.DieuKhoan.LuuTru('luuTruDK', data.DieuKhoan[0].LuuTru);
        DataCTT.id.DieuKhoan.Khac('khacDK', data.DieuKhoan[0].Khac);

        let arrNameDiaDiem = new Array();
        let arrMaDiaDiem = new Array();
        data.DanhSachDiaDiem.forEach((item) => {
            arrNameDiaDiem.push(item.TenDD);
            arrMaDiaDiem.push(item.MaDD);
        })
        console.log(arrMaDiaDiem);
        DataCTT.id.DiaDiem('diaDiem', data.DiaDiem[0].TenDD, data.DiaDiem[0].MaDD, arrNameDiaDiem, arrMaDiaDiem);
    })
    .catch((err) => console.error(err))


// Button QuayLai
document.getElementById('btnQuayLai').addEventListener('click', () => {
    window.location.href = `${localhost}/management`;
})

// Button Reset
for(let i=0; i<document.getElementsByClassName('btnReset').length; i++) {
    document.getElementsByClassName('btnReset')[i].addEventListener('click', () => {
        window.location.reload(true);
    })
}

//  Input Change
iGiaTour.addEventListener('change', () => {
    if(iGiaTour.value != '' && iGiaTour.classList.contains('errBox') || iGiaTour.classList.contains('empty')) {
        iGiaTour.classList.remove('errBox'); 
        iGiaTour.classList.remove('empty');
    }
})
iTenTour.addEventListener('change', () => {
    if(iTenTour.value != '' && iTenTour.classList.contains('errBox') || iTenTour.classList.contains('empty')) {
        iTenTour.classList.remove('errBox'); 
        iTenTour.classList.remove('empty'); 
    }
})
iThoiGian.addEventListener('change', () => {
    if(iThoiGian.value != '' && iThoiGian.classList.contains('errBox') || iThoiGian.classList.contains('empty')) {
        iThoiGian.classList.remove('errBox'); 
        iThoiGian.classList.remove('empty');
    }
})
iGioiThieu.addEventListener('change', () => {
    if(iGioiThieu.value != '' && iGioiThieu.classList.contains('errBox') || iGioiThieu.classList.contains('empty')) {
        iGioiThieu.classList.remove('errBox'); 
        iGioiThieu.classList.remove('empty');
    }
})
for(let i=0; i<cPhuongTien.length; i++) {
    cPhuongTien[i].addEventListener('change', () => {
        if(cPhuongTien[i].checked == true && document.getElementById('errPhuongTien').style.visibility != 'hidden') {
            document.getElementById('errPhuongTien').style.visibility = 'hidden'; 
            document.getElementById('errPhuongTien').classList.remove('empty');
        }
    })
}
iAnhTour.addEventListener('change', () => {
    if(iAnhTour.value != '' && document.getElementById('errAnhTour').style.visibility != 'hidden') {
        document.getElementById('errAnhTour').style.visibility = 'hidden'; 
        document.getElementById('errAnhTour').classList.remove('empty');
    }
})


// Validate FORM CHITIETTOUR
var validateFormChiTietTour = () => {

    if(maTourHide1.value == '') maTourHide1.classList.add('empty');
    if(iGiaTour.value == '') iGiaTour.classList.add('empty');
    if(iTenTour.value == '') iTenTour.classList.add('empty');
    if(iThoiGian.value == '') iThoiGian.classList.add('empty');
    if(iGioiThieu.value == '') iGioiThieu.classList.add('empty'); 

    // Validate Phuong Tien
    var tempPhuongTien = 0;
    for(let i=0; i<cPhuongTien.length; i++) {
        if(cPhuongTien[i].checked == true) tempPhuongTien ++;
    }
    if(tempPhuongTien == 0) {
        document.getElementById('errPhuongTien').style.visibility = 'unset';
        document.getElementById('errPhuongTien').classList.add('empty');
    }

    // Validate Anh Tour
    if(iAnhTour.src == `${localhost}/images/default.png`) {
        document.getElementById('errAnhTour').style.visibility = 'unset';
        document.getElementById('errAnhTour').classList.add('empty');
    }

    for(let i=0; i<document.getElementsByClassName('empty').length; i++) {
        document.getElementsByClassName('empty')[i].classList.add('errBox');
        var flag = false;
    }
    if(flag == false) return false;
    else return true;  
}

document.getElementById('btnChiTietTour').addEventListener('click', () => {
    if(validateFormChiTietTour() == true) {
        
        swal(
        'Good job!',
        'Sửa thông tin thành công !!',
        'success'
        ).then(() => {
            document.getElementById('myFormChiTietTour').submit();
        })        
    } else {
        swal(
            'Failure',
            'Bạn cần điền đầy đủ thông tin !',
            'error'
        )  
    }
})
// END Validate FORM CHITIETTOUR

//  Validate FORM LICHTRINHTOUR
document.getElementById('btnCapNhat').addEventListener('click', () => {
    if(iSoNgay.value != 0) {
        for(let i=0; i<iSoNgay.value; i++) {                      
            let idNgay = 'ngayLichTrinh' + i;
            let idNoiDung1 = 'noiDung1' + i;
            let idNoiDung2 = 'noiDung2' + i;
            let idAnhLichTrinh = 'anhLichTrinh' + i;
            let idAnhLichTrinhInput = 'anhLichTrinhInput' + i;
            let idErrAnhLichTrinh = 'errAnhLichTrinh' + i;
            let elementNgay = document.getElementById(idNgay);
            let elementNoiDung1 = document.getElementById(idNoiDung1);
            let elementNoiDung2 = document.getElementById(idNoiDung2);
            let elementAnhLichTrinh = document.getElementById(idAnhLichTrinh)
            let elementAnhLichTrinhInput = document.getElementById(idAnhLichTrinhInput);
            if (typeof(elementNgay) != 'undefined' && elementNgay != null) {
                elementNgay.addEventListener('change', () => {
                    if(elementNgay.value != '' && elementNgay.classList.contains('errBox') || elementNgay.classList.contains('empty')) {
                        elementNgay.classList.remove('errBox'); 
                        elementNgay.classList.remove('empty');                        
                    }
                })
            }
            if (typeof(elementNoiDung1) != 'undefined' && elementNoiDung1 != null) {
                elementNoiDung1.addEventListener('change', () => {
                    if(elementNoiDung1.value != '' && elementNoiDung1.classList.contains('errBox') || elementNoiDung1.classList.contains('empty')) {
                        elementNoiDung1.classList.remove('errBox'); 
                        elementNoiDung1.classList.remove('empty');                         
                    }
                })
            }
            if (typeof(elementNoiDung2) != 'undefined' && elementNoiDung2 != null) {
                elementNoiDung2.addEventListener('change', () => {
                    if(elementNoiDung2.value != '' && elementNoiDung2.classList.contains('errBox') || elementNoiDung2.classList.contains('empty')) {
                        elementNoiDung2.classList.remove('errBox'); 
                        elementNoiDung2.classList.remove('empty')                        
                    }

                })
            }
            if (typeof(elementAnhLichTrinhInput) != 'undefined' && elementAnhLichTrinhInput != null) {
                elementAnhLichTrinhInput.addEventListener('change', () => {
                    if(elementAnhLichTrinhInput.value != '' && document.getElementById(idErrAnhLichTrinh).style.visibility != 'hidden') {
                        document.getElementById(idErrAnhLichTrinh).style.visibility = 'hidden'; 
                        document.getElementById(idErrAnhLichTrinh).classList.remove('empty');
                    }
                })
            }
        }
    }
})

var validateFormLichTrinh = () => {
    if(iSoNgay.value != '') {
        for(let i=0; i<iSoNgay.value; i++) {
            let idNgay = 'ngayLichTrinh' + i;
            let idNoiDung1 = 'noiDung1' + i;
            let idNoiDung2 = 'noiDung2' + i;
            let idAnhLichTrinh = 'anhLichTrinh' + i;
            let idAnhLichTrinhInput = 'anhLichTrinhInput' + i;
            let idErrAnhLichTrinh = 'errAnhLichTrinh' + i;
            let elementNgay = document.getElementById(idNgay);
            let elementNoiDung1 = document.getElementById(idNoiDung1);
            let elementNoiDung2 = document.getElementById(idNoiDung2);
            let elementAnhLichTrinh = document.getElementById(idAnhLichTrinh);
            let elementAnhLichTrinhInput = document.getElementById(idAnhLichTrinhInput);
            if (typeof(elementNgay) != 'undefined' && elementNgay != null) {
                if(elementNgay.value == '') elementNgay.classList.add('empty');
            }
            if (typeof(elementNoiDung1) != 'undefined' && elementNoiDung1 != null) {
                if(elementNoiDung1.value == '') elementNoiDung1.classList.add('empty');
            }
            if (typeof(elementNoiDung2) != 'undefined' && elementNoiDung2 != null) {
                if(elementNoiDung2.value == '') elementNoiDung2.classList.add('empty');
            }
            if (typeof(elementAnhLichTrinh) != 'undefined' && elementAnhLichTrinh != null) {
                if(elementAnhLichTrinh.src == `${localhost}/images/default.png`) {
                    document.getElementById(idErrAnhLichTrinh).style.visibility = 'unset';
                    document.getElementById(idErrAnhLichTrinh).classList.add('empty');
                }
            }
        }
    }
    for(let i=0; i<document.getElementsByClassName('empty').length; i++) {
        document.getElementsByClassName('empty')[i].classList.add('errBox');
        var flag = false;
    }
    if(flag == false) return false;
    else return true;
}
document.getElementById('btnLichTrinh').addEventListener('click', () => {
    if(validateFormLichTrinh() == true) {
        swal(
        'Good job!',
        'Sửa thông tin thành công !!',
        'success'
        ).then(() => {
            document.getElementById('myFormLichTrinh').submit();
        })        
    } else {
        swal(
            'Failure',
            'Bạn cần điền đầy đủ thông tin !',
            'error'
        )  
    }
})
//  END Validate FORM LICHTRINHTOUR

// Validate FORM DICHVU
for(let i=0; i<cDichVu.length; i++) {
    cDichVu[i].addEventListener('change', () => {
        if(cDichVu[i].checked == true && document.getElementById('errDichVu').style.visibility != 'hidden') {
            document.getElementById('errDichVu').style.visibility = 'hidden'; 
            document.getElementById('errDichVu').classList.remove('empty');
        }
    })
}
var validateFormDichVu = () => {
    var tempDichVu = 0;
    for(let i=0; i<cDichVu.length; i++) {
        if(cDichVu[i].checked == true) tempDichVu ++;
    }
    if(tempDichVu == 0) {
        document.getElementById('errDichVu').style.visibility = 'unset';
        document.getElementById('errDichVu').classList.add('empty');
    }       
    for(let i=0; i<document.getElementsByClassName('empty').length; i++) {
        document.getElementsByClassName('empty')[i].classList.add('errBox');
        var flag = false;
    }
    if(flag == false) return false;
    else return true;    
}
document.getElementById('btnDichVu').addEventListener('click', () => {
    if(validateFormDichVu() == true) {
        swal(
        'Good job!',
        'Sửa thông tin thành công !!',
        'success'
        ).then(() => {
            document.getElementById('myFormDichVu').submit();
        })        
    } else {
        swal(
            'Failure',
            'Bạn cần điền đầy đủ thông tin !',
            'error'
        )  
    }
})
// END Validate FORM DICHVU

//  Validate FORM DOITAC
iTenDoiTac.addEventListener('change', () => {
    if(iTenDoiTac.value != '' && iTenDoiTac.classList.contains('errBox') || iTenDoiTac.classList.contains('empty')) {
        iTenDoiTac.classList.remove('errBox'); 
        iTenDoiTac.classList.remove('empty');
    }
})

iDiaChi.addEventListener('change', () => {
    if(iDiaChi.value != '' && iDiaChi.classList.contains('errBox') || iDiaChi.classList.contains('empty')) {
        iDiaChi.classList.remove('errBox'); 
        iDiaChi.classList.remove('empty');
    }
})
var validateFormDoiTac = () => {
    if(iMaDoiTac.value == '') iMaDoiTac.classList.add('empty');
    if(iTenDoiTac.value == '') iTenDoiTac.classList.add('empty');
    if(iDiaChi.value == '') iDiaChi.classList.add('empty');    
    for(let i=0; i<document.getElementsByClassName('empty').length; i++) {
        document.getElementsByClassName('empty')[i].classList.add('errBox');
        var flag = false;
    }
    if(flag == false) return false;
    else return true;
}
document.getElementById('btnDoiTac').addEventListener('click', () => {
    if(validateFormDoiTac() == true) {
        swal(
        'Good job!',
        'Thêm Tour du lịch thành công !!',
        'success'
        ).then(() => {
            document.getElementById('myFormDoiTac').submit();
        })
    } else {
        swal(
            'Failure',
            'Bạn cần điền đầy đủ thông tin !',
            'error'
        )  
    }
})

document.getElementById('btnKhuyenMai').addEventListener('click', () => {
    // if(validateFormDoiTac() == true) {
    //     swal(
    //     'Good job!',
    //     'Thêm Tour du lịch thành công !!',
    //     'success'
    //     ).then(() => {
    //         document.getElementById('myFormDoiTac').submit();
    //     })
    // } else {
    //     swal(
    //         'Failure',
    //         'Bạn cần điền đầy đủ thông tin !',
    //         'error'
    //     )  
    // }
    document.getElementById('myFormKhuyenMai').submit();
})
// END Validate FORM DOITAC

// Validate FORM DIEUKHOAN

// iVanChuyenDK.addEventListener('change', () => {
//     if(iVanChuyenDK.value != '' && iDiaChi.classList.contains('errBox') || iVanChuyenDK.classList.contains('empty')) {
//         iVanChuyenDK.classList.remove('errBox'); 
//         iVanChuyenDK.classList.remove('empty');
//     }
// })

// iLuuTruDK.addEventListener('change', () => {
//     if(iLuuTruDK.value != '' && iDiaChi.classList.contains('errBox') || iLuuTruDK.classList.contains('empty')) {
//         iLuuTruDK.classList.remove('errBox'); 
//         iLuuTruDK.classList.remove('empty');
//     }
// })

iKhacDK.addEventListener('change', () => {
    if(iKhacDK.value != '' && iDiaChi.classList.contains('errBox') || iKhacDK.classList.contains('empty')) {
        iKhacDK.classList.remove('errBox'); 
        iKhacDK.classList.remove('empty');
    }
})

var validateFormDieuKhoan = () => {
    //Validate Dieu Khoan
    // if(iVanChuyenDK.value == '') iVanChuyenDK.classList.add('empty');
    // if(iLuuTruDK.value == '') iLuuTruDK.classList.add('empty');
    if(iKhacDK.value == '') iKhacDK.classList.add('empty');

    for(let i=0; i<document.getElementsByClassName('empty').length; i++) {
        document.getElementsByClassName('empty')[i].classList.add('errBox');
        var flag = false;
    }
    if(flag == false) return false;
    else return true;    
}
document.getElementById('btnDieuKhoan').addEventListener('click', () => {
    if(validateFormDieuKhoan() == true) {
        swal(
        'Good job!',
        'Thêm Tour du lịch thành công !!',
        'success'
        ).then(() => {
            document.getElementById('myFormDieuKhoan').submit();
        })
    } else {
        swal(
            'Failure',
            'Bạn cần điền đầy đủ thông tin !',
            'error'
        )  
    }
})
//END Validate FORM DIEUKHOAN