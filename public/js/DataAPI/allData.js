const localhost = 'http://localhost:8080';

//  Check Params Query trên URL
var getQueryStringValue = (key) => {  
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
}  

//  
var setID = (idName, aData, aNameData, aMaData, arrNameData, arrMaData) => {
    switch (document.getElementById(idName).tagName) {
        case 'SPAN':
            document.getElementById(idName).innerHTML = aData;
            break;
        case 'INPUT':
            if(document.getElementById(idName).getAttribute('type') == 'checkbox') {
                if(aData == true || aData == 'true') {
                    document.getElementById(idName).checked = true;
                    document.getElementById(idName).value = true;
                }
                if(aData == false || aData == 'false'){
                    document.getElementById(idName).checked = false;
                    document.getElementById(idName).value = false;
                }
            } else {
                document.getElementById(idName).value = aData;
                break;  
            }
        case 'TEXTAREA':
            document.getElementById(idName).value = aData;
            break;  
        case 'IMG':
            document.getElementById(idName).src = `${localhost}/upload/tours/${aData}`;
            break;  
        case 'SELECT':
            if(aNameData != null && aMaData != null) {
                document.getElementById(idName).options[0].innerHTML = aNameData;
                document.getElementById(idName).options[0].value = aMaData;
                for(let i=0; i<arrNameData.length; i++) {
                    let option = document.createElement("option");
                    option.text = arrNameData[i];
                    option.value = arrMaData[i];
                    document.getElementById(idName).add(option);
                }
            } else {
                for(let i=0; i<arrNameData.length; i++) {
                    let option = document.createElement("option");
                    option.text = arrNameData[i];
                    option.value = arrMaData[i];
                    document.getElementById(idName).add(option);
                }
            }
            break;
        default:
            break;   
    }    
};

var setClass = (className, arrData) => {
    if(document.getElementsByClassName(className)[0].getAttribute('type') == 'checkbox') {
        arrData.forEach((item) => {
            if(arrData.indexOf(item) === -1) {
                console.log("STOP");
            } else {
                document.getElementById(item).checked = true;
            }            
        })
    } else {
        for(let i=0; i<arrData.length; i++) {
            switch (document.getElementsByClassName(className)[i].tagName) {
                case 'INPUT':
                    document.getElementsByClassName(className)[i].value = arrData[i];
                    break;
                case 'TEXTAREA':
                    document.getElementsByClassName(className)[i].value = arrData[i];
                    break;    
                case 'IMG':
                    document.getElementsByClassName(className)[i].src = `${localhost}/upload/tours/${arrData[i]}`;
                    break;              
                default:
                    break;
            }
        }
    }
};


// 
var loopDiv = (idName, className, html, divLenghth) => {
    if(divLenghth != 0) {
        if(idName) {
            for(let i=0; i<divLenghth; i++) {
                document.getElementById(idName).innerHTML += html;
            }
        }
        if(className) {
            for(let i=0; i<divLenghth; i++) {
                document.getElementsByClassName(className).innerHTML += html;
            }
        }
    } 
}

// Hiển thị ảnh trước khi Upload
var showImgBeforeUpload = (idInput, idImage) => {
    let reader = new FileReader();
    let input = document.getElementById(idInput);
    reader.onload = () => {
        let output = document.getElementById(idImage);
        output.src = reader.result;
    };
    reader.readAsDataURL(input.files[0]);
};

// handle Upload Anh
var handleUploadAnh = (idInput, idSpan, idImage) => {
    var input, span, image;
    input = document.getElementById(idInput);
    span = document.getElementById(idSpan);
    image = document.getElementById(idImage);
    showImgBeforeUpload(idInput, idImage);
    span.innerHTML = input.files[0].name;
}

//Thay đổi  ' -> ''
var replaceSingleQuote = (that) => {
    that.value = that.value.replace(/['|’]/g, "''");
}

// Scroll To Top

var scrollTo = (element, to, duration) => {
  if (duration <= 0) return;
  var difference = to - element.scrollTop;
  var perTick = difference / duration * 10;

  setTimeout(() => {
    element.scrollTop = element.scrollTop + perTick;
    if (element.scrollTop == to) return;
    scrollTo(element, to, duration - 10);
  }, 10);
}

// API
const API = {
    GET : {
        dataChiTietTour : (url, MaTour) => {
            return (
                axios.get(url + MaTour)
                .then(res =>  res.data)
                .catch(error => error)
            );
        },
        allDiaDiem : (url) => {
            return (
                axios.get(url)
                .then(res => res.data)
                .catch(error => error)
            );
        },
        groupDiaDiemKieuDD : (url, KieuDD) => {
            return (
                axios.get(url + KieuDD)
                .then(res => res.data)
                .catch(error => error)
            );
        },
        allDoiTac : (url) => {
            return (
                axios.get(url)
                .then(res => res.data)
                .catch(error => error)
            );
        },
        allToursWithCondition: (url, KieuDD, TenDD, SapXep) => {
            if(KieuDD != null && TenDD != null && SapXep != null) {
                return(
                    axios.get(`${url}?KieuDD=${KieuDD}&TenDD=${TenDD}&SapXep=${SapXep}`)
                    .then(res => res.data)
                    .catch(error => error)
                );
            }
            if(KieuDD != null && TenDD != null && SapXep == null) {
                return(
                    axios.get(`${url}?KieuDD=${KieuDD}&TenDD=${TenDD}`)
                    .then(res => res.data)
                    .catch(error => error)
                );
            }
            if(KieuDD != null && TenDD == null && SapXep == null) {
                return(
                    axios.get(`${url}?KieuDD=${KieuDD}`)
                    .then(res => res.data)
                    .catch(error => error)
                );
            }
            if(KieuDD == null && TenDD == null && SapXep == null) {
                return(
                    axios.get(url)
                    .then(res => res.data)
                    .catch(error => error)
                );
            }
        },
        allToursChoXuLy : (url) => {
            return (
                axios.get(url)
                .then(res => res.data)
                .catch(error => error)
            );
        },
        allToursDaXuLy : (url) => {
            return (
                axios.get(url)
                .then(res => res.data)
                .catch(error => error)
            );
        }
    },
    POST : {}
}


