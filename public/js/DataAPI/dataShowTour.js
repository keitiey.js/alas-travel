
const urlAllToursWithCondition = `${localhost}/api/tours/where`;
const urlGetKieuDD = `${localhost}/api/dia-diem/where`;

var loopTours = (index, MaTour, AnhTour, TenTour) => {
    let html = `
        <tr class="text-center">
            <td style="text-align: center;" >${index}</td>
            <td class="text-center">${MaTour}</td>
            <td>
                <img width="100%" src="${localhost}/upload/tours/${AnhTour}" />
            </td>
            <td>
                <p>
                    ${TenTour}
                </p>
            </td>
            <td>
                <a href="${localhost}/management/tours/chi-tiet-tour?MaTour=${MaTour}">
                    <button style="margin: 25% 0 0 0; width: 100%; height: 50px; " type="button" class="btn btn-success">Chi tiết TOUR</button>
                </a>
                <button style="margin: 5% 0 25% 0; width: 100%" onclick="handleDeleteBtn(this)" type="button" matour="${MaTour}" class="btn btn-danger">XÓA</button>
            </td>
        </tr>    
    `;
    return html;
}

var handleDeleteBtn = (that) => {
    console.log(that.getAttribute('matour'));
    swal({
    title: 'Bạn chắc chắn XÓA chứ?',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yep, tôi muốn xóa!'
    }).then(() => {
        axios.post(`${localhost}/api/tours/delete`, {
            MaTour: that.getAttribute('matour')
        })
        swal(
            'Deleted!',
            'Your file has been deleted.',
            'success'
        ).then(() => {
            window.location.reload();
        })
    })    
}


API.GET.allToursWithCondition(urlAllToursWithCondition, null, null, null)
.then((data) => {
    let bodyAllTours = document.getElementById('bodyAllTours');
    bodyAllTours.innerHTML = '';
    data.forEach((item, index) => {
        bodyAllTours.innerHTML += loopTours(index+1, item.MaTour, item.AnhTour, item.TenTour);
    })
})



const domKhuVuc = document.getElementById('khuVuc');
const domDiaDiem = document.getElementById('diaDiem');
const domSapXep = document.getElementById('sapXep');

domKhuVuc.addEventListener('change', () => {
    let KhuVuc;
    if(domKhuVuc.value == 'Tất cả') {
        KhuVuc = '';
    }else if(domKhuVuc.value == 'Trong nước') {
        KhuVuc = 'TrongNuoc';
    } else {
        KhuVuc = 'NgoaiNuoc';
    }
    if(KhuVuc == '') {
        axios.get(`${urlAllToursWithCondition}`)
        .then((result) => {
            domDiaDiem.innerHTML = '<option selected="selected">Chọn địa điểm</option>';
            let bodyAllTours = document.getElementById('bodyAllTours');
            bodyAllTours.innerHTML = '';
            result.data.forEach((item, index) => {
                bodyAllTours.innerHTML += loopTours(index+1, item.MaTour, item.AnhTour, item.TenTour);
            })
        })  
    } else {
        axios.get(`${urlGetKieuDD}?KieuDD=${KhuVuc}`)
        .then((result) => {
            // console.log(result.data);
            domDiaDiem.innerHTML = '<option selected="selected">Chọn địa điểm</option>';
            result.data.forEach((item, i) => {
                domDiaDiem.innerHTML += `<option>${item.TenDD}</option>`;
            })
            axios.get(`${urlAllToursWithCondition}?KieuDD=${domKhuVuc.value}`)
            .then((result) => {
                let bodyAllTours = document.getElementById('bodyAllTours');
                bodyAllTours.innerHTML = '';
                result.data.forEach((item, index) => {
                    bodyAllTours.innerHTML += loopTours(index+1, item.MaTour, item.AnhTour, item.TenTour);
                })            
            })
        })        
    }
})
domDiaDiem.addEventListener('change', () => {
    axios.get(`${urlAllToursWithCondition}?KieuDD=${domKhuVuc.value}&TenDD=${domDiaDiem.value}`)
    .then((result) => {
        let bodyAllTours = document.getElementById('bodyAllTours');
        bodyAllTours.innerHTML = '';
        result.data.forEach((item, index) => {
            bodyAllTours.innerHTML += loopTours(index+1, item.MaTour, item.AnhTour, item.TenTour);
        })            
    })    
})
domSapXep.addEventListener('change', () => {
    let valueSapXep;
    switch (domSapXep.value) {
        case 'Chọn cách sắp xếp':
            valueSapXep = 'MoiNhat';
            break;
        case 'Mới nhất':
            valueSapXep = 'MoiNhat';
            break;
        case 'Cũ nhất':
            valueSapXep = 'CuNhat';
            break;
        case 'A -> Z':
            valueSapXep = 'AtoZ';
            break;
        case 'Z -> A':
            valueSapXep = 'ZtoA';
            break;
        default:
            break;
    }
    if(domDiaDiem.value != 'Chọn địa điểm') {
        axios.get(`${urlAllToursWithCondition}?KieuDD=${domKhuVuc.value}&TenDD=${domDiaDiem.value}&SapXep=${valueSapXep}`)
        .then((result) => {
            console.log(`${urlAllToursWithCondition}?KieuDD=${domKhuVuc.value}&TenDD=${domDiaDiem.value}&SapXep=${valueSapXep}`);
            let bodyAllTours = document.getElementById('bodyAllTours');
            bodyAllTours.innerHTML = '';
            result.data.forEach((item, index) => {
                bodyAllTours.innerHTML += loopTours(index+1, item.MaTour, item.AnhTour, item.TenTour);
            })            
        }) 
    } else {
        axios.get(`${urlAllToursWithCondition}?SapXep=${valueSapXep}`)
        .then((result) => {
            console.log(`${urlAllToursWithCondition}?KieuDD=${domKhuVuc.value}&SapXep=${valueSapXep}`);
            let bodyAllTours = document.getElementById('bodyAllTours');
            bodyAllTours.innerHTML = '';
            result.data.forEach((item, index) => {
                bodyAllTours.innerHTML += loopTours(index+1, item.MaTour, item.AnhTour, item.TenTour);
            })            
        }) 
    }  
})