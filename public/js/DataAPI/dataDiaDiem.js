const btnXacNhan = document.getElementById('btnXacNhan');
const aKhuVuc = document.getElementById('khuVuc');
const opKhuVuc = document.getElementsByClassName('opKhuVuc');

const urlGetKieuDD = `${localhost}/api/dia-diem/where?KieuDD=`;
const urlGetAllDiaDiem = `${localhost}/api/dia-diem/select`;

const iMaDD = document.getElementById('MaDD');
const iTenDD = document.getElementById('TenDD');
const iAnhDD = document.getElementById('anhDiaDiemInput');

iMaDD.addEventListener('change', () => {
    if(iMaDD.value != '' && iMaDD.classList.contains('errBox') || iMaDD.classList.contains('empty')) {
        iMaDD.classList.remove('errBox'); 
        iMaDD.classList.remove('empty');
    }
})
iTenDD.addEventListener('change', () => {
    if(iTenDD.value != '' && iTenDD.classList.contains('errBox') || iTenDD.classList.contains('empty')) {
        iTenDD.classList.remove('errBox'); 
        iTenDD.classList.remove('empty');
    }
})
iAnhDD.addEventListener('change', () => {
    if(iAnhDD.value != '' && document.getElementById('errAnhDiaDiem').style.visibility != 'hidden') {
        document.getElementById('errAnhDiaDiem').style.visibility = 'hidden'; 
        document.getElementById('errAnhDiaDiem').classList.remove('empty');
    }
})

var validateForm = () => {
    if(iMaDD.value == '') iMaDD.classList.add('empty');
    if(iTenDD.value == '') iTenDD.classList.add('empty');
    if(iAnhDD.value == '') {
        document.getElementById('errAnhDiaDiem').style.visibility = 'unset';
        document.getElementById('errAnhDiaDiem').classList.add('empty');
    }
    for(let i=0; i<document.getElementsByClassName('empty').length; i++) {
        document.getElementsByClassName('empty')[i].classList.add('errBox');
        var flag = false;
    }
    if(flag == false) return false;
    else return true;
}

btnXacNhan.addEventListener('click', () => {
    if(validateForm() == true) {
        swal(
        'Good job!',
        'Thêm địa điểm thành công !!',
        'success'
        ).then(() => {
            document.getElementById('formDiaDiem').submit();
        })
    } else {
        swal(
            'Failure',
            'Bạn cần điền đầy đủ thông tin !',
            'error'
        )  
    }
})

var loopTours = (index, MaDD, TenDD, KieuDD, AnhDD) => {
    let html = `
        <tr class="text-center">
            <td style="text-align: center;" >${index}</td>
            <td class="text-center">${MaDD}</td>
            <td>
                <img width="100%" src="${localhost}/upload/tours/${AnhDD}" />
            </td>
            <td>
                <p>
                    ${TenDD}
                </p>
            </td>
            <td>
                <button style="margin: 5% 0 25% 0; width: 100%" onclick="handleDeleteBtn(this)" type="button" madd="${MaDD}" class="btn btn-danger">XÓA</button>
            </td>
        </tr>    
    `;
    return html;
}

var handleDeleteBtn = (that) => {
    axios.get(`${localhost}/api/dia-diem/so-luong-tour?MaDD=${that.getAttribute('madd')}`)
    .then((response) => {
        let slTour = response.data[0].count;
        if(slTour == 0) {
            // console.log('Xoa thoai mai');
            swal({
            title: 'Are you sure?',
            text: "Bạn chắc chắn muốn xóa chứ?!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yep, tôi muốn xóa!'
            }).then(function () {
                swal(
                    'Deleted!',
                    'Xóa địa điểm thành công!',
                    'success'
                )
                setTimeout(() => {
                    axios.post(`${localhost}/api/dia-diem/delete`, {
                        MaDD: that.getAttribute('madd')
                    })
                    .then((reponse) => {
                        console.log(response);
                        window.location.reload();
                    })
                },1000)
            })
        } else {
            // console.log('Khong xoa dc');
            swal(
                'Warning !!!',
                'Địa điểm này có Tour du lịch. Bạn không thể xóa được',
                'warning'
            )
        }
    })
    .catch((error) => {
        console.log(error);
    })
}

API.GET.allDiaDiem(urlGetAllDiaDiem)
.then((data) => {
    let bodyAllDiaDiem = document.getElementById('bodyAllDiaDiem');
    bodyAllDiaDiem.innerHTML = '';
    data.allDiaDiem.forEach((item, index) => {
        bodyAllDiaDiem.innerHTML += loopTours(index+1, item.MaDD, item.TenDD, item.KieuDD, item.AnhDD);
    })
    console.log(data);
})

aKhuVuc.addEventListener('change', () => {
    if(aKhuVuc.value == 'Tất cả') {
        API.GET.allDiaDiem(urlGetAllDiaDiem)
        .then((data) => {
            let bodyAllDiaDiem = document.getElementById('bodyAllDiaDiem');
            bodyAllDiaDiem.innerHTML = '';
            data.allDiaDiem.forEach((item, index) => {
                bodyAllDiaDiem.innerHTML += loopTours(index+1, item.MaDD, item.TenDD, item.KieuDD, item.AnhDD);
            })
        })        
    } else if(aKhuVuc.value == 'Trong nước') {
        API.GET.groupDiaDiemKieuDD(urlGetKieuDD, 'TrongNuoc')
        .then((data) => {
            let bodyAllDiaDiem = document.getElementById('bodyAllDiaDiem');
            bodyAllDiaDiem.innerHTML = '';
            data.forEach((item, index) => {
                bodyAllDiaDiem.innerHTML += loopTours(index+1, item.MaDD, item.TenDD, item.KieuDD, item.AnhDD);
            })
        })           
    } else {
        API.GET.groupDiaDiemKieuDD(urlGetKieuDD, 'NgoaiNuoc')
        .then((data) => {
            let bodyAllDiaDiem = document.getElementById('bodyAllDiaDiem');
            bodyAllDiaDiem.innerHTML = '';
            data.forEach((item, index) => {
                bodyAllDiaDiem.innerHTML += loopTours(index+1, item.MaDD, item.TenDD, item.KieuDD, item.AnhDD);
            })
        })  
    }
})