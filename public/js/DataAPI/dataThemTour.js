
const urlDiaDiem = `${ localhost }/api/dia-diem/select`;
const urlGroupDiaDiem = `${ localhost }/api/dia-diem/where?KieuDD=`;
const urlDoiTac = `${ localhost }/api/doi-tac/select`;

const iMaTour    = document.getElementById('maTour');
const iGiaTour   = document.getElementById('giaTour');
const iTenTour   = document.getElementById('tenTour');
const iThoiGian  = document.getElementById('thoiGian');
const iGioiThieu = document.getElementById('gioiThieu');
const cPhuongTien = document.getElementsByClassName('phuongTien');
const nDiaDiem = document.getElementsByName('diaDiem');
const iAnhTour = document.getElementById('anhTourInput');
const iSoNgay = document.getElementById('soNgay');
const cDichVu = document.getElementsByClassName('dichVu');
const iMaDoiTac = document.getElementById('maDoiTac');
const iTenDoiTac = document.getElementById('tenDoiTac');
const iDiaChi = document.getElementById('diachiDoiTac');
const iMaDK = document.getElementById('maDK');
const iVanChuyenDK = document.getElementById('vanChuyenDK');
const iLuuTruDK = document.getElementById('luuTruDK');
const iKhacDK = document.getElementById('khacDK');


const DataThemTour = {
    id : {
        DiaDiem     : (DiaDiem, arrNameData, arrMaDiaDiem) => { setID(DiaDiem, null, null, null, arrNameData, arrMaDiaDiem) }
        
    }
    // class : {

    // },
}

var changeKhuVuc = (idKhuVuc, idDiaDiemTN, idDiaDiemNN) => {
    var domKhuVuc = document.getElementById(idKhuVuc);
    for(let i=0; i < domKhuVuc.options.length; i++) {
        if(domKhuVuc.options[i].selected == true) {
            let KieuDD = domKhuVuc.options[i].value;
            if(KieuDD == 'Trong nước') {
                KieuDD = 'TrongNuoc'
            } else {
                KieuDD = 'NgoaiNuoc';
            }
            API.GET.groupDiaDiemKieuDD(urlGroupDiaDiem, KieuDD)
            .then((data) => {
                let arrNameDiaDiem = new Array();
                let arrMaDiaDiem = new Array();
                data.forEach((item) => {
                    arrNameDiaDiem.push(item.TenDD);
                    arrMaDiaDiem.push(item.MaDD);
                })
                if(KieuDD == 'TrongNuoc') {
                    document.getElementById('TrongNuoc').setAttribute('name', 'diaDiem');
                    document.getElementById('NgoaiNuoc').removeAttribute("name");
                    document.getElementById('TrongNuoc').style.display = 'inline'
                    document.getElementById('NgoaiNuoc').style.display = 'none';
                    while (document.getElementById(KieuDD).options.length == 0) {
                        DataThemTour.id.DiaDiem(KieuDD, arrNameDiaDiem, arrMaDiaDiem);
                    }
                } else {
                    document.getElementById('NgoaiNuoc').setAttribute('name', 'diaDiem');
                    document.getElementById('TrongNuoc').removeAttribute("name");
                    document.getElementById('NgoaiNuoc').style.display = 'inline';
                    document.getElementById('TrongNuoc').style.display = 'none'
                    while (document.getElementById(KieuDD).options.length == 0) {
                        DataThemTour.id.DiaDiem(KieuDD, arrNameDiaDiem, arrMaDiaDiem);
                    }                    
                }
            })
        }
    }
}

var handleMaDieuKhoan = (idInput, idOutput, idSpan) => {
    let input, output, span;
    input = document.getElementById(idInput);
    output = document.getElementById(idOutput);
    span = document.getElementById(idSpan);
    output.value = 'DK' + input.value;
    span.innerHTML = 'DK' + input.value;
}


var handleShowLichTrinh = (idSoNgay, idPlaceOfDisplay) => {
    let soNgay, placeOfDisplay;
    soNgay = document.getElementById(idSoNgay);
    placeOfDisplay = document.getElementById(idPlaceOfDisplay);
    let html = `
        <div class="row lich-trinh-ngay">
            <div class="col-md-8">
                <div class="form-inline" style="margin-bottom: 10px">
                    <label for="ngayLichTrinh">Ngày: </label>
                    <input type="number" min="1" class="form-control ngayLichTrinh" name="ngayLichTrinh" id="ngayLichTrinh">
                </div>
                <div class="form-group">
                    <label for="noiDung1">Nội dung 1: </label>
                    <textarea type="text" onchange="replaceSingleQuote(this)" class="form-control noiDung1" name="noiDung1"></textarea>
                </div>
                <div class="form-group">
                    <label for="noiDung2">Nội dung 2: </label>
                    <textarea type="text" onchange="replaceSingleQuote(this)" class="form-control noiDung2" name="noiDung2"></textarea>
                </div>                                
            </div> 
            <div class="col-md-4">     
                <div class="anh-lich-trinh-tour">
                    <img id="" class="anhLichTrinh" src="/images/default.png" />     
                </div>     
                <label for="" class="custom-file-upload anhLichTrinhLabel">
                    <i class="fa fa-upload" aria-hidden="true"></i> Chọn ảnh
                </label>
                <input id="" class="anhLichTrinhInput" name="anhLichTrinhInput" type="file"/><span id="" class="anhLichTrinhSpan"></span>                      
                <span style="display: block; visibility:hidden" class="errMessage errAnhLichTrinh">Ảnh không được bỏ trống</span>
            </div>
        </div>      
    `;
    placeOfDisplay.innerHTML = '';
    for(let i=0; i<soNgay.value; i++) {
        placeOfDisplay.innerHTML += html;
    }
    if(placeOfDisplay.innerHTML != null) {
        let anhLichTrinh, anhLichTrinhInput, anhLichTrinhSpan, anhLichTrinhLabel, ngayLichTrinh, noiDung1, noiDung2;
        ngayLichTrinh = document.querySelectorAll('.ngayLichTrinh');
        noiDung1 = document.querySelectorAll('.noiDung1');
        noiDung2 = document.querySelectorAll('.noiDung2');
        errAnhLichTrinh = document.querySelectorAll('.errAnhLichTrinh');
        anhLichTrinh = document.querySelectorAll('.anhLichTrinh');
        anhLichTrinhInput = document.querySelectorAll('.anhLichTrinhInput');
        anhLichTrinhSpan = document.querySelectorAll('.anhLichTrinhSpan');
        anhLichTrinhLabel = document.querySelectorAll('.anhLichTrinhLabel');
        // Set ID
        for(let i=0; i<soNgay.value; i++) {
            ngayLichTrinh[i].setAttribute('id', 'ngayLichTrinh' + i);
            noiDung1[i].setAttribute('id', 'noiDung1' + i);
            noiDung2[i].setAttribute('id', 'noiDung2' + i);
            errAnhLichTrinh[i].setAttribute('id', 'errAnhLichTrinh' + i);
            anhLichTrinh[i].setAttribute('id', 'anhLichTrinh' + i);
            anhLichTrinhInput[i].setAttribute('id', 'anhLichTrinhInput' + i);
            anhLichTrinhSpan[i].setAttribute('id', 'anhLichTrinhSpan' + i);
            anhLichTrinhLabel[i].setAttribute('for', 'anhLichTrinhInput' + i);
            let idInput = 'anhLichTrinhInput' + i;
            let idSpan = 'anhLichTrinhSpan' + i;
            let idImage = 'anhLichTrinh' + i;
            document.getElementById(idInput).addEventListener('change', () => {
                handleUploadAnh(idInput, idSpan, idImage)
            });
        }      
    }
}

//  Input Change
iMaTour.addEventListener('change', () => { 
    if(iMaTour.value != '' && iMaTour.classList.contains('errBox') || iMaTour.classList.contains('empty'))  {
        iMaTour.classList.remove('errBox');
        iMaTour.classList.remove('empty');
    }
})
iGiaTour.addEventListener('change', () => {
    if(iGiaTour.value != '' && iGiaTour.classList.contains('errBox') || iGiaTour.classList.contains('empty')) {
        iGiaTour.classList.remove('errBox'); 
        iGiaTour.classList.remove('empty');
    }
})
iTenTour.addEventListener('change', () => {
    if(iTenTour.value != '' && iTenTour.classList.contains('errBox') || iTenTour.classList.contains('empty')) {
        iTenTour.classList.remove('errBox'); 
        iTenTour.classList.remove('empty'); 
    }
})
iThoiGian.addEventListener('change', () => {
    if(iThoiGian.value != '' && iThoiGian.classList.contains('errBox') || iThoiGian.classList.contains('empty')) {
        iThoiGian.classList.remove('errBox'); 
        iThoiGian.classList.remove('empty');
    }
})
iGioiThieu.addEventListener('change', () => {
    if(iGioiThieu.value != '' && iGioiThieu.classList.contains('errBox') || iGioiThieu.classList.contains('empty')) {
        iGioiThieu.classList.remove('errBox'); 
        iGioiThieu.classList.remove('empty');
    }
})
for(let i=0; i<cPhuongTien.length; i++) {
    cPhuongTien[i].addEventListener('change', () => {
        if(cPhuongTien[i].checked == true && document.getElementById('errPhuongTien').style.visibility != 'hidden') {
            document.getElementById('errPhuongTien').style.visibility = 'hidden'; 
            document.getElementById('errPhuongTien').classList.remove('empty');
        }
    })
}

document.getElementById('khuVuc').addEventListener('change', () => {
    document.getElementById('errDiaDiem').style.visibility = 'hidden';
    document.getElementById('errDiaDiem').classList.remove('empty');
})

iAnhTour.addEventListener('change', () => {
    if(iAnhTour.value != '' && document.getElementById('errAnhTour').style.visibility != 'hidden') {
        document.getElementById('errAnhTour').style.visibility = 'hidden'; 
        document.getElementById('errAnhTour').classList.remove('empty');
    }
})
iSoNgay.addEventListener('change', () => {
    if(iSoNgay.value != '' && document.getElementById('errSoNgay').style.visibility != 'hidden') {
        document.getElementById('errSoNgay').style.visibility = 'hidden';
        document.getElementById('errSoNgay').classList.remove('empty');
    }
})
document.getElementById('btnCapNhat').addEventListener('click', () => {
    if(iSoNgay.value != '') {
        for(let i=0; i<iSoNgay.value; i++) {            
            let idNgay = 'ngayLichTrinh' + i;
            let idNoiDung1 = 'noiDung1' + i;
            let idNoiDung2 = 'noiDung2' + i;
            let idAnhLichTrinhInput = 'anhLichTrinhInput' + i;
            let idErrAnhLichTrinh = 'errAnhLichTrinh' + i;
            let elementNgay = document.getElementById(idNgay);
            let elementNoiDung1 = document.getElementById(idNoiDung1);
            let elementNoiDung2 = document.getElementById(idNoiDung2);
            let elementAnhLichTrinhInput = document.getElementById(idAnhLichTrinhInput);
            if (typeof(elementNgay) != 'undefined' && elementNgay != null) {
                elementNgay.addEventListener('change', () => {
                    if(elementNgay.value != '' && elementNgay.classList.contains('errBox') || elementNgay.classList.contains('empty')) {
                        elementNgay.classList.remove('errBox'); 
                        elementNgay.classList.remove('empty');                        
                    }
                })
            }
            if (typeof(elementNoiDung1) != 'undefined' && elementNoiDung1 != null) {
                elementNoiDung1.addEventListener('change', () => {
                    if(elementNoiDung1.value != '' && elementNoiDung1.classList.contains('errBox') || elementNoiDung1.classList.contains('empty')) {
                        elementNoiDung1.classList.remove('errBox'); 
                        elementNoiDung1.classList.remove('empty');                         
                    }
                })
            }
            if (typeof(elementNoiDung2) != 'undefined' && elementNoiDung2 != null) {
                elementNoiDung2.addEventListener('change', () => {
                    if(elementNoiDung2.value != '' && elementNoiDung2.classList.contains('errBox') || elementNoiDung2.classList.contains('empty')) {
                        elementNoiDung2.classList.remove('errBox'); 
                        elementNoiDung2.classList.remove('empty')                        
                    }

                })
            }
            if (typeof(elementAnhLichTrinhInput) != 'undefined' && elementAnhLichTrinhInput != null) {
                elementAnhLichTrinhInput.addEventListener('change', () => {
                    if(elementAnhLichTrinhInput.value != '' && document.getElementById(idErrAnhLichTrinh).style.visibility != 'hidden') {
                        document.getElementById(idErrAnhLichTrinh).style.visibility = 'hidden'; 
                        document.getElementById(idErrAnhLichTrinh).classList.remove('empty');
                    }
                })
            }
        }
    }
})
// Dich Vu
for(let i=0; i<cDichVu.length; i++) {
    cDichVu[i].addEventListener('change', () => {
        if(cDichVu[i].checked == true && document.getElementById('errDichVu').style.visibility != 'hidden') {
            document.getElementById('errDichVu').style.visibility = 'hidden'; 
            document.getElementById('errDichVu').classList.remove('empty');
        }
    })
}
iMaDoiTac.addEventListener('change', () => {
    if(iMaDoiTac.value != '' && iMaDoiTac.classList.contains('errBox') || iMaDoiTac.classList.contains('empty')) {
        iMaDoiTac.classList.remove('errBox'); 
        iMaDoiTac.classList.remove('empty');
    }
})

iTenDoiTac.addEventListener('change', () => {
    if(iTenDoiTac.value != '' && iTenDoiTac.classList.contains('errBox') || iTenDoiTac.classList.contains('empty')) {
        iTenDoiTac.classList.remove('errBox'); 
        iTenDoiTac.classList.remove('empty');
    }
})

iDiaChi.addEventListener('change', () => {
    if(iDiaChi.value != '' && iDiaChi.classList.contains('errBox') || iDiaChi.classList.contains('empty')) {
        iDiaChi.classList.remove('errBox'); 
        iDiaChi.classList.remove('empty');
    }
})

// iVanChuyenDK.addEventListener('change', () => {
//     if(iVanChuyenDK.value != '' && iDiaChi.classList.contains('errBox') || iVanChuyenDK.classList.contains('empty')) {
//         iVanChuyenDK.classList.remove('errBox'); 
//         iVanChuyenDK.classList.remove('empty');
//     }
// })

// iLuuTruDK.addEventListener('change', () => {
//     if(iLuuTruDK.value != '' && iDiaChi.classList.contains('errBox') || iLuuTruDK.classList.contains('empty')) {
//         iLuuTruDK.classList.remove('errBox'); 
//         iLuuTruDK.classList.remove('empty');
//     }
// })

iKhacDK.addEventListener('change', () => {
    if(iKhacDK.value != '' && iDiaChi.classList.contains('errBox') || iKhacDK.classList.contains('empty')) {
        iKhacDK.classList.remove('errBox'); 
        iKhacDK.classList.remove('empty');
    }
})


//  Validate FORM
var validateForm = () => {
    if(iMaTour.value == '') iMaTour.classList.add('empty');
    if(iGiaTour.value == '') iGiaTour.classList.add('empty');
    if(iTenTour.value == '') iTenTour.classList.add('empty');
    if(iThoiGian.value == '') iThoiGian.classList.add('empty');
    if(iGioiThieu.value == '') iGioiThieu.classList.add('empty');
    //  Validate Phuong Tien
    var tempPhuongTien = 0;
    for(let i=0; i<cPhuongTien.length; i++) {
        if(cPhuongTien[i].checked == true) tempPhuongTien ++;
    }
    if(tempPhuongTien == 0) {
        document.getElementById('errPhuongTien').style.visibility = 'unset';
        document.getElementById('errPhuongTien').classList.add('empty');
    }
    //  Validate Dia Diem
    if(nDiaDiem.length == 2) {
        document.getElementById('errDiaDiem').style.visibility = 'unset';
        document.getElementById('errDiaDiem').classList.add('empty');
        // setTimeout(() => {document.getElementById('errDiaDiem').style.visibility ='hidden';}, 5000);
    }
    //  Validate Anh Tour
    if(iAnhTour.value == '') {
        document.getElementById('errAnhTour').style.visibility = 'unset';
        document.getElementById('errAnhTour').classList.add('empty');
    }
    // Validate Lich Trinh Tour
    if(iSoNgay.value == '') {
        document.getElementById('errSoNgay').style.visibility = 'unset';
        document.getElementById('errSoNgay').classList.add('empty');
    }
    // Validate Ngay, NoiDung1, NoiDung2, AnhLichTrinh
    if(iSoNgay.value != '') {
        for(let i=0; i<iSoNgay.value; i++) {
            let idNgay = 'ngayLichTrinh' + i;
            let idNoiDung1 = 'noiDung1' + i;
            let idNoiDung2 = 'noiDung2' + i;
            let idAnhLichTrinhInput = 'anhLichTrinhInput' + i;
            let idErrAnhLichTrinh = 'errAnhLichTrinh' + i;
            let elementNgay = document.getElementById(idNgay);
            let elementNoiDung1 = document.getElementById(idNoiDung1);
            let elementNoiDung2 = document.getElementById(idNoiDung2);
            let elementAnhLichTrinhInput = document.getElementById(idAnhLichTrinhInput);
            if (typeof(elementNgay) != 'undefined' && elementNgay != null) {
                if(elementNgay.value == '') elementNgay.classList.add('empty');
            }
            if (typeof(elementNoiDung1) != 'undefined' && elementNoiDung1 != null) {
                if(elementNoiDung1.value == '') elementNoiDung1.classList.add('empty');
            }
            if (typeof(elementNoiDung2) != 'undefined' && elementNoiDung2 != null) {
                if(elementNoiDung2.value == '') elementNoiDung2.classList.add('empty');
            }
            if (typeof(elementAnhLichTrinhInput) != 'undefined' && elementAnhLichTrinhInput != null) {
                if(elementAnhLichTrinhInput.value == '') {
                    document.getElementById(idErrAnhLichTrinh).style.visibility = 'unset';
                    document.getElementById(idErrAnhLichTrinh).classList.add('empty');
                }
            }
        }
    }
    //  Validate Dich Vu
    var tempDichVu = 0;
    for(let i=0; i<cDichVu.length; i++) {
        if(cDichVu[i].checked == true) tempDichVu ++;
    }
    if(tempDichVu == 0) {
        document.getElementById('errDichVu').style.visibility = 'unset';
        document.getElementById('errDichVu').classList.add('empty');
    }    
    // Validate Doi Tac
    if(iMaDoiTac.value == '') iMaDoiTac.classList.add('empty');
    if(iTenDoiTac.value == '') iTenDoiTac.classList.add('empty');
    if(iDiaChi.value == '') iDiaChi.classList.add('empty');

    //Validate Dieu Khoan
    // if(iVanChuyenDK.value == '') iVanChuyenDK.classList.add('empty');
    // if(iLuuTruDK.value == '') iLuuTruDK.classList.add('empty');
    if(iKhacDK.value == '') iKhacDK.classList.add('empty');

    for(let i=0; i<document.getElementsByClassName('empty').length; i++) {
        document.getElementsByClassName('empty')[i].classList.add('errBox');
        var flag = false;
    }
    if(flag == false) return false;
    else return true;
}

document.getElementById('btnReset').addEventListener('click', () => {
    location.reload();
})

document.getElementById('btnXacNhan').addEventListener('click', () => {
    if(validateForm() == true) {
        swal(
        'Good job!',
        'Thêm Tour du lịch thành công !!',
        'success'
        ).then(() => {
            document.getElementById('myForm').submit();
        })
    } else {
        swal(
            'Failure',
            'Bạn cần điền đầy đủ thông tin !',
            'error'
        ).then( () => {
            scrollTo(document.body, 0, 700)
        })    
    }
})

var loopDoiTac = (MaDT, TenDT, DiaChi) => {
    let html = `
        <tr class="text-center">
            <td class="text-center">
                <p class="maDT">${MaDT}</p>
            </td>
            <td>
                <p class="tenDT">${TenDT}</p>
            </td>
            <td>
                <p class="diaChiDT">${DiaChi}</p>
            </td>
            <td>
                <button style="margin: 5% 0 0 0; width: 100%; " madt=${MaDT} tendt="${TenDT}" diachidoitac="${DiaChi}" onclick="setDoiTac(this)" type="button" class="btn btn-success">Chọn</button>
            </td>
        </tr>            
    `;
    return html;
}

var setDoiTac = (that) => {
    iMaDoiTac.value = that.getAttribute('madt');
    iTenDoiTac.value = that.getAttribute('tendt');
    iDiaChi.value = that.getAttribute('diachidoitac');
}

// Tìm đối tác
var searchDoiTac = () => {
    let keySearchDoiTac = document.getElementById('keySearchDoiTac');
    let urlSearchDoiTac = `${localhost}/api/doi-tac/select?MaDT=${keySearchDoiTac.value}&TenDT=${keySearchDoiTac.value}&DiaChi=${keySearchDoiTac.value}`;
    API.GET.allDoiTac(urlSearchDoiTac)
        .then((data) => {
            let bodyModalShowDoiTac = document.getElementById('bodyModalShowDoiTac');
            bodyModalShowDoiTac.innerHTML = '';
            data.forEach((item) => {
                bodyModalShowDoiTac.innerHTML += loopDoiTac(item.MaDT, item.TenDT, item.DiaChi);
            })
        })
}

// Show Đối Tác
document.getElementById('btnShowDT').addEventListener('click', () => {
    API.GET.allDoiTac(urlDoiTac)
        .then((data) => {
            let bodyModalShowDoiTac = document.getElementById('bodyModalShowDoiTac');
            bodyModalShowDoiTac.innerHTML = '';
            data.forEach((item) => {
                bodyModalShowDoiTac.innerHTML += loopDoiTac(item.MaDT, item.TenDT, item.DiaChi);
            })
        })
})