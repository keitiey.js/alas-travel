const btnComeBack = document.getElementById('comeBack');
const btnHuyTour = document.getElementById('btnHuyTour');

window.onload = () => {
    if(document.getElementById('xacNhanDatTour').value == 'true') {

        document.getElementById('xacNhanDatTour').checked = true;

    } else {
        
        document.getElementById('xacNhanDatTour').checked = false;

    }
}
btnComeBack.addEventListener('click', () => {
    window.location.href = `${ localhost }/management/tours/tours-da-xu-ly`;
})

var handleHuyTour = (MaDon) => {
    swal({
        title: 'Are you sure?',
        text: "Bạn có chắc chắn muốn hủy Tour này chứ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
        }).then(() => {
            swal(
                'Deleted!',
                'Đơn đặt Tour đã được hủy',
                'success'
            ).then(() => {
                let url = `${ localhost }/api/tours-da-xu-ly/huy-tour`;
                axios.post(url, {
                    MaDon: MaDon
                })
                .then((response) => { 
                    console.log(response);
                    window.location.href=`${ localhost }/management/tours/tours-da-xu-ly`; 
                })
                .catch((error) => { console.log(error) })
            })
            }, (dismiss) => {
                if (dismiss === 'cancel') {
                    swal(
                    'Cancelled',
                    'Đơn đặt Tour đã được giữ lại',
                    'error'
                    )
            }
    })    
}