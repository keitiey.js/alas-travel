
const urlAllDiaDiemChoXuLy = `${localhost}/api/tours-chua-xu-ly/select`;

var loopTours = (index, MaDon, MaTour, AnhTour, TenTour, NgayDat) => {
    let html = `
        <tr class="text-center">
            <td style="text-align: center;" >${index}</td>
            <td class="text-center">${MaTour}</td>
            <td>
                <img width="100%" src="${localhost}/upload/tours/${AnhTour}" />
            </td>
            <td>
                <p>
                    ${TenTour}
                </p>
            </td>
            <td>
                <p>
                    ${NgayDat}
                </p>
            </td>
            <td>
                <a href="${localhost}/management/tours/tours-dang-cho/${MaDon}">
                    <button style="margin: 25% 0 0 0; width: 100%; height: 50px; " type="button" class="btn btn-success">Chi tiết đơn</button>
                </a>
                
            </td>
        </tr>    
    `;
    return html;
}

// var handleDeleteBtn = (that) => {
//     console.log(that.getAttribute('matour'));
//     swal({
//     title: 'Bạn chắc chắn XÓA chứ?',
//     type: 'warning',
//     showCancelButton: true,
//     confirmButtonColor: '#3085d6',
//     cancelButtonColor: '#d33',
//     confirmButtonText: 'Yep, tôi muốn xóa!'
//     }).then(() => {
//         axios.post(`${localhost}/api/tours/delete`, {
//             MaTour: that.getAttribute('matour')
//         })
//         swal(
//             'Deleted!',
//             'Your file has been deleted.',
//             'success'
//         ).then(() => {
//             window.location.reload();
//         })
//     })    
// }


API.GET.allToursChoXuLy(urlAllDiaDiemChoXuLy)
.then((data) => {
    let bodyAllTours = document.getElementById('bodyAllTours');
    bodyAllTours.innerHTML = '';
    data.forEach((item, index) => {
        bodyAllTours.innerHTML += loopTours(index+1, item.MaDon, item.MaTour, item.AnhTour, item.TenTour, item.ThoiGianNhap);
    })
})

