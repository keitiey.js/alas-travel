const IXacNhanDatTour = document.getElementById('xacNhanDatTour');
const btnXacNhanDatTour = document.getElementById('btnXacNhanDatTour');
const btnXoaHoaDon = document.getElementById('btnXoaHoaDon');

window.onload = () => {
    if(IXacNhanDatTour.value == 'true') {

        IXacNhanDatTour.checked = true;

    } else {
        
        IXacNhanDatTour.checked = false;

    }
}
IXacNhanDatTour.addEventListener('change', () => {
    if(IXacNhanDatTour.value == 'true') {
        IXacNhanDatTour.value = 'false';
        document.getElementById('errMess').style.display = 'block';
        console.log(IXacNhanDatTour.value);
    } else {
        IXacNhanDatTour.value = 'true';
        console.log(IXacNhanDatTour.value);
        document.getElementById('errMess').style.display = 'none';
    }
})

var handleXacNhanTour = (MaDon) => {
    if(IXacNhanDatTour.value == 'false') {
        document.getElementById('errMess').style.display = 'block';
        swal(
            'Failure!',
            'Bạn cần xác nhận đơn đặt tour trước.',
            'error'
        )          
    } else {
        document.getElementById('errMess').style.display = 'none';
        swal(
            'Good Job!',
            'Xác nhận tour thành công',
            'success'
        ).then(() => {
            let urlPostCheckTour = `${ localhost }/api/tours-chua-xu-ly/checked`;
            axios.post(
                urlPostCheckTour,
                { MaDon: MaDon }
            )
            .then((response) => {
                console.log(response);
                window.location.href=`${ localhost }/management/tours/tours-dang-cho`;
            })
            .catch((error) => {
                console.log(error);
            })
        })          
    }  
}

var handleXoaHoaDon = (MaDon) => {
    swal({
        title: 'Are you sure?',
        text: "Bạn có chắc chắn muốn xóa hóa đơn chứ?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
        }).then(() => {
            swal(
                'Deleted!',
                'Đơn đặt Tour đã được xóa',
                'success'
            ).then(() => {
                let url = `${ localhost }/api/tours-chua-xu-ly/delete`;
                axios.post(url, {
                    MaDon: MaDon
                })
                .then((response) => { 
                    console.log(response);
                    window.location.href=`${ localhost }/management/tours/tours-dang-cho`; 
                })
                .catch((error) => { console.log(error) })
            })
            }, (dismiss) => {
                if (dismiss === 'cancel') {
                    swal(
                    'Cancelled',
                    'Đơn đặt Tour đã được giữ lại',
                    'error'
                    )
            }
    })
}
