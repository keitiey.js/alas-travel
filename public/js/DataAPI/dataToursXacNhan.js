
const urlAllDiaDiemDaXuLy = `${localhost}/api/tours-da-xu-ly/select`;

var loopTours = (index, MaDon, MaTour, AnhTour, TenTour, NgayDat) => {
    let html = `
        <tr class="text-center">
            <td style="text-align: center;" >${index}</td>
            <td class="text-center">${MaTour}</td>
            <td>
                <img width="100%" src="${localhost}/upload/tours/${AnhTour}" />
            </td>
            <td>
                <p>
                    ${TenTour}
                </p>
            </td>
            <td>
                <p>
                    ${NgayDat}
                </p>
            </td>
            <td>
                <a href="${localhost}/management/tours/tours-da-xu-ly/${MaDon}">
                    <button style="margin: 25% 0 0 0; width: 100%; height: 50px; " type="button" class="btn btn-success">Chi tiết đơn</button>
                </a>
                
            </td>
        </tr>    
    `;
    return html;
}


API.GET.allToursDaXuLy(urlAllDiaDiemDaXuLy)
.then((data) => {
    let bodyAllTours = document.getElementById('bodyAllTours');
    bodyAllTours.innerHTML = '';
    data.forEach((item, index) => {
        bodyAllTours.innerHTML += loopTours(index+1, item.MaDon, item.MaTour, item.AnhTour, item.TenTour, item.ThoiGianNhap);
    })
})

