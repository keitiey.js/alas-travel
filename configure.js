const localhost = 'http://localhost:8080';

module.exports = {
    localhost,
    urlChiTietTour: `${localhost}/api/tours/select?MaTour=`,
    urlTrangChu: `${localhost}/api/trangchu/select`,
    urlXemThemTour: `${localhost}/api/xem-them/where?Go=`,
    urlTourDaXem: {
        insert: `${localhost}/api/tours-da-xem/insert`
    }
};
