import express from 'express';
import React from 'react';

import { matchRoutes, renderRoutes } from 'react-router-config';
import { renderToString } from 'react-dom/server';
import StaticRouter from 'react-router-dom/StaticRouter';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import routes from '../App/routes';
import reducers from '../App/Redux/Reducer/reducer';

const router = express.Router();
const store = createStore(reducers, applyMiddleware(thunk));

router.get('*', (req, res) => {
    const branch = matchRoutes(routes, req.url);
    const promises = branch.map(({ route }) => {
        const fetchData = route.component.fetchData;
        return fetchData instanceof Function ? fetchData(store) : Promise.resolve(null);
    });

    return Promise.all(promises).then(() => {
        const context = {};
        const content = renderToString(
            <Provider store={store}>
                <StaticRouter location={req.url} context={context}>
                    {renderRoutes(routes)}
                </StaticRouter>
            </Provider>
        );
        if (context.status === 404) {
            res.status(404);
        }
        if (context.status === 302) {
            return res.redirect(302, context.url);
        }
        res.render('index', { title: "ALA's", data: store.getState(), content });
    });
});

module.exports = router;
