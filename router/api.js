const express = require('express');
const nodemailer = require('../Libs/NodeMailer');

const router = express.Router();
const {
    handleAQuery,
    handleQueries,
    handleInsertTables,
    handleDeleteTables,
    handleUpdateTables
} = require('../Libs/Database');

const { urlencodedParser } = require('../Libs/BodyParser');
const { cpUpload } = require('../Libs/Multer');
const { moneyToNumber } = require('../global');

// DOI_TAC
router.get('/doi-tac/select', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    let aQuery;
    if (req.query.MaDT != null && req.query.TenDT != null && req.query.DiaChi != null) {
        aQuery = `
            SELECT * 
            FROM "DOI_TAC"
            WHERE "MaDT" LIKE '%${req.query.MaDT}%'
                OR "TenDT" LIKE '%${req.query.TenDT}%'
                OR "DiaChi" LIKE '%${req.query.DiaChi}%'
        `;
    } else {
        aQuery = `
            SELECT *
            FROM "DOI_TAC"
        `;
    }
    handleAQuery(aQuery, (result) => {
        res.send(JSON.stringify(result.rows, null, 3));
        res.end();
    });
});


// CHITIETTOUR
router.get('/tours/select', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const arrQueries = [];

    // Query lay thong tin tour.
    arrQueries[0] = `
        SELECT "MaTour", "TenTour", "AnhTour", "ThoiGian", "GioiThieu", "GiaTour", "ThoiGianNhap"
        FROM "TOURS"
        WHERE "MaTour" = '${req.query.MaTour}'
    `;

    // Query lay thong tin Lich Trinh Tour
    arrQueries[1] = `
        SELECT "Ngay", "Anh", "NoiDung1", "NoiDung2" 
        FROM "CT_LICH_TRINH_TOUR"
        WHERE "MaTour" = '${req.query.MaTour}'    
    `;

    // Query lay dich vu 
    arrQueries[2] = `
        SELECT "DICH_VU"."MaDV", "TenDV" 
        FROM "DICH_VU", "CT_DICH_VU"
        WHERE 
            "DICH_VU"."MaDV" = "CT_DICH_VU"."MaDV"
            AND "CT_DICH_VU"."MaTour" = '${req.query.MaTour}' 
    `;

    // Query lay Phuong Tien
    arrQueries[3] = `
        SELECT "PHUONG_TIEN"."MaPT", "TenPT" 
        FROM "PHUONG_TIEN", "CT_PHUONG_TIEN"
        WHERE 
            "PHUONG_TIEN"."MaPT" = "CT_PHUONG_TIEN"."MaPT"
            AND "CT_PHUONG_TIEN"."MaTour" = '${req.query.MaTour}'     
    `;

    // Query lay Doi Tac
    arrQueries[4] = `
        SELECT "DOI_TAC"."MaDT", "TenDT", "DiaChi" 
        FROM "TOURS", "DOI_TAC"
        WHERE 
            "TOURS"."MaDT" = "DOI_TAC"."MaDT"
            AND "TOURS"."MaTour" = '${req.query.MaTour}'     
    `;

    // Query lay Dia Diem
    arrQueries[5] = `
        SELECT "DIA_DIEM"."MaDD", "TenDD", "KieuDD" 
        FROM "DIA_DIEM", "CT_DIA_DIEM"
        WHERE 
            "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
            AND "CT_DIA_DIEM"."MaTour" = '${req.query.MaTour}'     
    `;

    // Query lay Dieu Khoan
    arrQueries[6] = `
        SELECT "MaDK", "VanChuyen", "LuuTru", "Khac"
        FROM "TOURS", "DIEU_KHOAN"
        WHERE 
            "TOURS"."MaTour" = "DIEU_KHOAN"."MaTour"
            AND "DIEU_KHOAN"."MaTour" = '${req.query.MaTour}'    
    `;

    // Query lay Danh sach Dia Diem
    arrQueries[7] = `
        SELECT * FROM "DIA_DIEM"    
    `;

    // Query lay Tours da xem
    arrQueries[8] = `
        SELECT *
        FROM "TOURS_DA_XEM"
        	JOIN "TOURS" ON "TOURS_DA_XEM"."MaTour" = "TOURS"."MaTour"
        WHERE "IP" = '${req.sessionID}'
        ORDER BY "TOURS_DA_XEM"."ThoiGian" DESC
        LIMIT 3      
    `;
    // Query lay Dia Diem lien quan
    arrQueries[9] = `
        SELECT "DIA_DIEM"."TenDD", "TOURS"."MaTour", "TOURS"."AnhTour", 
            "TOURS"."TenTour", "TOURS"."ThoiGian", "TOURS"."GioiThieu", "TOURS"."GiaTour"
        FROM "TOURS" JOIN "CT_DIA_DIEM" ON "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
            JOIN "DIA_DIEM" ON "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
        WHERE "DIA_DIEM"."TenDD" = (
            SELECT "TenDD"
            FROM "CT_DIA_DIEM" JOIN "DIA_DIEM" ON "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
            WHERE "CT_DIA_DIEM"."MaTour" = '${req.query.MaTour}')
            LIMIT 3  
    `;

    //  Query lay Khuyen mai
    arrQueries[10] = `
        SELECT "MaKM", "TOURS"."MaTour", "TTKM", "Deal", "KM"
        FROM "TOURS" JOIN "CT_KHUYEN_MAI" ON "TOURS"."MaTour" = "CT_KHUYEN_MAI"."MaTour"
        WHERE "TOURS"."MaTour" = '${req.query.MaTour}'    
    `;

    const Data = [];
    const aJSON = {};

    handleQueries(arrQueries, Data, (result) => {
        Data.push(result.rows);

        if (Data.length === arrQueries.length) {
            aJSON.ChiTietTour = Data[0];
            aJSON.LichTrinhTour = Data[1];
            aJSON.DichVu = Data[2];
            aJSON.PhuongTien = Data[3];
            aJSON.DoiTac = Data[4];
            aJSON.DiaDiem = Data[5];
            aJSON.DieuKhoan = Data[6];
            aJSON.DanhSachDiaDiem = Data[7];
            aJSON.ToursDaXem = Data[8];
            aJSON.ToursLienQuan = Data[9];

            if (Data[10].length === 0) {
                aJSON.KhuyenMai.MaKM = `KM${req.body.MaTour}`;
                aJSON.KhuyenMai.MaTour = `${req.body.MaTour}`;
                aJSON.KhuyenMai.TTKM = null;
                aJSON.KhuyenMai.Deal = false;
                aJSON.KhuyenMai.KM = false;
            } else {
                aJSON.KhuyenMai = Data[10];
            }

            res.send(JSON.stringify(aJSON, null, 3));

            res.end();
        }
    });
});

// ALL TOURS follow with condition
router.get('/tours/where', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    let aQuery;
    if (req.query.KieuDD == null && req.query.TenDD == null && req.query.SapXep == null) {
        aQuery = `
            SELECT * FROM "TOURS"
        `;
    }
    if (req.query.KieuDD != null && req.query.TenDD == null && req.query.SapXep == null) {
        aQuery = `
            SELECT *
            FROM "TOURS", "DIA_DIEM", "CT_DIA_DIEM"
            WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                AND "DIA_DIEM"."KieuDD" = '${req.query.KieuDD}'
        `;
    }
    if (req.query.KieuDD != null && req.query.TenDD != null && req.query.SapXep == null) {
        aQuery = `
            SELECT *
            FROM "TOURS", "DIA_DIEM", "CT_DIA_DIEM"
            WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                AND "DIA_DIEM"."KieuDD" = '${req.query.KieuDD}'
                AND "DIA_DIEM"."TenDD" = '${req.query.TenDD}'
        `;
    }
    if (req.query.KieuDD != null && req.query.TenDD == null && req.query.SapXep != null) {
        if (req.query.SapXep === 'AtoZ') {
            aQuery = `
                SELECT *
                FROM "TOURS", "DIA_DIEM", "CT_DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    AND "DIA_DIEM"."KieuDD" = '${req.query.KieuDD}'
                    ORDER BY "TOURS"."TenTour" ASC
            `;
        }
        if (req.query.SapXep === 'ZtoA') {
            aQuery = `
                SELECT *
                FROM "TOURS", "DIA_DIEM", "CT_DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    AND "DIA_DIEM"."KieuDD" = '${req.query.KieuDD}'
                    ORDER BY "TOURS"."TenTour" DESC
            `;
        }
        if (req.query.SapXep === 'MoiNhat') {
            aQuery = `
                SELECT *
                FROM "TOURS", "DIA_DIEM", "CT_DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    AND "DIA_DIEM"."KieuDD" = '${req.query.KieuDD}'
                    ORDER BY "TOURS"."ThoiGianNhap" DESC
            `;
        }
        if (req.query.SapXep === 'CuNhat') {
            aQuery = `
                SELECT *
                FROM "TOURS", "DIA_DIEM", "CT_DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    AND "DIA_DIEM"."KieuDD" = '${req.query.KieuDD}'
                    ORDER BY "TOURS"."ThoiGianNhap" ASC
            `;
        }
    }
    if (req.query.KieuDD == null && req.query.TenDD == null && req.query.SapXep != null) {
        if (req.query.SapXep === 'AtoZ') {
            aQuery = `
                SELECT *
                FROM "TOURS", "DIA_DIEM", "CT_DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    ORDER BY "TOURS"."TenTour" ASC
            `;
        }
        if (req.query.SapXep === 'ZtoA') {
            aQuery = `
                SELECT *
                FROM "TOURS", "DIA_DIEM", "CT_DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    ORDER BY "TOURS"."TenTour" DESC
            `;
        }
        if (req.query.SapXep === 'MoiNhat') {
            aQuery = `
                SELECT *
                FROM "TOURS", "DIA_DIEM", "CT_DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    ORDER BY "TOURS"."ThoiGianNhap" DESC
            `;
        }
        if (req.query.SapXep === 'CuNhat') {
            aQuery = `
                SELECT *
                FROM "TOURS", "DIA_DIEM", "CT_DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    ORDER BY "TOURS"."ThoiGianNhap" ASC
            `;
        }
    }
    if (req.query.KieuDD != null && req.query.TenDD != null && req.query.SapXep != null) {
        if (req.query.SapXep === 'AtoZ') {
            aQuery = `
                SELECT *
                FROM "TOURS", "DIA_DIEM", "CT_DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    AND "DIA_DIEM"."KieuDD" = '${req.query.KieuDD}'
                    AND "DIA_DIEM"."TenDD" = '${req.query.TenDD}'
                    ORDER BY "TOURS"."TenTour" ASC
            `;
        }
        if (req.query.SapXep === 'ZtoA') {
            aQuery = `
                SELECT *
                FROM "TOURS", "DIA_DIEM", "CT_DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    AND "DIA_DIEM"."KieuDD" = '${req.query.KieuDD}'
                    AND "DIA_DIEM"."TenDD" = '${req.query.TenDD}'
                    ORDER BY "TOURS"."TenTour" DESC
            `;
        }
        if (req.query.SapXep === 'MoiNhat') {
            aQuery = `
                SELECT *
                FROM "TOURS", "DIA_DIEM", "CT_DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    AND "DIA_DIEM"."KieuDD" = '${req.query.KieuDD}'
                    AND "DIA_DIEM"."TenDD" = '${req.query.TenDD}'
                    ORDER BY "TOURS"."ThoiGianNhap" DESC
            `;
        }
        if (req.query.SapXep === 'CuNhat') {
            aQuery = `
                SELECT *
                FROM "TOURS", "DIA_DIEM", "CT_DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    AND "DIA_DIEM"."KieuDD" = '${req.query.KieuDD}'
                    AND "DIA_DIEM"."TenDD" = '${req.query.TenDD}'
                    ORDER BY "TOURS"."ThoiGianNhap" ASC
            `;
        }
    }
    handleAQuery(aQuery, (result) => {
        res.send(JSON.stringify(result.rows, null, 3));
        res.end();
    });
});


router.post('/tours/insert', urlencodedParser, cpUpload, (req, res) => {
    const Data = {
        TOURS: {
            MaTour: req.body.maTour,
            TenTour: req.body.tenTour,
            AnhTour: req.files.anhTourInput[0].originalname,
            ThoiGian: req.body.thoiGian,
            GioiThieu: req.body.gioiThieu,
            GiaTour: req.body.giaTour,
            MaDT: req.body.maDoiTac
        },
        DOI_TAC: {
            MaDT: req.body.maDoiTac,
            TenDT: req.body.tenDoiTac,
            DiaChi: req.body.diachiDoiTac
        },
        DIEU_KHOAN: {
            MaTour: req.body.maTour,
            MaDK: req.body.maDK,
            VanChuyen: req.body.vanChuyenDK,
            LuuTru: req.body.luuTruDK,
            Khac: req.body.khacDK
        },
        CT_DIA_DIEM: {
            MaTour: req.body.maTour,
            MaDD: req.body.diaDiem
        },
        CT_PHUONG_TIEN: {
            MaTour: req.body.maTour,
            MaPT: req.body.phuongTien
        },
        CT_DICH_VU: {
            MaTour: req.body.maTour,
            MaDV: req.body.dichVu
        },
        CT_LICH_TRINH_TOUR: {
            MaTour: req.body.maTour,
            Anh: req.files.anhLichTrinhInput,
            Ngay: req.body.ngayLichTrinh,
            NoiDung1: req.body.noiDung1,
            NoiDung2: req.body.noiDung2
        },
        CT_KHUYEN_MAI: {
            MaKM: `KM${req.body.maTour}`,
            MaTour: req.body.maTour,
            TTKM: '',
            Deal: false,
            KM: false
        }
    };
    const arrQueries = [];
    //TOURS
    arrQueries[0] = `
        INSERT INTO "TOURS" ("MaTour", "TenTour", "AnhTour", "ThoiGian", 
            "GioiThieu", "GiaTour", "MaDT")
        VALUES(
            '${Data.TOURS.MaTour}',
            '${Data.TOURS.TenTour}',
            '${Data.TOURS.AnhTour}',
            '${Data.TOURS.ThoiGian}',
            '${Data.TOURS.GioiThieu}',
            '${Data.TOURS.GiaTour}',
            '${Data.TOURS.MaDT}'
        )
    `;
    //DOI_TAC
    arrQueries[1] = `
        INSERT INTO "DOI_TAC" ("MaDT", "TenDT", "DiaChi")
        SELECT '${Data.DOI_TAC.MaDT}', '${Data.DOI_TAC.TenDT}', '${Data.DOI_TAC.DiaChi}'
        WHERE
            NOT EXISTS (
                SELECT "MaDT" FROM "DOI_TAC" WHERE "MaDT" = '${Data.DOI_TAC.MaDT}'
            )
    
    `;
    //DIEU_KHOAN
    arrQueries[2] = `
        INSERT INTO "DIEU_KHOAN" ("MaTour", "MaDK", "VanChuyen", "LuuTru", "Khac")
        VALUES (
            '${Data.DIEU_KHOAN.MaTour}',
            '${Data.DIEU_KHOAN.MaDK}',
            '${Data.DIEU_KHOAN.VanChuyen}',
            '${Data.DIEU_KHOAN.LuuTru}',
            '${Data.DIEU_KHOAN.Khac}'
        )
    `;
    //CT_DIA_DIEM
    arrQueries[3] = `
        INSERT INTO "CT_DIA_DIEM" ("MaTour", "MaDD")
        VALUES (
            '${Data.CT_DIA_DIEM.MaTour}',
            '${Data.CT_DIA_DIEM.MaDD}'
        )
    `;
    //CT_PHUONG_TIEN
    if (typeof (Data.CT_PHUONG_TIEN.MaPT) === 'string') {
        arrQueries[4] = `
            INSERT INTO "CT_PHUONG_TIEN" ("MaTour", "MaPT")
            VALUES ( '${Data.CT_PHUONG_TIEN.MaTour}', '${Data.CT_PHUONG_TIEN.MaPT}')`;
    } else {
        arrQueries[4] = `
            INSERT INTO "CT_PHUONG_TIEN" ("MaTour", "MaPT")
            VALUES ( '${Data.CT_PHUONG_TIEN.MaTour}', '${Data.CT_PHUONG_TIEN.MaPT[0]}')`;
        for (let i = 1; i < Data.CT_PHUONG_TIEN.MaPT.length; i++) {
            arrQueries[4] += `,('${Data.CT_PHUONG_TIEN.MaTour}', '${Data.CT_PHUONG_TIEN.MaPT[i]}')`;
        }
    }
    //CT_DICH_VU
    if (typeof (Data.CT_DICH_VU.MaDV) === 'string') {
        arrQueries[5] = `
            INSERT INTO "CT_DICH_VU" ("MaTour", "MaDV")
            VALUES ('${Data.CT_DICH_VU.MaTour}', '${Data.CT_DICH_VU.MaDV}') `;
    } else {
        arrQueries[5] = `
            INSERT INTO "CT_DICH_VU" ("MaTour", "MaDV")
            VALUES ('${Data.CT_DICH_VU.MaTour}', '${Data.CT_DICH_VU.MaDV[0]}') `;
        for (let i = 1; i < Data.CT_DICH_VU.MaDV.length; i++) {
            arrQueries[5] += `,('${Data.CT_DICH_VU.MaTour}', '${Data.CT_DICH_VU.MaDV[i]}')`;
        }
    }
    //CT_LICH_TRINH_TOUR
    if (typeof (Data.CT_LICH_TRINH_TOUR.Ngay) === 'string') {
        arrQueries[6] = `
            INSERT INTO "CT_LICH_TRINH_TOUR" ("MaTour", "Anh", "Ngay", "NoiDung1", "NoiDung2")
            VALUES ('${Data.CT_LICH_TRINH_TOUR.MaTour}', 
            '${Data.CT_LICH_TRINH_TOUR.Anh[0].originalname}', 
            '${Data.CT_LICH_TRINH_TOUR.Ngay}', 
            '${Data.CT_LICH_TRINH_TOUR.NoiDung1}', 
            '${Data.CT_LICH_TRINH_TOUR.NoiDung2}')`;
    } else {
        arrQueries[6] = `
            INSERT INTO "CT_LICH_TRINH_TOUR" ("MaTour", "Anh", "Ngay", "NoiDung1", "NoiDung2")
            VALUES ('${Data.CT_LICH_TRINH_TOUR.MaTour}', 
            '${Data.CT_LICH_TRINH_TOUR.Anh[0].originalname}', 
            '${Data.CT_LICH_TRINH_TOUR.Ngay[0]}', 
            '${Data.CT_LICH_TRINH_TOUR.NoiDung1[0]}', 
            '${Data.CT_LICH_TRINH_TOUR.NoiDung2[0]}')`;
        for (let i = 1; i < Data.CT_LICH_TRINH_TOUR.Ngay.length; i++) {
            arrQueries[6] += `,('${Data.CT_LICH_TRINH_TOUR.MaTour}', 
            '${Data.CT_LICH_TRINH_TOUR.Anh[i].originalname}', 
            '${Data.CT_LICH_TRINH_TOUR.Ngay[i]}', 
            '${Data.CT_LICH_TRINH_TOUR.NoiDung1[i]}', 
            '${Data.CT_LICH_TRINH_TOUR.NoiDung2[i]}')`;
        }
    }
    // CT_KHUYEN_MAI
    arrQueries[7] = `
        INSERT INTO "CT_KHUYEN_MAI" ("MaKM", "MaTour", "TTKM", "Deal", "KM")
        VALUES (
            '${Data.CT_KHUYEN_MAI.MaKM}',
            '${Data.CT_KHUYEN_MAI.MaTour}',
            '${Data.CT_KHUYEN_MAI.TTKM}',
            '${Data.CT_KHUYEN_MAI.Deal}',
            '${Data.CT_KHUYEN_MAI.KM}'
        )
    `;
    handleInsertTables(arrQueries, (result) => {
        // console.log(result);
        res.redirect('/management');
    });
});

router.get('/tours/deal/select', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const aQuery = `
        SELECT *
        FROM "TOURS", "CT_KHUYEN_MAI"
        WHERE "TOURS"."MaTour" = "CT_KHUYEN_MAI"."MaTour"
            AND "CT_KHUYEN_MAI"."Deal" = true
    `;
    handleAQuery(aQuery, (result) => {
        res.send(JSON.stringify(result.rows, null, 3));
        res.end();
    });
});

// LIST DIADIEM
router.get('/dia-diem/select', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const arrQueries = [];
    // Query lay all DiaDiem
    arrQueries[0] = `
        SELECT * FROM "DIA_DIEM"
    `;
    // Query lay DiaDiem trong nuoc
    arrQueries[1] = `
        SELECT *
        FROM "DIA_DIEM"
        WHERE "KieuDD" = 'Trong nước'    
    `;
    arrQueries[2] = `
        SELECT *
        FROM "DIA_DIEM"
        WHERE "KieuDD" = 'Ngoài nước'        
    `;
    const Data = [];
    const aJSON = {};
    handleQueries(arrQueries, Data, (result) => {
        Data.push(result.rows);
        if (Data.length === arrQueries.length) {
            aJSON.allDiaDiem = Data[0];
            aJSON.TrongNuoc = Data[1];
            aJSON.NgoaiNuoc = Data[2];

            res.send(JSON.stringify(aJSON, null, 3));
            res.end();
        }
    });
});

// DIADIEM theo KieuDD
router.get('/dia-diem/where', (req, res) => {
    res.setHeader('Content-Type', 'application/json');

    let KieuDD;

    if (req.query.KieuDD === 'TrongNuoc') {
        KieuDD = 'Trong nước';
    }
    if (req.query.KieuDD === 'NgoaiNuoc') {
        KieuDD = 'Ngoài nước';
    }
    const aQuery = `
        SELECT *
        FROM "DIA_DIEM"
        WHERE "KieuDD" = '${KieuDD}'      
    `;

    handleAQuery(aQuery, (Data) => {
        res.send(JSON.stringify(Data.rows, null, 3));
        res.end();
    });
});

// UPDATE TOUR

router.post('/tours/update', urlencodedParser, cpUpload, (req, res) => {
    if (req.body.LoaiForm === 'ChiTietTour') {
        const arrQueries = [];
        if (req.files.anhTourInput != null) {
            arrQueries.push(`
                UPDATE "TOURS"
                SET "GiaTour" = '${req.body.giaTour}',
                    "TenTour" = '${req.body.tenTour}',
                    "ThoiGian" = '${req.body.thoiGian}',
                    "GioiThieu" = '${req.body.gioiThieu}',
                    "AnhTour" = '${req.files.anhTourInput[0].originalname}'
                WHERE "MaTour" = '${req.body.maTour}'
            `);
            // Phuong Tien
            if (typeof (req.body.phuongTien) === 'string') {
                arrQueries.push(`
                    DELETE FROM "CT_PHUONG_TIEN" WHERE "MaTour" = '${req.body.maTour}';
                `);
                arrQueries.push(`
                    INSERT INTO "CT_PHUONG_TIEN" ("MaPT", "MaTour")
                    SELECT '${req.body.phuongTien}', '${req.body.maTour}'
                    WHERE
                        NOT EXISTS (
                            SELECT "MaPT", "MaTour" FROM "CT_PHUONG_TIEN" 
                            WHERE "MaTour" = '${req.body.maTour}' 
                                AND "MaPT" = '${req.body.phuongTien}'
                        )
                `);
            } else {
                arrQueries.push(`
                    DELETE FROM "CT_PHUONG_TIEN" WHERE "MaTour" = '${req.body.maTour}';
                `);
                for (let i = 0; i < req.body.phuongTien.length; i++) {
                    arrQueries.push(`
                        INSERT INTO "CT_PHUONG_TIEN" ("MaPT", "MaTour")
                        SELECT '${req.body.phuongTien[i]}', '${req.body.maTour}'
                        WHERE
                            NOT EXISTS (
                                SELECT "MaPT", "MaTour" FROM "CT_PHUONG_TIEN" 
                                WHERE "MaTour" = '${req.body.maTour}' 
                                    AND "MaPT" = '${req.body.phuongTien[i]}'
                            )
                    `);
                }
            }
            arrQueries.push(`
                UPDATE "CT_DIA_DIEM"
                SET "DiaDiem" = '${req.body.diaDiem}'
                WHERE "MaTour" = '${req.body.maTour}'
            `);
        } else {
            arrQueries.push(`
                UPDATE "TOURS"
                SET "GiaTour" = '${req.body.giaTour}',
                    "TenTour" = '${req.body.tenTour}',
                    "ThoiGian" = '${req.body.thoiGian}',
                    "GioiThieu" = '${req.body.gioiThieu}'
                WHERE "MaTour" = '${req.body.maTour}'
            `);
            // Phuong Tien
            if (typeof (req.body.phuongTien) === 'string') {
                arrQueries.push(`
                    DELETE FROM "CT_PHUONG_TIEN" WHERE "MaTour" = '${req.body.maTour}';
                `);
                arrQueries.push(`
                    INSERT INTO "CT_PHUONG_TIEN" ("MaPT", "MaTour")
                    SELECT '${req.body.phuongTien}', '${req.body.maTour}'
                    WHERE
                        NOT EXISTS (
                            SELECT "MaPT", "MaTour" FROM "CT_PHUONG_TIEN" 
                            WHERE "MaTour" = '${req.body.maTour}' 
                            AND "MaPT" = '${req.body.phuongTien}'
                        )
                `);
            } else {
                arrQueries.push(`
                    DELETE FROM "CT_PHUONG_TIEN" WHERE "MaTour" = '${req.body.maTour}';
                `);
                for (let i = 0; i < req.body.phuongTien.length; i++) {
                    arrQueries.push(`
                        INSERT INTO "CT_PHUONG_TIEN" ("MaPT", "MaTour")
                        SELECT '${req.body.phuongTien[i]}', '${req.body.maTour}'
                        WHERE
                            NOT EXISTS (
                                SELECT "MaPT", "MaTour" FROM "CT_PHUONG_TIEN" 
                                WHERE "MaTour" = '${req.body.maTour}' 
                                AND "MaPT" = '${req.body.phuongTien[i]}'
                            )
                    `);
                }
            }
            arrQueries.push(`
                UPDATE "CT_DIA_DIEM"
                SET "MaDD" = '${req.body.diaDiem}'
                WHERE "MaTour" = '${req.body.maTour}'
            `);
        }

        handleUpdateTables(arrQueries, (result) => {
            // console.log(result);
            res.redirect(`/management/tours/chi-tiet-tour?MaTour=${req.body.maTour}`);
        });
    }
    if (req.body.LoaiForm === 'LichTrinh') {
        const Data = {
            CT_LICH_TRINH_TOUR: {
                MaTour: req.body.maTour,
                // Anh : req.files.anhLichTrinhInput,
                AnhHide: req.body.anhLichTrinhInputHide,
                Ngay: req.body.ngayLichTrinh,
                NoiDung1: req.body.noiDung1,
                NoiDung2: req.body.noiDung2
            }
        };
        const arrQueries = [];

        if (req.files.anhLichTrinhInput != null) {
            arrQueries[0] = `
                DELETE FROM "CT_LICH_TRINH_TOUR" 
                WHERE "MaTour" = '${Data.CT_LICH_TRINH_TOUR.MaTour}'
            `;
            if (typeof (Data.CT_LICH_TRINH_TOUR.Ngay) === 'string') {
                arrQueries[1] = `
                    INSERT INTO "CT_LICH_TRINH_TOUR" ("MaTour", "Anh", "Ngay", 
                    "NoiDung1", "NoiDung2")
                    VALUES ('${Data.CT_LICH_TRINH_TOUR.MaTour}', 
                    '${Data.CT_LICH_TRINH_TOUR.AnhHide}', 
                    '${Data.CT_LICH_TRINH_TOUR.Ngay}', 
                    '${Data.CT_LICH_TRINH_TOUR.NoiDung1}', 
                    '${Data.CT_LICH_TRINH_TOUR.NoiDung2}')`;
            } else {
                arrQueries[1] = `
                    INSERT INTO "CT_LICH_TRINH_TOUR" ("MaTour", "Anh", "Ngay", 
                    "NoiDung1", "NoiDung2")
                    VALUES ('${Data.CT_LICH_TRINH_TOUR.MaTour}', 
                    '${Data.CT_LICH_TRINH_TOUR.AnhHide[0]}', 
                    '${Data.CT_LICH_TRINH_TOUR.Ngay[0]}', 
                    '${Data.CT_LICH_TRINH_TOUR.NoiDung1[0]}', 
                    '${Data.CT_LICH_TRINH_TOUR.NoiDung2[0]}')`;
                for (let i = 1; i < Data.CT_LICH_TRINH_TOUR.Ngay.length; i++) {
                    arrQueries[1] += `,('${Data.CT_LICH_TRINH_TOUR.MaTour}', 
                        '${Data.CT_LICH_TRINH_TOUR.AnhHide[i]}', 
                        '${Data.CT_LICH_TRINH_TOUR.Ngay[i]}', 
                        '${Data.CT_LICH_TRINH_TOUR.NoiDung1[i]}', 
                        '${Data.CT_LICH_TRINH_TOUR.NoiDung2[i]}')`;
                }
            }
        } else {
            arrQueries[0] = `
                DELETE FROM "CT_LICH_TRINH_TOUR" 
                WHERE "MaTour" = '${Data.CT_LICH_TRINH_TOUR.MaTour}'
            `;
            if (typeof (Data.CT_LICH_TRINH_TOUR.Ngay) === 'string') {
                arrQueries[1] = `
                    INSERT INTO "CT_LICH_TRINH_TOUR" ("MaTour", "Anh", "Ngay", 
                    "NoiDung1", "NoiDung2")
                    VALUES ('${Data.CT_LICH_TRINH_TOUR.MaTour}', 
                    '${Data.CT_LICH_TRINH_TOUR.AnhHide}', 
                    '${Data.CT_LICH_TRINH_TOUR.Ngay}', 
                    '${Data.CT_LICH_TRINH_TOUR.NoiDung1}', 
                    '${Data.CT_LICH_TRINH_TOUR.NoiDung2}')`;
            } else {
                arrQueries[1] = `
                    INSERT INTO "CT_LICH_TRINH_TOUR" ("MaTour", "Anh", "Ngay", 
                    "NoiDung1", "NoiDung2")
                    VALUES ('${Data.CT_LICH_TRINH_TOUR.MaTour}', 
                    '${Data.CT_LICH_TRINH_TOUR.AnhHide[0]}', 
                    '${Data.CT_LICH_TRINH_TOUR.Ngay[0]}', 
                    '${Data.CT_LICH_TRINH_TOUR.NoiDung1[0]}', 
                    '${Data.CT_LICH_TRINH_TOUR.NoiDung2[0]}')`;
                for (let i = 1; i < Data.CT_LICH_TRINH_TOUR.Ngay.length; i++) {
                    arrQueries[1] += `,('${Data.CT_LICH_TRINH_TOUR.MaTour}', 
                        '${Data.CT_LICH_TRINH_TOUR.AnhHide[i]}', 
                        '${Data.CT_LICH_TRINH_TOUR.Ngay[i]}', 
                        '${Data.CT_LICH_TRINH_TOUR.NoiDung1[i]}', 
                        '${Data.CT_LICH_TRINH_TOUR.NoiDung2[i]}')`;
                }
            }
        }
        // console.log(Data);
        handleInsertTables(arrQueries, (result) => {
            // console.log(result);
            res.redirect(`/management/tours/chi-tiet-tour?MaTour=${req.body.maTour}`);
        });
    }
    if (req.body.LoaiForm === 'DichVu') {
        const Data = {
            CT_DICH_VU: {
                MaTour: req.body.maTour,
                MaDV: req.body.dichVu
            }
        };
        const arrQueries = [];

        if (typeof (Data.CT_DICH_VU.MaDV) === 'string') {
            arrQueries[0] = `
                DELETE FROM "CT_DICH_VU" WHERE "MaTour" = '${Data.CT_DICH_VU.MaTour}'
            `;
            arrQueries[1] = `
                INSERT INTO "CT_DICH_VU" ("MaTour", "MaDV")
                VALUES ('${Data.CT_DICH_VU.MaTour}', '${Data.CT_DICH_VU.MaDV}') `;
        } else {
            arrQueries[0] = `
                DELETE FROM "CT_DICH_VU" WHERE "MaTour" = '${Data.CT_DICH_VU.MaTour}'
            `;
            arrQueries[1] = `
                INSERT INTO "CT_DICH_VU" ("MaTour", "MaDV")
                VALUES ('${Data.CT_DICH_VU.MaTour}', '${Data.CT_DICH_VU.MaDV[0]}') `;
            for (let i = 1; i < Data.CT_DICH_VU.MaDV.length; i++) {
                arrQueries[1] += `,('${Data.CT_DICH_VU.MaTour}', '${Data.CT_DICH_VU.MaDV[i]}')`;
            }
        }
        handleInsertTables(arrQueries, (result) => {
            // console.log(result);
            res.redirect(`/management/tours/chi-tiet-tour?MaTour=${req.body.maTour}`);
        });
    }
    if (req.body.LoaiForm === 'DoiTac') {
        const Data = {
            DOI_TAC: {
                MaDT: req.body.maDoiTac,
                TenDT: req.body.tenDoiTac,
                DiaChi: req.body.diachiDoiTac
            }
        };
        const aQuery = `
            UPDATE "DOI_TAC"
            SET "TenDT" = '${Data.DOI_TAC.TenDT}',
                "DiaChi" = '${Data.DOI_TAC.DiaChi}'
            WHERE "MaDT" = '${Data.DOI_TAC.MaDT}'
        `;
        handleAQuery(aQuery, (result) => {
            // console.log(result);
            res.redirect(`/management/tours/chi-tiet-tour?MaTour=${req.body.maTour}`);
        });
    }
    if (req.body.LoaiForm === 'DieuKhoan') {
        const Data = {
            DIEU_KHOAN: {
                MaTour: req.body.maTour,
                MaDK: req.body.maDK,
                VanChuyen: req.body.vanChuyenDK,
                LuuTru: req.body.luuTruDK,
                Khac: req.body.khacDK
            }
        };
        const aQuery = `
            UPDATE "DIEU_KHOAN"
            SET "VanChuyen" = '${Data.DIEU_KHOAN.VanChuyen}',
                "LuuTru" = '${Data.DIEU_KHOAN.LuuTru}',
                "Khac" = '${Data.DIEU_KHOAN.Khac}'
            WHERE "MaTour" = '${Data.DIEU_KHOAN.MaTour}' AND "MaDK" = '${Data.DIEU_KHOAN.MaDK}'
        `;
        handleAQuery(aQuery, (result) => {
            // console.log(result);
            res.redirect(`/management/tours/chi-tiet-tour?MaTour=${req.body.maTour}`);
        });
    }
    if (req.body.LoaiForm === 'KhuyenMai') {
        const Data = {
            CT_KHUYEN_MAI: {
                MaKM: req.body.maKM,
                MaTour: req.body.maTour,
                TTKM: req.body.thongtinKM,
                Deal: req.body.deal,
                KM: req.body.km
            }
        };
        const aQuery = `
            UPDATE "CT_KHUYEN_MAI"
            SET "TTKM" = '${Data.CT_KHUYEN_MAI.TTKM}',
                "Deal" = '${Data.CT_KHUYEN_MAI.Deal}',
                "KM" = '${Data.CT_KHUYEN_MAI.KM}'
            WHERE "MaKM" = '${Data.CT_KHUYEN_MAI.MaKM}'
        `;
        handleAQuery(aQuery, (result) => {
            // console.log(result);
            res.redirect(`/management/tours/chi-tiet-tour?MaTour=${req.body.maTour}`);
        });
    }
});

// DELETE TOUR
router.post('/tours/delete', urlencodedParser, (req, res) => {
    const arrQueries = [];
    arrQueries[0] = `
        DELETE FROM "TOURS"
        WHERE "MaTour" = '${req.body.MaTour}'
    `;
    arrQueries[1] = `
        DELETE FROM "DIEU_KHOAN"
        WHERE "MaTour" = '${req.body.MaTour}'
    `;
    arrQueries[2] = `
        DELETE FROM "CT_DIA_DIEM"
        WHERE "MaTour" = '${req.body.MaTour}'
    `;
    arrQueries[3] = `
        DELETE FROM "CT_PHUONG_TIEN"
        WHERE "MaTour" = '${req.body.MaTour}'
    `;
    arrQueries[4] = `
        DELETE FROM "CT_DICH_VU"
        WHERE "MaTour" = '${req.body.MaTour}'
    `;
    arrQueries[5] = `
        DELETE FROM "CT_KHUYEN_MAI"
        WHERE "MaTour" = '${req.body.MaTour}'
    `;
    arrQueries[6] = `
        DELETE FROM "CT_LICH_TRINH_TOUR"
        WHERE "MaTour" = '${req.body.MaTour}'
    `;
    handleDeleteTables(arrQueries, (result) => {
        // console.log(result);
        res.end('XOA THANH CONG !');
    });
});


//  API TRANGCHU
router.get('/trangchu/select', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const arrQueries = [];
    // DEAL GIA
    arrQueries[0] = `
        SELECT *
        FROM "TOURS", "CT_KHUYEN_MAI", "CT_DIA_DIEM", "DIA_DIEM"
        WHERE "TOURS"."MaTour" = "CT_KHUYEN_MAI"."MaTour"
            AND "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
            AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
            AND "CT_KHUYEN_MAI"."Deal" = true
            LIMIT 6
    `;
    //  TOUR KHUYEN MAI
    arrQueries[1] = `
        SELECT *
        FROM "TOURS", "CT_KHUYEN_MAI", "CT_DIA_DIEM", "DIA_DIEM"
        WHERE "TOURS"."MaTour" = "CT_KHUYEN_MAI"."MaTour"
            AND "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
            AND "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
            AND "CT_KHUYEN_MAI"."KM" = true
        LIMIT 6
    `;
    //  TOUR NGOAI NUOC
    arrQueries[2] = `
        SELECT *
        FROM "TOURS", "CT_DIA_DIEM", "DIA_DIEM"
        WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
            AND "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
            AND "DIA_DIEM"."KieuDD" = 'Ngoài nước'
        LIMIT 6
    `;
    //  TOUR TRONG NUOC
    arrQueries[3] = `
        SELECT *
        FROM "TOURS", "CT_DIA_DIEM", "DIA_DIEM"
        WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
            AND "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
            AND "DIA_DIEM"."KieuDD" = 'Trong nước'
        LIMIT 6
    `;
    // TOUR MOI
    arrQueries[4] = `
        SELECT *
        FROM "TOURS", "CT_DIA_DIEM", "DIA_DIEM"
        WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
            AND "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
            ORDER BY "ThoiGianNhap" DESC
        LIMIT 6
    `;
    //  DIEM DU LICH PHO BIEN
    arrQueries[5] = `
        SELECT 
            COUNT("CT_DIA_DIEM"."MaDD") AS myCount,
            "DIA_DIEM"."TenDD",
            "DIA_DIEM"."KieuDD",
            "DIA_DIEM"."AnhDD",
            "DIA_DIEM"."MaDD"
        FROM 
            "CT_DIA_DIEM" 
            LEFT JOIN "DIA_DIEM" ON 
                "CT_DIA_DIEM"."MaDD"="DIA_DIEM"."MaDD" 
        GROUP BY 
            "DIA_DIEM"."TenDD",
            "DIA_DIEM"."KieuDD",
            "DIA_DIEM"."AnhDD",
            "DIA_DIEM"."MaDD"
        ORDER BY myCount DESC
        LIMIT 4
    `;
    // TOURS DA XEM
    arrQueries[6] = `
        SELECT *
        FROM "TOURS_DA_XEM"
        	JOIN "TOURS" ON "TOURS_DA_XEM"."MaTour" = "TOURS"."MaTour"
        WHERE "IP" = '${req.sessionID}'
        ORDER BY "TOURS_DA_XEM"."ThoiGian" DESC
        LIMIT 6  
    `;
    const Data = [];
    const aJSON = {};
    handleQueries(arrQueries, Data, (result) => {
        Data.push(result.rows);
        if (Data.length === arrQueries.length) {
            aJSON.Deal = Data[0];
            aJSON.KhuyenMai = Data[1];
            aJSON.NgoaiNuoc = Data[2];
            aJSON.TrongNuoc = Data[3];
            aJSON.TourMoi = Data[4];
            aJSON.DiemPhoBien = Data[5];
            aJSON.ToursDaXem = Data[6];
            res.send(JSON.stringify(aJSON, null, 3));
            res.end();
        }
    });
});

router.get('/xem-them/where', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const arrQueries = [];
    if (req.query.Go === 'Deal') {
        if (req.query.DeXuat == null && req.query.Gia == null && req.query.ThoiLuongTour == null) {
            arrQueries[0] = `
                SELECT * 
                FROM "TOURS", "CT_KHUYEN_MAI"
                WHERE "TOURS"."MaTour" = "CT_KHUYEN_MAI"."MaTour"
                    AND "CT_KHUYEN_MAI"."Deal" = true        
            `;
        } else if (req.query.DeXuat != null
            && req.query.Gia == null
            && req.query.ThoiLuongTour == null) {
            arrQueries[0] = `
                SELECT 
                    COUNT("CT_DAT_TOUR"."MaTour") AS myCount,
                    "TOURS"."MaTour",
                    "TOURS"."TenTour",
                    "TOURS"."ThoiGian",
                    "TOURS"."GiaTour",
                    "TOURS"."AnhTour",
                    "CT_KHUYEN_MAI"."Deal"
                FROM 
                    "TOURS" 
                    LEFT JOIN "CT_DAT_TOUR" ON 
                        "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                    LEFT JOIN "CT_KHUYEN_MAI" ON
                        "TOURS"."MaTour" = "CT_KHUYEN_MAI"."MaTour"
                WHERE "CT_KHUYEN_MAI"."Deal" = true
                GROUP BY 
                    "TOURS"."MaTour",
                    "TOURS"."TenTour",
                    "TOURS"."ThoiGian",
                    "TOURS"."GiaTour",
                    "TOURS"."AnhTour",
                    "CT_KHUYEN_MAI"."Deal"
                ORDER BY myCount DESC        
            `;
        } else if (req.query.DeXuat == null
            && req.query.Gia != null
            && req.query.ThoiLuongTour == null) {
            if (req.query.Gia === 'Thap') {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour",
                        "CT_KHUYEN_MAI"."Deal"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_KHUYEN_MAI" ON
                            "TOURS"."MaTour" = "CT_KHUYEN_MAI"."MaTour"
                    WHERE "CT_KHUYEN_MAI"."Deal" = true
                    ORDER BY "TOURS"."GiaTour" ASC            
                `;
            } else {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour",
                        "CT_KHUYEN_MAI"."Deal"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_KHUYEN_MAI" ON
                            "TOURS"."MaTour" = "CT_KHUYEN_MAI"."MaTour"
                    WHERE "CT_KHUYEN_MAI"."Deal" = true
                    ORDER BY "TOURS"."GiaTour" DESC               
                `;
            }
        } else if (req.query.DeXuat == null
            && req.query.Gia == null && req.query.ThoiLuongTour != null) {
            if (req.query.ThoiLuongTour === 'Thap') {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour",
                        "CT_KHUYEN_MAI"."Deal"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_KHUYEN_MAI" ON
                            "TOURS"."MaTour" = "CT_KHUYEN_MAI"."MaTour"
                    WHERE "CT_KHUYEN_MAI"."Deal" = true
                    ORDER BY "TOURS"."ThoiGian" ASC            
                `;
            } else {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour",
                        "CT_KHUYEN_MAI"."Deal"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_KHUYEN_MAI" ON
                            "TOURS"."MaTour" = "CT_KHUYEN_MAI"."MaTour"
                    WHERE "CT_KHUYEN_MAI"."Deal" = true
                    ORDER BY "TOURS"."ThoiGian" DESC            
                `;
            }
        }
        arrQueries[1] = `
            SELECT 
                COUNT("CT_DIA_DIEM"."MaDD") AS myCount,
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            FROM 
                "CT_DIA_DIEM" 
                LEFT JOIN "DIA_DIEM" ON 
                    "CT_DIA_DIEM"."MaDD"="DIA_DIEM"."MaDD" 
                WHERE "DIA_DIEM"."KieuDD" = 'Trong nước'
            GROUP BY 
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            ORDER BY myCount DESC
            LIMIT 6        
        `;
        arrQueries[2] = `
            SELECT 
                COUNT("CT_DIA_DIEM"."MaDD") AS myCount,
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            FROM 
                "CT_DIA_DIEM" 
                LEFT JOIN "DIA_DIEM" ON 
                    "CT_DIA_DIEM"."MaDD"="DIA_DIEM"."MaDD" 
                WHERE "DIA_DIEM"."KieuDD" = 'Ngoài nước'
            GROUP BY 
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            ORDER BY myCount DESC
            LIMIT 6          
        `;
        arrQueries[3] = `
            SELECT *
            FROM "DIA_DIEM"
            WHERE "KieuDD" = 'Trong nước'        
        `;
        arrQueries[4] = `
            SELECT *
            FROM "DIA_DIEM"
            WHERE "KieuDD" = 'Ngoài nước'        
        `;
        const Data = [];
        const aJSON = {};
        handleQueries(arrQueries, Data, (result) => {
            aJSON.Go = 'ALA\'s DEAL';
            Data.push(result.rows);
            if (Data.length === arrQueries.length) {
                aJSON.XemThem = Data[0];
                aJSON.HotTrongNuoc = Data[1];
                aJSON.HotNgoaiNuoc = Data[2];
                aJSON.TrongNuoc = Data[3];
                aJSON.NgoaiNuoc = Data[4];
                res.send(JSON.stringify(aJSON, null, 3));
                res.end();
            }
        });
    }
    if (req.query.Go === 'TrongNuoc') {
        if (req.query.DeXuat == null && req.query.Gia == null && req.query.ThoiLuongTour == null) {
            arrQueries[0] = `
                SELECT * 
                FROM "TOURS", "CT_DIA_DIEM", "DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    AND "DIA_DIEM"."KieuDD" = 'Trong nước'        
            `;
        } else if (req.query.DeXuat != null
            && req.query.Gia == null
            && req.query.ThoiLuongTour == null) {
            arrQueries[0] = `
                SELECT 
                    COUNT("CT_DAT_TOUR"."MaTour") AS myCount,
                    "TOURS"."MaTour",
                    "TOURS"."TenTour",
                    "TOURS"."ThoiGian",
                    "TOURS"."GiaTour",
                    "TOURS"."AnhTour"
                FROM 
                    "TOURS" 
                    LEFT JOIN "CT_DAT_TOUR" ON 
                        "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                    LEFT JOIN "CT_DIA_DIEM" ON
                        "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    LEFT JOIN "DIA_DIEM" ON
                        "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                WHERE "DIA_DIEM"."KieuDD" = 'Trong nước'
                GROUP BY 
                    "TOURS"."MaTour",
                    "TOURS"."TenTour",
                    "TOURS"."ThoiGian",
                    "TOURS"."GiaTour",
                    "TOURS"."AnhTour"
                ORDER BY myCount DESC        
            `;
        } else if (req.query.DeXuat == null
            && req.query.Gia != null
            && req.query.ThoiLuongTour == null) {
            if (req.query.Gia === 'Thap') {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    WHERE "DIA_DIEM"."KieuDD" = 'Trong nước'
                    ORDER BY "TOURS"."GiaTour" ASC            
                `;
            } else {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    WHERE "DIA_DIEM"."KieuDD" = 'Trong nước'
                    ORDER BY "TOURS"."GiaTour" DESC              
                `;
            }
        } else if (req.query.DeXuat == null
            && req.query.Gia == null
            && req.query.ThoiLuongTour != null) {
            if (req.query.ThoiLuongTour === 'Thap') {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                    WHERE "DIA_DIEM"."KieuDD" = 'Trong nước'
                    ORDER BY "TOURS"."ThoiGian" ASC             
                `;
            } else {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                    WHERE "DIA_DIEM"."KieuDD" = 'Trong nước'
                    ORDER BY "TOURS"."ThoiGian" DESC             
                `;
            }
        }
        arrQueries[1] = `
            SELECT 
                COUNT("CT_DIA_DIEM"."MaDD") AS myCount,
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            FROM 
                "CT_DIA_DIEM" 
                LEFT JOIN "DIA_DIEM" ON 
                    "CT_DIA_DIEM"."MaDD"="DIA_DIEM"."MaDD" 
                WHERE "DIA_DIEM"."KieuDD" = 'Trong nước'
            GROUP BY 
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            ORDER BY myCount DESC
            LIMIT 6        
        `;
        arrQueries[2] = `
            SELECT 
                COUNT("CT_DIA_DIEM"."MaDD") AS myCount,
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            FROM 
                "CT_DIA_DIEM" 
                LEFT JOIN "DIA_DIEM" ON 
                    "CT_DIA_DIEM"."MaDD"="DIA_DIEM"."MaDD" 
                WHERE "DIA_DIEM"."KieuDD" = 'Ngoài nước'
            GROUP BY 
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            ORDER BY myCount DESC
            LIMIT 6          
        `;
        arrQueries[3] = `
            SELECT *
            FROM "DIA_DIEM"
            WHERE "KieuDD" = 'Trong nước'        
        `;
        arrQueries[4] = `
            SELECT *
            FROM "DIA_DIEM"
            WHERE "KieuDD" = 'Ngoài nước'        
        `;
        const Data = [];
        const aJSON = {};
        handleQueries(arrQueries, Data, (result) => {
            aJSON.Go = 'Tours du lịch trong nước';
            Data.push(result.rows);
            if (Data.length === arrQueries.length) {
                aJSON.XemThem = Data[0];
                aJSON.HotTrongNuoc = Data[1];
                aJSON.HotNgoaiNuoc = Data[2];
                aJSON.TrongNuoc = Data[3];
                aJSON.NgoaiNuoc = Data[4];
                res.send(JSON.stringify(aJSON, null, 3));
                res.end();
            }
        });
    }
    if (req.query.Go === 'NgoaiNuoc') {
        if (req.query.DeXuat == null && req.query.Gia == null && req.query.ThoiLuongTour == null) {
            arrQueries[0] = `
                SELECT * 
                FROM "TOURS", "CT_DIA_DIEM", "DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    AND "DIA_DIEM"."KieuDD" = 'Ngoài nước'        
            `;
        } else if (req.query.DeXuat != null
            && req.query.Gia == null
            && req.query.ThoiLuongTour == null) {
            arrQueries[0] = `
                SELECT 
                    COUNT("CT_DAT_TOUR"."MaTour") AS myCount,
                    "TOURS"."MaTour",
                    "TOURS"."TenTour",
                    "TOURS"."ThoiGian",
                    "TOURS"."GiaTour",
                    "TOURS"."AnhTour"
                FROM 
                    "TOURS" 
                    LEFT JOIN "CT_DAT_TOUR" ON 
                        "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                    LEFT JOIN "CT_DIA_DIEM" ON
                        "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    LEFT JOIN "DIA_DIEM" ON
                        "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                WHERE "DIA_DIEM"."KieuDD" = 'Ngoài nước'
                GROUP BY 
                    "TOURS"."MaTour",
                    "TOURS"."TenTour",
                    "TOURS"."ThoiGian",
                    "TOURS"."GiaTour",
                    "TOURS"."AnhTour"
                ORDER BY myCount DESC        
            `;
        } else if (req.query.DeXuat == null
            && req.query.Gia != null
            && req.query.ThoiLuongTour == null) {
            if (req.query.Gia === 'Thap') {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    WHERE "DIA_DIEM"."KieuDD" = 'Ngoài nước'
                    ORDER BY "TOURS"."GiaTour" ASC            
                `;
            } else {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD"
                    WHERE "DIA_DIEM"."KieuDD" = 'Ngoài nước'
                    ORDER BY "TOURS"."GiaTour" DESC              
                `;
            }
        } else if (req.query.DeXuat == null
            && req.query.Gia == null
            && req.query.ThoiLuongTour != null) {
            if (req.query.ThoiLuongTour === 'Thap') {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                    WHERE "DIA_DIEM"."KieuDD" = 'Ngoài nước'
                    ORDER BY "TOURS"."ThoiGian" ASC             
                `;
            } else {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                    WHERE "DIA_DIEM"."KieuDD" = 'Ngoài nước'
                    ORDER BY "TOURS"."ThoiGian" DESC             
                `;
            }
        }
        arrQueries[1] = `
            SELECT 
                COUNT("CT_DIA_DIEM"."MaDD") AS myCount,
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            FROM 
                "CT_DIA_DIEM" 
                LEFT JOIN "DIA_DIEM" ON 
                    "CT_DIA_DIEM"."MaDD"="DIA_DIEM"."MaDD" 
                WHERE "DIA_DIEM"."KieuDD" = 'Trong nước'
            GROUP BY 
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            ORDER BY myCount DESC
            LIMIT 6        
        `;
        arrQueries[2] = `
            SELECT 
                COUNT("CT_DIA_DIEM"."MaDD") AS myCount,
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            FROM 
                "CT_DIA_DIEM" 
                LEFT JOIN "DIA_DIEM" ON 
                    "CT_DIA_DIEM"."MaDD"="DIA_DIEM"."MaDD" 
                WHERE "DIA_DIEM"."KieuDD" = 'Ngoài nước'
            GROUP BY 
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            ORDER BY myCount DESC
            LIMIT 6          
        `;
        arrQueries[3] = `
            SELECT *
            FROM "DIA_DIEM"
            WHERE "KieuDD" = 'Trong nước'        
        `;
        arrQueries[4] = `
            SELECT *
            FROM "DIA_DIEM"
            WHERE "KieuDD" = 'Ngoài nước'        
        `;
        const Data = [];
        const aJSON = {};
        handleQueries(arrQueries, Data, (result) => {
            aJSON.Go = 'Tours du lịch ngoài nước';
            Data.push(result.rows);
            if (Data.length === arrQueries.length) {
                aJSON.XemThem = Data[0];
                aJSON.HotTrongNuoc = Data[1];
                aJSON.HotNgoaiNuoc = Data[2];
                aJSON.TrongNuoc = Data[3];
                aJSON.NgoaiNuoc = Data[4];
                res.send(JSON.stringify(aJSON, null, 3));
                res.end();
            }
        });
    }
    if (req.query.Go === 'TourMoi') {
        if (req.query.DeXuat == null
            && req.query.Gia == null
            && req.query.ThoiLuongTour == null) {
            arrQueries[0] = `
                SELECT * 
                FROM "TOURS", "CT_DIA_DIEM", "DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD" 
                    ORDER BY "TOURS"."ThoiGianNhap" DESC  
            `;
        } else if (req.query.DeXuat != null
            && req.query.Gia == null
            && req.query.ThoiLuongTour == null) {
            arrQueries[0] = `
                SELECT 
                    COUNT("CT_DAT_TOUR"."MaTour") AS myCount,
                    "TOURS"."MaTour",
                    "TOURS"."TenTour",
                    "TOURS"."ThoiGian",
                    "TOURS"."GiaTour",
                    "TOURS"."AnhTour"
                FROM 
                    "TOURS" 
                    LEFT JOIN "CT_DAT_TOUR" ON 
                        "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                    LEFT JOIN "CT_DIA_DIEM" ON
                        "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    LEFT JOIN "DIA_DIEM" ON
                        "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                WHERE 
                "TOURS"."MaTour" IN (   SELECT 
                                            "TOURS"."MaTour"
                                        FROM 
                                            "TOURS" 
                                            LEFT JOIN "CT_DAT_TOUR" ON 
                                                "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                                            LEFT JOIN "CT_DIA_DIEM" ON
                                                "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                                            LEFT JOIN "DIA_DIEM" ON
                                                "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"   
                                            ORDER BY "TOURS"."ThoiGianNhap" DESC )
                GROUP BY 
                    "TOURS"."MaTour",
                    "TOURS"."TenTour",
                    "TOURS"."ThoiGian",
                    "TOURS"."GiaTour",
                    "TOURS"."AnhTour"
                ORDER BY myCount DESC     
            `;
        } else if (req.query.DeXuat == null
            && req.query.Gia != null
            && req.query.ThoiLuongTour == null) {
            if (req.query.Gia === 'Thap') {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DAT_TOUR" ON 
                            "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                    WHERE 
                    "TOURS"."MaTour" IN (   SELECT 
                                                "TOURS"."MaTour"
                                            FROM 
                                                "TOURS" 
                                                LEFT JOIN "CT_DAT_TOUR" ON 
                                                    "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                                                LEFT JOIN "CT_DIA_DIEM" ON
                                                    "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                                                LEFT JOIN "DIA_DIEM" ON
                                                    "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"   
                                                ORDER BY "TOURS"."ThoiGianNhap" DESC )
                    ORDER BY "TOURS"."GiaTour" ASC       
                `;
            } else {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DAT_TOUR" ON 
                            "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                    WHERE 
                    "TOURS"."MaTour" IN (   SELECT 
                                                "TOURS"."MaTour"
                                            FROM 
                                                "TOURS" 
                                                LEFT JOIN "CT_DAT_TOUR" ON 
                                                    "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                                                LEFT JOIN "CT_DIA_DIEM" ON
                                                    "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                                                LEFT JOIN "DIA_DIEM" ON
                                                    "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"   
                                                ORDER BY "TOURS"."ThoiGianNhap" DESC )
                    ORDER BY "TOURS"."GiaTour" DESC           
                `;
            }
        } else if (req.query.DeXuat == null 
            && req.query.Gia == null 
            && req.query.ThoiLuongTour != null) {
            if (req.query.ThoiLuongTour === 'Thap') {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DAT_TOUR" ON 
                            "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                    WHERE 
                    "TOURS"."MaTour" IN (   SELECT 
                                                "TOURS"."MaTour"
                                            FROM 
                                                "TOURS" 
                                                LEFT JOIN "CT_DAT_TOUR" ON 
                                                    "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                                                LEFT JOIN "CT_DIA_DIEM" ON
                                                    "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                                                LEFT JOIN "DIA_DIEM" ON
                                                    "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"   
                                                ORDER BY "TOURS"."ThoiGianNhap" DESC )
                        ORDER BY "TOURS"."ThoiGian" DESC         
                `;
            } else {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DAT_TOUR" ON 
                            "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                    WHERE 
                    "TOURS"."MaTour" IN (   SELECT 
                                                "TOURS"."MaTour"
                                            FROM 
                                                "TOURS" 
                                                LEFT JOIN "CT_DAT_TOUR" ON 
                                                    "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                                                LEFT JOIN "CT_DIA_DIEM" ON
                                                    "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                                                LEFT JOIN "DIA_DIEM" ON
                                                    "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"   
                                                ORDER BY "TOURS"."ThoiGianNhap" DESC )
                    ORDER BY "TOURS"."ThoiGian" ASC           
                `;
            }
        }
        arrQueries[1] = `
            SELECT 
                COUNT("CT_DIA_DIEM"."MaDD") AS myCount,
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            FROM 
                "CT_DIA_DIEM" 
                LEFT JOIN "DIA_DIEM" ON 
                    "CT_DIA_DIEM"."MaDD"="DIA_DIEM"."MaDD" 
                WHERE "DIA_DIEM"."KieuDD" = 'Trong nước'
            GROUP BY 
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            ORDER BY myCount DESC
            LIMIT 6        
        `;
        arrQueries[2] = `
            SELECT 
                COUNT("CT_DIA_DIEM"."MaDD") AS myCount,
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            FROM 
                "CT_DIA_DIEM" 
                LEFT JOIN "DIA_DIEM" ON 
                    "CT_DIA_DIEM"."MaDD"="DIA_DIEM"."MaDD" 
                WHERE "DIA_DIEM"."KieuDD" = 'Ngoài nước'
            GROUP BY 
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            ORDER BY myCount DESC
            LIMIT 6          
        `;
        arrQueries[3] = `
            SELECT *
            FROM "DIA_DIEM"
            WHERE "KieuDD" = 'Trong nước'        
        `;
        arrQueries[4] = `
            SELECT *
            FROM "DIA_DIEM"
            WHERE "KieuDD" = 'Ngoài nước'        
        `;
        const Data = [];
        const aJSON = {};
        handleQueries(arrQueries, Data, (result) => {
            aJSON.Go = 'Tours mới tại ALA\'s';
            Data.push(result.rows);
            if (Data.length === arrQueries.length) {
                aJSON.XemThem = Data[0];
                aJSON.HotTrongNuoc = Data[1];
                aJSON.HotNgoaiNuoc = Data[2];
                aJSON.TrongNuoc = Data[3];
                aJSON.NgoaiNuoc = Data[4];
                res.send(JSON.stringify(aJSON, null, 3));
                res.end();
            }
        });
    }
    if (req.query.Go !== 'Deal' && req.query.Go !== 'TourMoi' 
        && req.query.Go !== 'TrongNuoc' && req.query.Go !== 'NgoaiNuoc') {
        if (req.query.DeXuat == null && req.query.Gia == null && req.query.ThoiLuongTour == null) {
            arrQueries[0] = `
                SELECT * 
                FROM "TOURS", "CT_DIA_DIEM", "DIA_DIEM"
                WHERE "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    AND "DIA_DIEM"."MaDD" = "CT_DIA_DIEM"."MaDD" 
                    AND LOWER("CT_DIA_DIEM"."MaDD") = LOWER('${req.query.Go}')
                    OR LOWER("DIA_DIEM"."TenDD") LIKE LOWER('%${req.query.Go}%')
            `;
        } else if (req.query.DeXuat != null && req.query.Gia == null 
            && req.query.ThoiLuongTour == null) {
            arrQueries[0] = `
                SELECT 
                    COUNT("CT_DAT_TOUR"."MaTour") AS myCount,
                    "TOURS"."MaTour",
                    "TOURS"."TenTour",
                    "TOURS"."ThoiGian",
                    "TOURS"."GiaTour",
                    "TOURS"."AnhTour"
                FROM 
                    "TOURS" 
                    LEFT JOIN "CT_DAT_TOUR" ON 
                        "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                    LEFT JOIN "CT_DIA_DIEM" ON
                        "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                    LEFT JOIN "DIA_DIEM" ON
                        "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                WHERE LOWER("CT_DIA_DIEM"."MaDD") = LOWER('${req.query.Go}')
                    OR LOWER("DIA_DIEM"."TenDD") LIKE LOWER('%${req.query.Go}%')
                GROUP BY 
                    "TOURS"."MaTour",
                    "TOURS"."TenTour",
                    "TOURS"."ThoiGian",
                    "TOURS"."GiaTour",
                    "TOURS"."AnhTour"
                ORDER BY myCount DESC     
            `;
        } else if (req.query.DeXuat == null && req.query.Gia != null 
            && req.query.ThoiLuongTour == null) {
            if (req.query.Gia === 'Thap') {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DAT_TOUR" ON 
                            "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                    WHERE 
                        LOWER("CT_DIA_DIEM"."MaDD") = LOWER('${req.query.Go}')
                        OR LOWER("DIA_DIEM"."TenDD") LIKE LOWER('%${req.query.Go}%')
                    ORDER BY "TOURS"."GiaTour" ASC    
                `;
            } else {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DAT_TOUR" ON 
                            "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                    WHERE 
                        LOWER("CT_DIA_DIEM"."MaDD") = LOWER('${req.query.Go}')
                        OR LOWER("DIA_DIEM"."TenDD") LIKE LOWER('%${req.query.Go}%')
                    ORDER BY "TOURS"."GiaTour" DESC          
                `;
            }
        } else if (req.query.DeXuat == null && req.query.Gia == null 
            && req.query.ThoiLuongTour != null) {
            if (req.query.ThoiLuongTour === 'Thap') {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DAT_TOUR" ON 
                            "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                    WHERE 
                        LOWER("CT_DIA_DIEM"."MaDD") = LOWER('${req.query.Go}')
                        OR LOWER("DIA_DIEM"."TenDD") LIKE LOWER('%${req.query.Go}%')
                    ORDER BY "TOURS"."ThoiGian" DESC         
                `;
            } else {
                arrQueries[0] = `
                    SELECT 
                        "TOURS"."MaTour",
                        "TOURS"."TenTour",
                        "TOURS"."ThoiGian",
                        "TOURS"."GiaTour",
                        "TOURS"."AnhTour"
                    FROM 
                        "TOURS" 
                        LEFT JOIN "CT_DAT_TOUR" ON 
                            "CT_DAT_TOUR"."MaTour"="TOURS"."MaTour" 
                        LEFT JOIN "CT_DIA_DIEM" ON
                            "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
                        LEFT JOIN "DIA_DIEM" ON
                            "CT_DIA_DIEM"."MaDD" = "DIA_DIEM"."MaDD"
                    WHERE 
                        LOWER("CT_DIA_DIEM"."MaDD") = LOWER('${req.query.Go}')
                        OR LOWER("DIA_DIEM"."TenDD") LIKE LOWER('%${req.query.Go}%')
                    ORDER BY "TOURS"."ThoiGian" ASC              
                `;
            }
        }
        arrQueries[1] = `
            SELECT 
                COUNT("CT_DIA_DIEM"."MaDD") AS myCount,
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            FROM 
                "CT_DIA_DIEM" 
                LEFT JOIN "DIA_DIEM" ON 
                    "CT_DIA_DIEM"."MaDD"="DIA_DIEM"."MaDD" 
                WHERE "DIA_DIEM"."KieuDD" = 'Trong nước'
            GROUP BY 
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            ORDER BY myCount DESC
            LIMIT 6        
        `;
        arrQueries[2] = `
            SELECT 
                COUNT("CT_DIA_DIEM"."MaDD") AS myCount,
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            FROM 
                "CT_DIA_DIEM" 
                LEFT JOIN "DIA_DIEM" ON 
                    "CT_DIA_DIEM"."MaDD"="DIA_DIEM"."MaDD" 
                WHERE "DIA_DIEM"."KieuDD" = 'Ngoài nước'
            GROUP BY 
                "DIA_DIEM"."TenDD",
                "DIA_DIEM"."KieuDD",
                "DIA_DIEM"."MaDD"
            ORDER BY myCount DESC
            LIMIT 6          
        `;
        arrQueries[3] = `
            SELECT "TenDD"
            FROM "DIA_DIEM"
            WHERE LOWER("DIA_DIEM"."MaDD") = LOWER('${req.query.Go}')
            OR LOWER("DIA_DIEM"."TenDD") LIKE LOWER('%${req.query.Go}%')
        `;
        arrQueries[4] = `
            SELECT *
            FROM "DIA_DIEM"
            WHERE "KieuDD" = 'Trong nước'        
        `;
        arrQueries[5] = `
            SELECT *
            FROM "DIA_DIEM"
            WHERE "KieuDD" = 'Ngoài nước'        
        `;
        const Data = [];
        const aJSON = {};
        handleQueries(arrQueries, Data, (result) => {
            Data.push(result.rows);
            if (Data.length === arrQueries.length) {
                if (Data[3].length === 0) {
                    aJSON.Go = 'Không có kết quả tìm kiếm T_T';
                } else {
                    aJSON.Go = `Tours du lịch tại ${Data[3][0].TenDD}`;
                }
                aJSON.XemThem = Data[0];
                aJSON.HotTrongNuoc = Data[1];
                aJSON.HotNgoaiNuoc = Data[2];
                aJSON.TrongNuoc = Data[4];
                aJSON.NgoaiNuoc = Data[5];
                res.send(JSON.stringify(aJSON, null, 3));
                res.end();
            }
        });
    }
});

router.post('/chi-tiet-dat-tour', urlencodedParser, (req, res) => {
    const aTime = new Date();
    const ChiTietHoaDon = {};
    ChiTietHoaDon.MaDon = `${req.body.MaTour}${req.body.SDTKH}${req.body.NgayKH}`;
    ChiTietHoaDon.MaTour = req.body.MaTour;
    ChiTietHoaDon.TenTour = req.body.TenTour;
    ChiTietHoaDon.NgayKH = req.body.NgayKH;
    ChiTietHoaDon.SLKhach = req.body.SLKhach;
    ChiTietHoaDon.TongTien = moneyToNumber(req.body.TongTien);
    ChiTietHoaDon.TenKH = req.body.TenKH;
    ChiTietHoaDon.EmailKH = req.body.EmailKH;
    ChiTietHoaDon.SDTKH = req.body.SDTKH;
    ChiTietHoaDon.YeuCauKhac = req.body.Khac;
    ChiTietHoaDon.NVChecked = false;
    ChiTietHoaDon.HinhThucThanhToan = req.body.HinhThucThanhToan;
    ChiTietHoaDon.HuyTour = false;
    ChiTietHoaDon.Thang = aTime.getMonth();
    ChiTietHoaDon.Nam = aTime.getFullYear();
    const aQuery = `
        INSERT INTO "CT_DAT_TOUR" ("MaDon", "TenKH", "SDTKH", "EmailKH", "MaTour", 
        "SLKhach", "NgayKH", "TongTien", "Checked", "YeuCauKhac", 
        "HinhThucThanhToan", "HuyTour", "Thang", "Nam")
        VALUES (
            '${ChiTietHoaDon.MaDon}',
            '${ChiTietHoaDon.TenKH}',
            '${ChiTietHoaDon.SDTKH}',
            '${ChiTietHoaDon.EmailKH}',
            '${ChiTietHoaDon.MaTour}',
            '${ChiTietHoaDon.SLKhach}',
            '${ChiTietHoaDon.NgayKH}',
            '${ChiTietHoaDon.TongTien}',
            '${ChiTietHoaDon.NVChecked}',
            '${ChiTietHoaDon.YeuCauKhac}',
            '${ChiTietHoaDon.HinhThucThanhToan}',
            '${ChiTietHoaDon.HuyTour}',
            '${ChiTietHoaDon.Thang}',
            '${ChiTietHoaDon.Nam}'
        )
    `;
    handleAQuery(aQuery, (result) => {
        console.log(result);
        const mailOptions = {
            from: '"ALAs.com" <alas.travel.io@gmail.com>', // sender address
            to: `${ChiTietHoaDon.EmailKH}`, // list of receivers
            subject: "[ALA's Travel] Xác nhận đặt hàng ", // Subject line
            text: '', // plain text body
            html: `
                <span>Chào bạn, <b>${ChiTietHoaDon.TenKH}</b></span><br/>
                <p>Bạn vừa đặt 1 Tour <b>${ChiTietHoaDon.MaTour}</b> - 
                    <b>${ChiTietHoaDon.TenTour}</b> với số lượng là 
                    <b>${ChiTietHoaDon.SLKhach}</b> khách</p> <br />
                <p>Nhân viên của ALA's sẽ liên hệ với bạn trong thời gian sớm nhất<p>
            ` // html body
        };
        nodemailer.transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
            res.send('DON DAT TOUR DANG DUOC XU LY...');
        });
        res.end();
    });
});

router.get('/tours-da-xem', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const aQuery = `
        SELECT *
        FROM "TOURS_DA_XEM"
        	JOIN "TOURS" ON "TOURS_DA_XEM"."MaTour" = "TOURS"."MaTour"
        WHERE "IP" = '${req.sessionID}'
        ORDER BY "TOURS_DA_XEM"."ThoiGian" DESC
        LIMIT 6  
    `;
    handleAQuery(aQuery, (result) => {
        res.send(JSON.stringify(result.rows, null, 3));
        res.end();
    });
});
router.post('/tours-da-xem/insert', urlencodedParser, (req, res) => {
    const aQuery = `
        INSERT INTO "TOURS_DA_XEM" ("IP", "MaTour")
        SELECT '${req.sessionID}', '${req.body.MaTour}'
        WHERE
            NOT EXISTS (
                SELECT "MaTour" FROM "TOURS_DA_XEM" 
                WHERE "MaTour" = '${req.body.MaTour}' 
                AND "IP" = '${req.sessionID}'
            )       
    `;
    handleAQuery(aQuery, (result) => {
        console.log(result);
        res.end();
    });
});
router.post('/tours-da-xem/delete', urlencodedParser, (req, res) => {
    const aQuery = `
        DELETE FROM "TOURS_DA_XEM"
        WHERE "MaTour" = '${req.body.MaTour}'
    `;
    handleAQuery(aQuery, (result) => {
        console.log(result);
        res.end();
    });
});

//  TOURS CHUA XU LY

router.post('/tours-chua-xu-ly/delete', urlencodedParser, (req, res) => {
    const aQuery = `
        DELETE FROM "CT_DAT_TOUR"
        WHERE "MaDon" = '${req.body.MaDon}'
    `;
    handleAQuery(aQuery, (result) => {
        console.log(result);
        res.end();
    });
});

router.post('/tours-chua-xu-ly/checked', urlencodedParser, (req, res) => {
    const aQuery = `
        UPDATE "CT_DAT_TOUR"
        SET "Checked" = true
        WHERE "CT_DAT_TOUR"."MaDon" = '${req.body.MaDon}'
    `;
    handleAQuery(aQuery, (result) => {
        console.log(result);
        res.end();
    });
});

router.get('/tours-chua-xu-ly/select', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const aQuery = `
        SELECT *
        FROM "CT_DAT_TOUR" JOIN "TOURS" ON "CT_DAT_TOUR"."MaTour" = "TOURS"."MaTour"
        WHERE "CT_DAT_TOUR"."Checked" = false   
        ORDER BY "CT_DAT_TOUR"."NgayDatTour" DESC  
    `;
    handleAQuery(aQuery, (result) => {
        res.send(JSON.stringify(result.rows, null, 3));
        res.end();
    });
});
router.get('/tours-chua-xu-ly/where', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const aQuery = `
        SELECT *
        FROM "CT_DAT_TOUR" JOIN "TOURS" ON "CT_DAT_TOUR"."MaTour" = "TOURS"."MaTour"
        WHERE "CT_DAT_TOUR"."MaDon" = '${req.query.MaDon}'    
    `;
    handleAQuery(aQuery, (result) => {
        res.send(JSON.stringify(result.rows, null, 3));
        res.end();
    });
});
//  TOURS DA XU LY
router.get('/tours-da-xu-ly/select', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const aQuery = `
        SELECT *
        FROM "CT_DAT_TOUR" JOIN "TOURS" ON "CT_DAT_TOUR"."MaTour" = "TOURS"."MaTour"
        WHERE "CT_DAT_TOUR"."Checked" = true AND "CT_DAT_TOUR"."HuyTour" = false  
        ORDER BY "CT_DAT_TOUR"."NgayDatTour" DESC      
    `;
    handleAQuery(aQuery, (result) => {
        res.send(JSON.stringify(result.rows, null, 3));
        res.end();
    });
});
router.post('/tours-da-xu-ly/huy-tour', urlencodedParser, (req, res) => {
    const aQuery = `
        UPDATE "CT_DAT_TOUR"
        SET "HuyTour" = true
        WHERE "CT_DAT_TOUR"."MaDon" = '${req.body.MaDon}'     
    `;
    handleAQuery(aQuery, (result) => {
        console.log(result);
        res.end();
    });
});

//  BAO CAO
router.get('/bao-cao/where', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    const arrQueries = [];
    const aYear = req.query.Nam;
    for (let i = 0; i < 12; i++) {
        arrQueries[i] = `
                SELECT
                    (	SELECT COUNT("MaDon")
                        FROM "CT_DAT_TOUR"
                        WHERE "Thang" = '${i + 1}' AND "Nam" = '${aYear}' 
                    ) AS SLDatTour,
                    (	SELECT COUNT("MaDon")
                        FROM "CT_DAT_TOUR"
                        WHERE "Thang" = '${i + 1}' AND "Nam" = '${aYear}' AND "HuyTour" = true
                    ) AS SLHuyTour,
                    (
                        SELECT COUNT("Thang") 
                        FROM "KHACH_TRUY_CAP"
                        WHERE "Thang" = '${i + 1}' AND "Nam" = '${aYear}'
                    ) AS SLKTC     
            `;
    }
    const Data = [];
    const aJSON = {};
    handleQueries(arrQueries, Data, (result) => {
        Data.push(result.rows);
        if (Data.length === arrQueries.length) {
            aJSON.Thang1 = Data[0];
            aJSON.Thang2 = Data[1];
            aJSON.Thang3 = Data[2];
            aJSON.Thang4 = Data[3];
            aJSON.Thang5 = Data[4];
            aJSON.Thang6 = Data[5];
            aJSON.Thang7 = Data[6];
            aJSON.Thang8 = Data[7];
            aJSON.Thang9 = Data[8];
            aJSON.Thang10 = Data[9];
            aJSON.Thang11 = Data[10];
            aJSON.Thang12 = Data[11];
            res.send(JSON.stringify(aJSON, null, 3));
            res.end();
        }
    });
});

//  DIA DIEM
router.post('/dia-diem/insert', urlencodedParser, cpUpload, (req, res) => {
    const aQuery = `
            INSERT INTO "DIA_DIEM" ("MaDD", "TenDD", "KieuDD", "AnhDD")
            SELECT '${req.body.MaDD}', '${req.body.TenDD}', 
            '${req.body.KieuDD}', '${req.files.anhDiaDiemInput[0].originalname}'
            WHERE
                NOT EXISTS (
                    SELECT "MaDD" FROM "DIA_DIEM" WHERE "MaDD" = '${req.body.MaDD}'
                )
        `;
    handleAQuery(aQuery, (result) => {
        console.log(result);
        res.redirect('/management/dia-diem');
    });
});
//  SO LUONG TOUR TAI DIA DIEM
router.get('/dia-diem/so-luong-tour', (req, res) => {
    const aQuery = `
            SELECT COUNT("CT_DIA_DIEM"."MaTour")
            FROM "TOURS" JOIN "CT_DIA_DIEM" ON "TOURS"."MaTour" = "CT_DIA_DIEM"."MaTour"
            WHERE "CT_DIA_DIEM"."MaDD" = '${req.query.MaDD}'
        `;
    handleAQuery(aQuery, (result) => {
        res.send(JSON.stringify(result.rows, null, 3));
        res.end();
    });
});
// DELETE DIA DIEM
router.post('/dia-diem/delete', urlencodedParser, (req, res) => {
    const MaDD = req.body.MaDD;
    const aQuery = `
            DELETE FROM "DIA_DIEM"
            WHERE "MaDD" = '${MaDD}'  
        `;
    handleAQuery(aQuery, (result) => {
        // console.log(result);
        res.redirect('/management/dia-diem');
    });
});

module.exports = router;

