const express = require('express');

const router = express.Router();
const { handleAQuery } = require('../Libs/Database');
const { localhost } = require('../configure');

module.exports = (passport, authenticationMiddleware) => {
// LOGIN
    //GET
    router.get('/login', (req, res) => {
        res.render('login', { message: req.flash('error') });
    });
    //POST
    router.post('/login', passport.authenticate('local', {
        failureRedirect: '/management/login',
        successRedirect: '/management/',
        failureFlash: true
    }));

//LOGOUT
    router.get('/logout', authenticationMiddleware(), (req, res) => {
        req.session.destroy();
        res.redirect('/management');
    });

//  ADMIN
    // GET
    router.get('/', authenticationMiddleware(), (req, res) => {
        res.render('admin', { localhost, user: req.user.firstName });
    });
    // POST

//  CHI TIET TOUR
    //GET
    router.get('/tours/chi-tiet-tour', authenticationMiddleware(), (req, res) => {
        res.render('chi-tiet-tour', { localhost, user: req.user.firstName });
    });
    //POST

//  THEM TOUR
    //GET
    router.get('/tours/them-tour', authenticationMiddleware(), (req, res) => {
        res.render('them-tour', { localhost, user: req.user.firstName });
    });
    //POST
    
//  TOURS DANG CHO
    router.get('/tours/tours-dang-cho', authenticationMiddleware(), (req, res) => {
        res.render('toursdangcho', { localhost, user: req.user.firstName });
    });
    router.get('/tours/tours-dang-cho/:MaDon', authenticationMiddleware(), (req, res) => {
        const aQuery = `
            SELECT *
            FROM "CT_DAT_TOUR" JOIN "TOURS" ON "CT_DAT_TOUR"."MaTour" = "TOURS"."MaTour"
            WHERE "CT_DAT_TOUR"."MaDon" = '${req.params.MaDon}'   
                AND "CT_DAT_TOUR"."Checked" = false     
        `;
        handleAQuery(aQuery, (result) => {
            const Data = result.rows[0];

            res.render('chi-tiet-toursdangcho', { Data, localhost, user: req.user.firstName });
        });
    });

//  TOURS DA XY LY
    router.get('/tours/tours-da-xu-ly', authenticationMiddleware(), (req, res) => {
        res.render('toursxacnhan', { localhost });
    });
    router.get('/tours/tours-da-xu-ly/:MaDon', authenticationMiddleware(), (req, res) => {
        const aQuery = `
            SELECT *
            FROM "CT_DAT_TOUR" JOIN "TOURS" ON "CT_DAT_TOUR"."MaTour" = "TOURS"."MaTour"
            WHERE "CT_DAT_TOUR"."MaDon" = '${req.params.MaDon}'   
                AND "CT_DAT_TOUR"."Checked" = true     
        `;
        handleAQuery(aQuery, (result) => {
            const Data = result.rows[0];

            res.render('chi-tiet-toursdaxuly', { Data, localhost, user: req.user.firstName });
        });
    });
//  BAO CAO
    router.get('/bao-cao', authenticationMiddleware(), (req, res) => {
        const aTime = new Date();

        res.render('baocao', { localhost, nam: aTime.getFullYear(), user: req.user.firstName });
    });
//  DIA DIEM
    router.get('/dia-diem', authenticationMiddleware(), (req, res) => {
        res.render('diadiem', { localhost, user: req.user.firstName });
    });
//  TIN NHAN
    router.get('/tin-nhan', authenticationMiddleware(), (req, res) => {
        res.render('CSKH', { localhost, user: req.user.firstName });
    });
    return router;
};

