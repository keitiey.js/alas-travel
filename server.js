require('babel-register');
const express = require('express');

//Passport
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');
const flash = require('connect-flash');

//	Libs
const { 
	handleClientSendData, handleClientSendDataBanner, handleTrangQuanTriSendData, handleUserJoinRoom,
	handleUserSendMessage, handleAdminJoinGroup, handleAdminOutGroup, handleUserOutGroup, 
	handleAdminSendMessage
} = require('./Libs/Socket');
const { passport, authenticationMiddleware } = require('./Libs/Passport');

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

//	PORT
const port = 8080;
const url = `http://localhost:${port}`;
server.listen(process.env.PORT || port, () => console.log(url));

//	SOCKET.IO
io.on('connection', (socket) => {
	io.sockets.setMaxListeners(0);
	console.log(`SocketID: ${socket.id}`);
	socket.on('disconnect', () => {
		console.log(`Disconnection: ${socket.id}`);
	});
	handleClientSendData(socket);
	handleClientSendDataBanner(io, socket);
	handleTrangQuanTriSendData(socket);
	handleUserJoinRoom(io, socket);
	handleUserSendMessage(io, socket);
	handleAdminJoinGroup(socket);
	handleAdminOutGroup(io, socket);
	handleUserOutGroup(io, socket);
	handleAdminSendMessage(io, socket);
});

//	URL
const api = require('./router/api');
const management = require('./router/management')(passport, authenticationMiddleware);
const index = require('./router/index');

app.set('view engine', 'ejs');
app.set('views', './views');

//	Middleware
app.use(express.static('public'));
app.use(cookieParser());
app.use(bodyParser());
app.use(session({ secret: 'keyboard cat' }));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

app.use('/api', api);
app.use('/management', management);
app.use('/', index);

