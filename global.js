const numberToMoney = (x) => {
    const parts = x.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return parts.join('.');
};
// Convert 9,500,000 => 9500000
const moneyToNumber = (x) => Number(x.replace(/[^0-9\.]+/g, '')); //eslint-disable-line

module.exports = {
    numberToMoney,
    moneyToNumber
};
