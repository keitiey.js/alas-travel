const path = require('path');

module.exports = {
    entry: [
        // 'script-loader!jquery/dist/jquery.min.js',
        // 'script-loader!react-chat-bubble/lib/ChatBubble.js',
        './App/App.js'
    ],
    output: {
        path: path.resolve(__dirname, 'public'),
        publicPath: '/',
        filename: 'bundle.js'
    },
    devServer: {
        historyApiFallback: true
    },
    // externals: {
    //     jquery: 'jQuery'
    // },
    // plugins: [
    //     new webpack.ProvidePlugin({
    //     $: "jquery",
    //     jQuery: "jquery",
    //     "window.jQuery": "jquery"
    //     })
    // ],
    resolve: {
        extensions: ['.js']
    },
    module: {
        loaders: [
            {
                loader: 'babel-loader',
                test: /\.js?$/,
                exclude: /node_modules/
            },
            {
                test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
                loader: 'file-loader?name=fonts/[hash].[ext]'
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    'url-loader?limit=10000',
                    'img-loader?name=images/[hash]-[name].[ext]'
                ]
            },
            { test: /\.css$/, loader: 'style-loader!css-loader' }
        ]
    }
    
};
