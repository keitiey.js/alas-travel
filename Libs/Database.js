const { Pool } = require('pg');

//  Config your database
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'travelweb',
    password: '1',
    port: 5432,
    max: 20,
    idleTimeoutMillis: 30000,
    connectionTimeoutMillis: 2000,
});

const handleAQuery = (strQuery, callback) => {
  pool.connect((err, client, done) => {
    if (err) throw err;
    client.query(strQuery, (errQuery, result) => {
        done();
        if (errQuery) {
            console.log('handle query failed', [strQuery, errQuery.stack]);
        } else {
            callback(result); 
        }
    });
  });
}; 

const handleQueries = (arrQueries, arrData, callback) => {
    pool.connect((err, client, done) => {
        if (err) throw err;
        arrQueries.forEach((q) => {
            client.query(q, (errQuery, result) => {
                done();
                if (errQuery) {
                    console.log('handle query failed', [q, errQuery.stack]);
                } else {
                    callback(result); 
                }
            });
        });
    });
};

const handleInsertTables = (arrQueries, callback) => {
    pool.connect((err, client, done) => {
        if (err) throw err;
        arrQueries.forEach((q) => {
            client.query(q, (errQuery, result) => {
                done();
                if (errQuery) {
                    console.log('handle query failed', [q, errQuery.stack]);
                } else {
                    callback(result);
                }
            });
        });
    });    
};

const handleDeleteTables = (arrQueries, callback) => {
    pool.connect((err, client, done) => {
        if (err) throw err;
        arrQueries.forEach((q) => {
            client.query(q, (errQuery, result) => {
                done();
                if (errQuery) {
                    console.log('handle query failed', [q, errQuery.stack]);
                } else {
                    callback(result);
                }
            });
        });
    });    
};

const handleUpdateTables = (arrQueries, callback) => {
    pool.connect((err, client, done) => {
        if (err) throw err;
        arrQueries.forEach((q) => {
            client.query(q, (errQuery, result) => {
                done();
                if (errQuery) {
                    console.log('handle query failed', [q, errQuery.stack]);
                } else {
                    callback(result); 
                }
            });
        });
    });      
};

module.exports = {
    pool, 
    handleAQuery,
    handleInsertTables,
    handleDeleteTables,
    handleQueries,
    handleUpdateTables
};
