//Passport
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const users = {
	admin: {
		password: '1234',
		email: 'keitiey@something.com',
		firstName: 'Quản trị viên'
	}
};

const findUser = (username, done) => {
	done(null, users[username]);
};

const createLiteUser = (user) => {
	const liteUser = {
		email: user.email,
		firstName: user.firstName
	};
	return liteUser;
};

const authenticationMiddleware = () => (req, res, next) => {
	if (req.isAuthenticated()) {
		return next();
	}
	res.redirect('/management/login');
};

passport.use(new LocalStrategy((username, password, done) => {
	console.log('local stragegy', username, password);
	findUser(username, (err, user) => {
		if (!user) {
			return done(null, false, { message: 'Tài khoản sai. Hãy thử lại' });
		}
		if (password !== user.password) {
			return done(null, false, { message: 'Mật khẩu sai. Hãy thử lại' });
		}
		return done(null, user);
	});
}));

passport.serializeUser((user, cb) => {
	const liteUser = createLiteUser(user);
	cb(null, liteUser);
});

passport.deserializeUser((user, cb) => {
	cb(null, user);
});

module.exports = {
    passport,
    authenticationMiddleware
};
