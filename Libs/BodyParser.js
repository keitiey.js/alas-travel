const BodyParser = require('body-parser');

const jsonParser = BodyParser.json();
const urlencodedParser = BodyParser.urlencoded({ extended: false });

module.exports = {
    jsonParser,
    urlencodedParser
};
