const { handleAQuery, handleQueries } = require('../Libs/Database');

const arrRoom = [];
export const handleClientSendData = (socket) => {
	socket.on('Client-send-data', (data) => {
		console.log(`${socket.id} vua gui ${data}`);
		const arrQueries = [];
		arrQueries[0] = `
				SELECT "MaDD","TenDD"
				FROM "DIA_DIEM"
				WHERE LOWER("TenDD") LIKE LOWER('%${data}%')
				LIMIT 6
			`;
		arrQueries[1] = `
				SELECT "MaTour", "TenTour"
				FROM "TOURS"
				WHERE LOWER("TenTour") LIKE LOWER(U&'%${data}%')
					OR LOWER("MaTour") LIKE LOWER(U&'%${data}%')	
				LIMIT 6
			`;
		const Data = [];
		const aJSON = {};
		handleQueries(arrQueries, Data, (result) => {
			Data.push(result.rows);
			if (Data.length === arrQueries.length) {
				aJSON.DiaDiem = Data[0];
				aJSON.Tours = Data[1];
			}
			socket.emit('Server-send-data', aJSON);
		});
	});
};

export const handleClientSendDataBanner = (io, socket) => {
	socket.on('Client-send-data-banner', (data) => {
		console.log(`${socket.id} vua gui ${data}`);
		const arrQueries = [];
		arrQueries[0] = `
				SELECT "MaDD","TenDD"
				FROM "DIA_DIEM"
				WHERE LOWER("TenDD") LIKE LOWER('%${data}%')
				LIMIT 6
			`;
		arrQueries[1] = `
				SELECT "MaTour", "TenTour"
				FROM "TOURS"
				WHERE LOWER("TenTour") LIKE LOWER(U&'%${data}%')
					OR LOWER("MaTour") LIKE LOWER(U&'%${data}%')	
				LIMIT 6
			`;
		const Data = [];
		const aJSON = {};
		handleQueries(arrQueries, Data, (result) => {
			Data.push(result.rows);
			if (Data.length === arrQueries.length) {
				aJSON.DiaDiem = Data[0];
				aJSON.Tours = Data[1];
			}
			io.sockets.emit('Server-send-data-banner', aJSON);
		});
		//	KHACH TRUY CAP
		const clientIp = socket.request.connection.remoteAddress;
		const aTime = new Date();
		const aQuery = `
				INSERT INTO "KHACH_TRUY_CAP" ("Thang", "Nam", "IP")
				SELECT '${aTime.getMonth()}', '${aTime.getFullYear()}', '${clientIp}'
				WHERE
					NOT EXISTS (
						SELECT "IP" FROM "KHACH_TRUY_CAP" WHERE "IP" = '${clientIp}'
					)   
			`;
		handleAQuery(aQuery, (result) => {
			console.log(result);
		});
	});
};

export const handleTrangQuanTriSendData = (socket) => {
	socket.on('TrangQuanTri_send_data', (data) => {
		const aQuery = `
			SELECT "MaTour", "TenTour", "AnhTour"
			FROM "TOURS"
			WHERE LOWER("TenTour") LIKE LOWER(U&'%${data}%')
				OR LOWER("MaTour") LIKE LOWER(U&'%${data}%')	
			LIMIT 6		
		`;
		handleAQuery(aQuery, (result) => {
			socket.emit('Server_send_data_to_TrangQuanTri', result.rows);
		});
	});
};

export const handleUserJoinRoom = (io, socket) => {
	socket.on('User_join_room', (data) => {
		socket.nameRoom = data.nameRoom; //eslint-disable-line
		socket.nameUser = data.nameUser; //eslint-disable-line
		socket.sdtUser = data.sdtUser; //eslint-disable-line
		socket.join(socket.nameRoom);
		if (arrRoom.length === 0) {
			arrRoom.push(socket.nameRoom);
		} else if (socket.id !== socket.nameRoom) {
			if (arrRoom.indexOf(`${socket.nameRoom}`) > -1) {
				console.log('Bi trung!');
			} else {
				arrRoom.push(socket.nameRoom);
			}
		}
		console.log(arrRoom);
		io.sockets.emit('Server_send_listRoom', arrRoom);
	});
};

export const handleUserSendMessage = (io, socket) => {
	socket.on('User_send_message', (data) => {
		console.log(socket.nameRoom);
		console.log(data);
		io.sockets.in(socket.nameRoom).emit('User_chat', data);
	});
};

export const handleAdminJoinGroup = (socket) => {
	socket.on('Admin_join_room', (data) => {
		socket.nameRoom = data; //eslint-disable-line
		socket.join(socket.nameRoom);
		console.log(`Admin joined to ${socket.nameRoom}`);
		socket.emit('Admin_joined_room', socket.nameRoom);
	});
};

export const handleAdminOutGroup = (io, socket) => {
	socket.on('Admin_out_room', () => {
		socket.leave(socket.nameRoom);
		console.log(`Admin_out_room ${socket.nameRoom}`); //eslint-disable-line
		io.sockets.in(socket.nameRoom).emit('Admin_outed_room_to_user', 'CSKH is offline');
		socket.emit('Admin_outed_room_to_admin', 'CSKH is offline');
	});
};

export const handleUserOutGroup = (io, socket) => {
	socket.on('User_out_room', () => {
		socket.leave(socket.nameRoom);
		console.log(`User_out_room ${socket.nameRoom}`); //eslint-disable-line
		arrRoom.splice(arrRoom.indexOf(socket.nameRoom), 1);
		console.log(arrRoom);
		io.sockets.emit('Server_send_listRoom', arrRoom);
	});
};

export const handleAdminSendMessage = (io, socket) => {
	socket.on('Admin_send_message', (data) => {
		console.log(socket.nameRoom);
		console.log(data);
		io.sockets.in(socket.nameRoom).emit('Admin_chat', data);
	});
};
