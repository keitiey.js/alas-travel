const multer = require('multer');

//Upload Banner
const upToBanner = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'public/upload/banner');
  },
  filename(req, file, cb) {
    cb(null, file.originalname);
  }
});
const upBanner = multer({ storage: upToBanner }).single('anhBannerInput');

const upToAnhLichTrinh = multer.diskStorage({
    destination(req, file, cb) {
        cb(null, 'public/upload/tours');
    },
    filename(req, file, cb) {
        cb(null, file.originalname);
    }
});
const upAnhLichTrinh = multer({ storage: upToAnhLichTrinh }).single('anhLichTrinhInput');

const uploadStorage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'public/upload/tours');
  },
  filename(req, file, cb) {
    cb(null, file.originalname);
  }
});

const upToAnhDiaDiem = multer.diskStorage({
    destination(req, file, cb) {
        cb(null, 'public/upload/tours');
    },
    filename(req, file, cb) {
        cb(null, file.originalname);
    }
});
const upAnhDiaDiem = multer({ storage: upToAnhDiaDiem }).single('anhDiaDiemInput');

const cpUpload = multer({ 
    storage: uploadStorage 
}).fields([
    { name: 'anhBannerInput', maxCount: 1 },
    { name: 'anhTourInput', maxCount: 1 },
    { name: 'anhLichTrinhInput', maxCount: 10 },
    { name: 'anhDiaDiemInput', maxCount: 1 }
]);


module.exports = {
    upBanner,
    upAnhLichTrinh,
    upAnhDiaDiem,
    cpUpload,
};
